package com.instaplay.flictrick.Model.Subscription;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionPackage implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("monthly")
    @Expose
    private String monthly;
    @SerializedName("yearly")
    @Expose
    private String yearly;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("status")
    @Expose
    private String status;

    protected SubscriptionPackage(Parcel in) {
        id = in.readString();
        country = in.readString();
        monthly = in.readString();
        yearly = in.readString();
        currency = in.readString();
        status = in.readString();
    }

    public static final Creator<SubscriptionPackage> CREATOR = new Creator<SubscriptionPackage>() {
        @Override
        public SubscriptionPackage createFromParcel(Parcel in) {
            return new SubscriptionPackage(in);
        }

        @Override
        public SubscriptionPackage[] newArray(int size) {
            return new SubscriptionPackage[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(country);
        parcel.writeString(monthly);
        parcel.writeString(yearly);
        parcel.writeString(currency);
        parcel.writeString(status);
    }
}
