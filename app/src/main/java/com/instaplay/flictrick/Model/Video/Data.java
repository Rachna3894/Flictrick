package com.instaplay.flictrick.Model.Video;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data implements Parcelable {
    @SerializedName("rootCategoryId")
    @Expose
    private String rootCategoryId;
    @SerializedName("rootCategoryName")
    @Expose
    private String rootCategoryName;
    @SerializedName("subCatData")
    @Expose
    private ArrayList<SubCatData> subCatData;

    public Data(){

    }

    public Data(Parcel in) {
        rootCategoryId = in.readString();
        rootCategoryName = in.readString();
        subCatData = in.createTypedArrayList(SubCatData.CREATOR);
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public String getRootCategoryId() {
        return rootCategoryId;
    }

    public void setRootCategoryId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }

    public String getRootCategoryName() {
        return rootCategoryName;
    }

    public void setRootCategoryName(String rootCategoryName) {
        this.rootCategoryName = rootCategoryName;
    }

    public ArrayList<SubCatData> getSubCatData() {
        return subCatData;
    }

    public void setSubCatData(ArrayList<SubCatData> subCatData) {
        this.subCatData = subCatData;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(rootCategoryId);
        parcel.writeString(rootCategoryName);
        parcel.writeTypedList(subCatData);
    }
}
