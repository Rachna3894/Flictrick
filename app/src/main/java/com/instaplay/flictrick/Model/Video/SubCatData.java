package com.instaplay.flictrick.Model.Video;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
public class SubCatData implements Parcelable {
    @SerializedName("subRootCategoryId")
    @Expose
    private String subRootCategoryId;
    @SerializedName("subRootCategoryName")
    @Expose
    private String subRootCategoryName;
    @SerializedName("subRootCategorySlug")
    @Expose
    private String subRootCategorySlug;
    @SerializedName("is_metadata")
    @Expose
    private String isMetadata;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("cast")
    @Expose
    private String cast;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("trailerSubtitleFilePath")
    @Expose
    private String trailerSubtitleFilePath;
    @SerializedName("videoSubtitleFilePath")
    @Expose
    private String videoSubtitleFilePath;
    @SerializedName("contentEpisodeData")
    @Expose
    private ArrayList<ContentEpisodeData> contentEpisodeData;

    protected SubCatData(Parcel in) {
        subRootCategoryId = in.readString();
        subRootCategoryName = in.readString();
        subRootCategorySlug = in.readString();
        isMetadata = in.readString();
        description = in.readString();
        cast = in.readString();
        title = in.readString();
        contentType = in.readString();
        year = in.readString();
        status = in.readString();
        trailerSubtitleFilePath = in.readString();
        contentEpisodeData = in.createTypedArrayList(ContentEpisodeData.CREATOR);
    }

    public static final Creator<SubCatData> CREATOR = new Creator<SubCatData>() {
        @Override
        public SubCatData createFromParcel(Parcel in) {
            return new SubCatData(in);
        }

        @Override
        public SubCatData[] newArray(int size) {
            return new SubCatData[size];
        }
    };

    public String getSubRootCategoryId() {
        return subRootCategoryId;
    }

    public void setSubRootCategoryId(String subRootCategoryId) {
        this.subRootCategoryId = subRootCategoryId;
    }

    public String getSubRootCategoryName() {
        return subRootCategoryName;
    }

    public void setSubRootCategoryName(String subRootCategoryName) {
        this.subRootCategoryName = subRootCategoryName;
    }

    public String getSubRootCategorySlug() {
        return subRootCategorySlug;
    }

    public void setSubRootCategorySlug(String subRootCategorySlug) {
        this.subRootCategorySlug = subRootCategorySlug;
    }

    public String getIsMetadata() {
        return isMetadata;
    }

    public void setIsMetadata(String isMetadata) {
        this.isMetadata = isMetadata;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrailerSubtitleFilePath() {
        return trailerSubtitleFilePath;
    }

    public void setTrailerSubtitleFilePath(String trailerSubtitleFilePath) {
        this.trailerSubtitleFilePath = trailerSubtitleFilePath;
    }

    public String getVideoSubtitleFilePath() {
        return videoSubtitleFilePath;
    }

    public void setVideoSubtitleFilePath(String videoSubtitleFilePath) {
        this.videoSubtitleFilePath = videoSubtitleFilePath;
    }

    public ArrayList<ContentEpisodeData> getContentEpisodeData() {
        return contentEpisodeData;
    }

    public void setContentEpisodeData(ArrayList<ContentEpisodeData> contentEpisodeData) {
        this.contentEpisodeData = contentEpisodeData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(subRootCategoryId);
        parcel.writeString(subRootCategoryName);
        parcel.writeString(subRootCategorySlug);
        parcel.writeString(isMetadata);
        parcel.writeString(description);
        parcel.writeString(cast);
        parcel.writeString(title);
        parcel.writeString(contentType);
        parcel.writeString(year);
        parcel.writeString(status);
        parcel.writeString(trailerSubtitleFilePath);
        parcel.writeTypedList(contentEpisodeData);
    }
}
