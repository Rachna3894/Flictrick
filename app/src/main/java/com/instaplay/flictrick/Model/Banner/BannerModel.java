package com.instaplay.flictrick.Model.Banner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.instaplay.flictrick.Model.Category.CategoryModel;
import com.instaplay.flictrick.Model.Video.SubCategoryModel;

public class BannerModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("bannerPath")
    @Expose
    private String bannerPath;

    public String getAppBannerPath() {
        return appBannerPath;
    }

    public void setAppBannerPath(String appBannerPath) {
        this.appBannerPath = appBannerPath;
    }

    @SerializedName("appBannerPath")
    @Expose
    private String appBannerPath;
    @SerializedName("episodeUrl")
    @Expose
    private String episodeUrl;
    @SerializedName("rootCategory")
    @Expose
    private CategoryModel rootCategory;
    @SerializedName("subRootCategory")
    @Expose
    private SubCategoryModel subRootCategory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBannerPath() {
        return bannerPath;
    }

    public void setBannerPath(String bannerPath) {
        this.bannerPath = bannerPath;
    }

    public String getEpisodeUrl() {
        return episodeUrl;
    }

    public void setEpisodeUrl(String episodeUrl) {
        this.episodeUrl = episodeUrl;
    }

    public CategoryModel getRootCategory() {
        return rootCategory;
    }

    public void setRootCategory(CategoryModel rootCategory) {
        this.rootCategory = rootCategory;
    }

    public SubCategoryModel getSubRootCategory() {
        return subRootCategory;
    }

    public void setSubRootCategory(SubCategoryModel subRootCategory) {
        this.subRootCategory = subRootCategory;
    }
}
