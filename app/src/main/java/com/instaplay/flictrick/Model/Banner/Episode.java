package com.instaplay.flictrick.Model.Banner;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Episode implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("episodeNumber")
    @Expose
    private String episodeNumber;
    @SerializedName("episodePath")
    @Expose
    private String episodePath;
    @SerializedName("status")
    @Expose
    private String status;

    protected Episode(Parcel in) {
        id = in.readString();
        episodeNumber = in.readString();
        episodePath = in.readString();
        status = in.readString();
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        @Override
        public Episode createFromParcel(Parcel in) {
            return new Episode(in);
        }

        @Override
        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getEpisodePath() {
        return episodePath;
    }

    public void setEpisodePath(String episodePath) {
        this.episodePath = episodePath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(episodeNumber);
        parcel.writeString(episodePath);
        parcel.writeString(status);
    }
}
