package com.instaplay.flictrick.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.instaplay.flictrick.Model.Video.Data;

import java.util.ArrayList;

public class DataResponse {
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private ArrayList<Data>data;



}
