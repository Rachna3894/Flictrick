package com.instaplay.flictrick.Model.Video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchContentRequest {
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("rootCategoryId")
    @Expose
    private String rootCategoryId;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRootCategoryId() {
        return rootCategoryId;
    }

    public void setRootCategoryId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }
}
