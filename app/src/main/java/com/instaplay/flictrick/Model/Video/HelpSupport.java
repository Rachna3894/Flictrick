package com.instaplay.flictrick.Model.Video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpSupport {
    @SerializedName("sender")
    @Expose
    private String sender;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @SerializedName("query")
    @Expose
    private String query;
}
