package com.instaplay.flictrick.Model.Video;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentEpisodeData implements Parcelable {

    @SerializedName("content_episode_id")
    @Expose
    private String contentEpisodeId;
    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("episodeNumber")
    @Expose
    private String episodeNumber;
    @SerializedName("episodeSubtitle")
    @Expose
    private String episodeSubtitle;
    @SerializedName("episode_path")
    @Expose
    private String episodePath;
    @SerializedName("trailer_path")
    @Expose
    private String trailerPath;
    @SerializedName("teaser_path")
    @Expose
    private String teaserPath;
    @SerializedName("banner_path")
    @Expose
    private String bannerPath;
    @SerializedName("thumbnail_path")
    @Expose
    private String thumbnailPath;


    public ContentEpisodeData(String ContentEpisodeId,String VideoTitle,String EpisodeNumber,String Subtitle,String episodePath,String trailerPath,String teaserPath,String BannerPath,String thumbnailPath){
        this.contentEpisodeId= ContentEpisodeId;
        this.videoTitle = VideoTitle;
        this.episodeNumber= EpisodeNumber;
        this.episodeSubtitle = Subtitle;
        this.episodePath= episodePath;
        this.trailerPath= trailerPath;
        this.teaserPath = teaserPath;
        this.bannerPath= BannerPath;
        this.thumbnailPath= thumbnailPath;
    }

    protected ContentEpisodeData(Parcel in) {
        contentEpisodeId = in.readString();
        videoTitle = in.readString();
        episodeNumber = in.readString();
        episodeSubtitle = in.readString();
        episodePath = in.readString();
        trailerPath = in.readString();
        teaserPath = in.readString();
        bannerPath = in.readString();
        thumbnailPath = in.readString();
    }

    public static final Creator<ContentEpisodeData> CREATOR = new Creator<ContentEpisodeData>() {
        @Override
        public ContentEpisodeData createFromParcel(Parcel in) {
            return new ContentEpisodeData(in);
        }

        @Override
        public ContentEpisodeData[] newArray(int size) {
            return new ContentEpisodeData[size];
        }
    };

    public String getContentEpisodeId() {
        return contentEpisodeId;
    }

    public void setContentEpisodeId(String contentEpisodeId) {
        this.contentEpisodeId = contentEpisodeId;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getEpisodeSubtitle() {
        return episodeSubtitle;
    }

    public void setEpisodeSubtitle(String episodeSubtitle) {
        this.episodeSubtitle = episodeSubtitle;
    }

    public String getEpisodePath() {
        return episodePath;
    }

    public void setEpisodePath(String episodePath) {
        this.episodePath = episodePath;
    }

    public String getTrailerPath() {
        return trailerPath;
    }

    public void setTrailerPath(String trailerPath) {
        this.trailerPath = trailerPath;
    }

    public String getTeaserPath() {
        return teaserPath;
    }

    public void setTeaserPath(String teaserPath) {
        this.teaserPath = teaserPath;
    }

    public String getBannerPath() {
        return bannerPath;
    }

    public void setBannerPath(String bannerPath) {
        this.bannerPath = bannerPath;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(contentEpisodeId);
        parcel.writeString(videoTitle);
        parcel.writeString(episodeNumber);
        parcel.writeString(episodeSubtitle);
        parcel.writeString(episodePath);
        parcel.writeString(trailerPath);
        parcel.writeString(teaserPath);
        parcel.writeString(bannerPath);
        parcel.writeString(thumbnailPath);
    }
}
