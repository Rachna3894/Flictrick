package com.instaplay.flictrick.Model.Video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subCategoryName")
    @Expose
    private String subCategoryName;
    @SerializedName("subCategorySlug")
    @Expose
    private String subCategorySlug;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_metadata")
    @Expose
    private String isMetadata;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("cast")
    @Expose
    private String cast;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("contentType")
    @Expose
    private String contentType;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("trailerSubtitleFilePath")
    @Expose
    private String trailerSubtitleFilePath;
    @SerializedName("videoSubtitleFilePath")
    @Expose
    private String videoSubtitleFilePath;
    @SerializedName("insertionTime")
    @Expose
    private String insertionTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategorySlug() {
        return subCategorySlug;
    }

    public void setSubCategorySlug(String subCategorySlug) {
        this.subCategorySlug = subCategorySlug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsMetadata() {
        return isMetadata;
    }

    public void setIsMetadata(String isMetadata) {
        this.isMetadata = isMetadata;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTrailerSubtitleFilePath() {
        return trailerSubtitleFilePath;
    }

    public void setTrailerSubtitleFilePath(String trailerSubtitleFilePath) {
        this.trailerSubtitleFilePath = trailerSubtitleFilePath;
    }

    public String getVideoSubtitleFilePath() {
        return videoSubtitleFilePath;
    }

    public void setVideoSubtitleFilePath(String videoSubtitleFilePath) {
        this.videoSubtitleFilePath = videoSubtitleFilePath;
    }

    public String getInsertionTime() {
        return insertionTime;
    }

    public void setInsertionTime(String insertionTime) {
        this.insertionTime = insertionTime;
    }
}
