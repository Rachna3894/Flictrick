package com.instaplay.flictrick.Model.Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("rootCategoryName")
    @Expose
    private String rootCategoryName;
    @SerializedName("status")
    @Expose
    private String status;

    public CategoryModel(String Id,String category,String Status) {
        this.id= Id;
        this.rootCategoryName= category;
        this.status= Status;
    }

    public CategoryModel(String category){
        this.rootCategoryName=category;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    private boolean check= false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRootCategoryName() {
        return rootCategoryName;
    }

    public void setRootCategoryName(String rootCategoryName) {
        this.rootCategoryName = rootCategoryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
