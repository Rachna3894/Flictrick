package com.instaplay.flictrick.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceModel {

    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
