package com.instaplay.flictrick.Model.Subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionRequest {
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("country")
    @Expose
    private String country;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

