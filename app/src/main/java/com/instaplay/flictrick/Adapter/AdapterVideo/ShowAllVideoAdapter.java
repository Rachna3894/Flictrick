package com.instaplay.flictrick.Adapter.AdapterVideo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Activity.WebSeriesVideoActivity;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class ShowAllVideoAdapter extends BaseAdapter {
    private ArrayList<SearchData> results;
    Context mContext;
    String rootCat=null,rootId;
    SubscriptionPackage aPackage;
    private ArrayList<SearchData>dataArrayList;
    private ArrayList<ContentEpisodeData> episodes;
    onClickListener onClickListener;

    public ShowAllVideoAdapter(ArrayList<SearchData> list, Context context, String rootCategory, SubscriptionPackage subscription) {
        this.mContext = context;
        this.results = list;
        this.rootCat = rootCategory;
        this.aPackage=subscription;

     }

    public ShowAllVideoAdapter(ArrayList<SearchData> list, Context context, String rootCategory, SubscriptionPackage subscription,ArrayList<SearchData>data,String rootCatId,onClickListener listener) {
        this.mContext = context;
        this.results = list;
        this.rootCat = rootCategory;
        this.aPackage = subscription;
        this.dataArrayList= data;
        this.rootId = rootCatId;
        this.onClickListener= listener;

    }




    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int i) {

        return results.size();

    }

    @Override
    public long getItemId(int i) {
        return results.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        TextView video_path;
        ImageView smallThumbnailImageView;
        View listitemView = view;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(mContext).inflate(R.layout.show_all_adapter, viewGroup, false);
        }
        video_path = listitemView.findViewById(R.id.video_path);
        smallThumbnailImageView = listitemView.findViewById(R.id.thumbnail);
        SearchData subCatData = results.get(position);
        ArrayList<ContentEpisodeData> content = subCatData.getContentEpisodeData();

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

        for (int i = 0; i < content.size(); i++) {
            // titleTextView.setText(subCatData.getTitle());
           /* if (subCatData.getTitle().isEmpty()) {
                titleTextView.setText(content.get(i).getVideoTitle());
            } else {
                titleTextView.setText(subCatData.getTitle());
            }*/
            //Log.d("ShowAllcat",rootCat);

            if (rootCat.equals("Web Series")) {
                Glide.with(mContext).load(content.get(0).getBannerPath()).
                        apply(requestOptions).into(smallThumbnailImageView);
          if (content.get(0).getTrailerPath()==null){
              video_path.setText(content.get(i).getEpisodePath());
          }else{
              video_path.setText(content.get(0).getTrailerPath());
          }

            } else {
                Glide.with(mContext).load(content.get(i).getThumbnailPath()).diskCacheStrategy(DiskCacheStrategy.ALL).
                        apply(requestOptions).into(smallThumbnailImageView);
                video_path.setText(content.get(i).getEpisodePath());
            }
        }
            //int pos = i;
            smallThumbnailImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String status = "";
                    //Log.d("status12",rootCat);
                    status = new SessionManagement(mContext).SubscriptionStatus();
                    if (status.equals("Subscription Is Active")) {

                            if (rootCat.equals("Web Series")) {
                                Intent intent = new Intent(mContext, WebSeriesVideoActivity.class);
                                intent.putExtra("VideoDesc", subCatData.getDescription());
                                intent.putExtra("related", dataArrayList);
                                intent.putExtra("rootCategoryId", rootId);
                                intent.putExtra("subRootCategoryId", subCatData.getSubRootCategoryId());
                                intent.putExtra("ContentEpisode", content);
                                intent.putExtra("VideoCast", subCatData.getCast());
                                intent.putExtra("VideoDesc", subCatData.getDescription());
                                intent.putExtra("Category", rootCat);
                                intent.putExtra("type", "Home");
                                intent.putExtra("url", video_path.getText().toString());
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            }
                      else {
                            Intent intent = new Intent(mContext, ShowAllContent.class);
                            intent.putExtra("url_related",video_path.getText().toString());
                            intent.putParcelableArrayListExtra("Content_related", results);
                            intent.putExtra("type", "related");
                            intent.putExtra("rootCategoryId", rootId);
                            intent.putExtra("subRootCategoryId", subCatData.getSubRootCategoryId());
                            intent.putExtra("VideoCast", subCatData.getCast());
                            intent.putExtra("VideoDesc", subCatData.getDescription());
                            intent.putExtra("category", rootCat);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(mContext, SubscriptionActivity.class);
                        intent.putExtra("package", aPackage);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }

            });



       // }


      /*  smallThumbnailImageView.animate().rotationY(90f).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                smallThumbnailImageView.setRotationY(270f);

                smallThumbnailImageView.animate().rotationY(360f).setListener(null);
            }
        });*/
        return listitemView;
    }


   public  interface onClickListener{
        void ClickListener(String path,String title,String des);
    }
}

