package com.instaplay.flictrick.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Activity.WebSeriesVideoActivity;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ChildViewHolder> {
    private ArrayList<SearchData> results = new ArrayList<>();
    Context mContext;
    String rootcategory;
    SubscriptionPackage aPackage;
    ClickListener listener;

    public SearchAdapter(ArrayList<SearchData> list, Context context, String cat/*,ClickListener clickListener,SubscriptionPackage mPackage*/) {
        this.mContext = context;
        this.results = list;
        this.rootcategory = cat;
       // this.listener=clickListener;
       // this.aPackage= mPackage;
    }

    @NonNull
    @Override
    public SearchAdapter.ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_adapter, parent, false);
        return new SearchAdapter.ChildViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        SearchData episodeData = results.get(position);
        ArrayList<ContentEpisodeData> contentEpisodeData = episodeData.getContentEpisodeData();
        Log.d("data123", "" + episodeData.getRootCategoryName());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

            for (int i = 0; i < contentEpisodeData.size(); i++) {
                if (contentEpisodeData.get(i).getBannerPath()!=null){
                    Glide.with(mContext).load(contentEpisodeData.get(i).getBannerPath()).
                            apply(requestOptions).into(holder.smallThumbnailImageView);
                    //Log.d("thumbNail",""+contentEpisodeData.get(i).getBannerPath());
                }
                if (episodeData.getTitle().isEmpty()) {
                    holder.titleTextView.setText(contentEpisodeData.get(i).getVideoTitle());
                } else {
                    holder.titleTextView.setText(episodeData.getTitle());
                }
                int pos = i;
                holder.smallThumbnailImageView.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        //String status=new SessionManagement(mContext).SubscriptionStatus();
                        String status = "";
                        status = new SessionManagement(mContext).SubscriptionStatus();
                        if (status.equals("Subscription Is Active")){
                            Log.d("12345",episodeData.getRootCategoryName());
                            if (episodeData.getRootCategoryName().equals("Web Series")){
                                Intent intent = new Intent(mContext, WebSeriesVideoActivity.class);
                                intent.putExtra("VideoDesc", episodeData.getDescription());
                                intent.putExtra("rootCategoryId",episodeData.getRootCategoryId());
                                intent.putExtra("subRootCategoryId",episodeData.getSubRootCategoryId());
                                intent.putExtra("ContentEpisode", episodeData.getContentEpisodeData());
                                intent.putExtra("VideoTitle", episodeData.getCast());
                                intent.putExtra("VideoDesc", episodeData.getDescription());
                                intent.putExtra("Category", episodeData.getRootCategoryName());
                                intent.putExtra("type", "Search");
                                intent.putExtra("url", contentEpisodeData.get(1).getEpisodePath());
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            }else{
                                Intent intent = new Intent(mContext, WebSeriesVideoActivity.class);
                                intent.putExtra("VideoDesc", episodeData.getDescription());
                                intent.putExtra("rootCategoryId",episodeData.getRootCategoryId());
                                intent.putExtra("subRootCategoryId",episodeData.getSubRootCategoryId());

                                //intent.putExtra("related", AllContent);
                                //intent.putExtra("Package", aPackage);
                                intent.putExtra("ContentEpisode", episodeData.getContentEpisodeData());
                                intent.putExtra("VideoTitle", episodeData.getCast());
                                intent.putExtra("VideoDesc", episodeData.getDescription());
                                intent.putExtra("Category", episodeData.getRootCategoryName());
                                intent.putExtra("url", contentEpisodeData.get(1).getEpisodePath());

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            }

                            //listener.onClick(episodeData.getRootCategoryId(),episodeData.getRootCategoryName());

                        }else {
                            Intent intent = new Intent(mContext, SubscriptionActivity.class);
                            intent.putExtra("url", contentEpisodeData.get(pos).getEpisodePath());
                            intent.putExtra("package", aPackage);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }
                        /*if (rootcategory.equals("Web Series")) {
                            AppCompatActivity activity = (AppCompatActivity) view.getContext();
                            VideoFragment myFragment = new VideoFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("Video", contentEpisodeData.get(pos).getEpisodePath());
                            bundle.putString("RootCatId", results.get(holder.getAdapterPosition()).getRootCategoryId());
                            bundle.putString("SubRootCatId", results.get(holder.getAdapterPosition()).getSubRootCategoryId());
                            myFragment.setArguments(bundle);
                            activity.getSupportFragmentManager().beginTransaction().replace(R.id.flFragment, myFragment).addToBackStack(myFragment.getTag()).commit();
                            Log.d("showPosition90", String.valueOf(holder.getLayoutPosition()));
                        } */

                    }
                });
            }
        //}


    }

    @Override
    public int getItemCount() {
        return results.size();
    }


    class ChildViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private ImageView smallThumbnailImageView;

        public ChildViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.video_title);
            smallThumbnailImageView = itemView.findViewById(R.id.thumbnail);
        }
    }

    public interface ClickListener{
        void onClick(String rootCategoryId,String category);
    }


}
