package com.instaplay.flictrick.Adapter.AdapterVideo;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class EpisodeAdapter extends RecyclerView.Adapter<EpisodeAdapter.ViewHolder> /*implements Filterable*/ {
    Context mContext;
    String SubRootcat,CategoryType,rootId="";
    EpisodeChildAdapter.ClickListener clickListener;
    SubscriptionPackage subscriptionPackage;
    boolean DataStatus= false;
    @NonNull
    private RecyclerView.RecycledViewPool
            viewPool = new RecyclerView
            .RecycledViewPool();
    private ArrayList<SearchData> itemList;
    @Override
    public EpisodeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.adapter_main, parent, false);
        return new EpisodeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodeAdapter.ViewHolder holder, final int position) {
        SearchData parentItem = itemList.get(position);
       // ArrayList<SubCatData> subCatData = itemList.get(position).getSubCatData();

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ShowAllContent.class);
                intent.putExtra("type","ShowAll");
                intent.putExtra("AllData",itemList);
                intent.putParcelableArrayListExtra("Content",itemList);
                if (parentItem.getRootCategoryId()==null){
                    intent.putExtra("rootCategoryId",rootId);
                }else {
                    intent.putExtra("rootCategoryId",parentItem.getRootCategoryId());
                }
                intent.putExtra("VideoTitle",parentItem.getRootCategoryName());
                intent.putExtra("package",subscriptionPackage);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });





        // }

        // Create a layout manager
        // to assign a layout
        // to the RecyclerView.

        // Here we have assigned the layout
        // as LinearLayout with vertical orientation
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(
                holder.ChildRecyclerView
                        .getContext(),
                LinearLayoutManager.HORIZONTAL,
                false);
        // Since this is a nested layout, so
        // to define how many child items
        // should be prefetched when the
        // child RecyclerView is nested
        // inside the parent RecyclerView,
        // we use the following method
        // layoutManager.setInitialPrefetchItemCount(itemList.get(position).getEpisodeList().size());
        // Create an instance of the child
        // item view adapter and set its
        // adapter, layout manager and RecyclerViewPool


            if (CategoryType!=null) {

                if (!DataStatus) {
                    Log.d("IDCat", "" +  parentItem.getRootCategoryId());
                    holder.imageView.setVisibility(View.VISIBLE);
                    holder.ChildRecyclerView.setVisibility(View.VISIBLE);
                    if (SubRootcat.equals("46")){
                        holder.ParentItemTitle.setText("Short Films");
                    }else if (SubRootcat.equals("47")){
                        holder.ParentItemTitle.setText("Feature Films");
                    }else if (SubRootcat.equals("48")){
                        holder.ParentItemTitle.setText("Documentary");
                    }else if (SubRootcat.equals("52")){
                        holder.ParentItemTitle.setText("Comedy");
                    }else if (SubRootcat.equals("53") || SubRootcat.equals("54")){
                        holder.ParentItemTitle.setText("Animation");
                    }
                    else {
                        holder.ParentItemTitle.setText(SubRootcat);
                    }

                    Log.d("shortFilm", "" + itemList.size());
                    DataStatus= true;
                    EpisodeChildAdapter childItemAdapter = new EpisodeChildAdapter(itemList, mContext, parentItem.getRootCategoryId(), clickListener, CategoryType);
                    if (childItemAdapter!=null){
                        holder.setIsRecyclable(false);
                        holder.ChildRecyclerView.setLayoutManager(layoutManager);
                        holder.ChildRecyclerView.setAdapter(childItemAdapter);
                       // holder.ChildRecyclerView.setScrollX(10);
                        holder.ChildRecyclerView.setRecycledViewPool(viewPool);
                        childItemAdapter.notifyDataSetChanged();
                    }

                }
            }
           else
            {
                //it will work in the case of webseries episode
                if (itemList.get(position).getSubRootCategoryId().equals(SubRootcat)) {
                    holder.imageView.setVisibility(View.GONE);
                    holder.ChildRecyclerView.setVisibility(View.VISIBLE);
                    Log.d("trew12" + SubRootcat, String.valueOf(itemList.get(position).getSubRootCategoryId()));
                    holder.ParentItemTitle.setText(parentItem.getSubRootCategoryName());
                    EpisodeChildAdapter childItemAdapter = new EpisodeChildAdapter(parentItem.getContentEpisodeData(), mContext, parentItem.getRootCategoryId(), clickListener);
                    holder.ChildRecyclerView.setLayoutManager(layoutManager);
                    holder.ChildRecyclerView.setAdapter(childItemAdapter);
                    holder.ChildRecyclerView.setRecycledViewPool(viewPool);

                }
            }



        }






    // This method returns the number
    // of items we have added in the
    // ParentItemList i.e. the number
    // of instances we have created
    // of the ParentItemList
    @Override
    public int getItemCount() {

        return itemList.size();
    }




    public void setSearchResults(ArrayList<SearchData> results, Context context, String text, EpisodeChildAdapter.ClickListener listener) {
        this.itemList = results;
        this.mContext = context;
        this.SubRootcat = text;
        this.clickListener= listener;
        //this.searchDataList = searchData;
        notifyDataSetChanged();
    }

    public void setResultForOtherCategory(ArrayList<SearchData> results, Context context, String text, EpisodeChildAdapter.ClickListener listener,String type,String rootId) {
        this.itemList = results;
        this.mContext = context;
        this.SubRootcat = text;
        this.clickListener = listener;
        this.CategoryType= type;
        this.rootId = rootId;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView ChildRecyclerView;
        private TextView ParentItemTitle;
        private TextView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ChildRecyclerView = itemView.findViewById(R.id.child_recyclerview);
            ParentItemTitle = itemView.findViewById(R.id.parent_item_title);
            imageView = itemView.findViewById(R.id.img);
        }
    }
}

