package com.instaplay.flictrick.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Activity.WebSeriesVideoActivity;
import com.instaplay.flictrick.Model.Banner.BannerModel;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.SubCategoryModel;
import com.instaplay.flictrick.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends
        SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;
    private String mBanner = "";
    private List<Integer> mSliderItems = new ArrayList<>();
    private List<BannerModel> bannerSlide = new ArrayList<>();
    SubscriptionPackage subscriptionPackage;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public void renewItems(List<Integer> sliderItems, String banner) {
        this.mSliderItems = sliderItems;
        mBanner = banner;
        notifyDataSetChanged();


    }

    public void renewItems(Context mContext, List<BannerModel> sliderItems, String banner, SubscriptionPackage aPackage) {
        this.context = mContext;
        this.bannerSlide = sliderItems;
        mBanner = banner;
        this.subscriptionPackage = aPackage;
        notifyDataSetChanged();
    }


    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {

        View inflate = null;
        if (mBanner.equals("Banner")) {
            inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_adapter_layout, null);
        }/*else {
            inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout, null);

        }*/
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        if (mBanner.equals("Banner")) {
            BannerModel bannerResponse = bannerSlide.get(position);
            SubCategoryModel subCategoryModel = bannerResponse.getSubRootCategory();
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            //GetSubscriptionPlan();
            Glide.with(context).load(bannerResponse.getAppBannerPath()).into(viewHolder.imageViewBackground);
            // viewHolder.textViewDescription.setText(subCategoryModel.getTitle());
            viewHolder.imageViewBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String status = new SessionManagement(context).SubscriptionStatus();
                    if (status.equals("Subscription Is Active")) {
                        if (bannerResponse.getRootCategory().getRootCategoryName().equals("Web Series")) {
                            Intent intent = new Intent(context, WebSeriesVideoActivity.class);
                            intent.putExtra("Category", bannerResponse.getRootCategory().getRootCategoryName());
                            intent.putExtra("subRootCategoryId", bannerResponse.getSubRootCategory().getId());
                            intent.putExtra("VideoCast", bannerResponse.getSubRootCategory().getTitle());
                            intent.putExtra("VideoTitle", bannerResponse.getSubRootCategory().getTitle());
                            intent.putExtra("VideoDesc", bannerResponse.getSubRootCategory().getDescription());
                            intent.putExtra("rootCategoryId", bannerResponse.getRootCategory().getId());
                            intent.putExtra("url_related", bannerResponse.getEpisodeUrl());
                            intent.putExtra("type", "Home");
                            intent.putExtra("url", bannerResponse.getEpisodeUrl());
                           // intent.putExtra("type", "related");
                            context.startActivity(intent);

                        } else {
                            Intent intent = new Intent(context, ShowAllContent.class);
                            // intent.putExtra("aPackage_related", subscriptionPackage);
                            intent.putExtra("url_related", bannerResponse.getEpisodeUrl());
                            //intent.putParcelableArrayListExtra("Content_related", results);
                            intent.putExtra("type", "related");
                            intent.putExtra("rootCategoryId", bannerResponse.getRootCategory().getId());
                            //intent.putExtra("VideoTitle", content.getCast());
                            intent.putExtra("VideoCast", bannerResponse.getSubRootCategory().getTitle());
                            intent.putExtra("VideoDesc", bannerResponse.getSubRootCategory().getDescription());
                            intent.putExtra("category", bannerResponse.getRootCategory().getRootCategoryName());
                            if (bannerResponse.getSubRootCategory().getTrailerSubtitleFilePath().isEmpty()) {
                                intent.putExtra("subtitle", bannerResponse.getSubRootCategory().getVideoSubtitleFilePath());
                            } else {
                                intent.putExtra("subtitle", bannerResponse.getSubRootCategory().getTrailerSubtitleFilePath());

                            }

                            context.startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(context, SubscriptionActivity.class);
                        // intent.putExtra("url",bannerResponse.getEpisodeUrl());
                        intent.putExtra("package", subscriptionPackage);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

                }
            });
        } else {
            Integer sliderItem = mSliderItems.get(position);
            Glide.with(viewHolder.itemView)
                    .load(sliderItem)
                    .fitCenter()
                    .override(300, 300)
                    .into(viewHolder.imageViewBackground);


          /*  viewHolder.textViewDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/
        }
        //viewHolder.textViewDescription.setText(sliderItem.getDescription());
        //viewHolder.textViewDescription.setTextSize(16);
        //viewHolder.textViewDescription.setTextColor(Color.WHITE);

    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        if (mBanner.equals("Banner")) {
            return bannerSlide.size();
        } else {
            return mSliderItems.size();
        }

    }


    class SliderAdapterVH extends ViewHolder {

        //View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView watchNow, textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.txtMetaData);
            watchNow = itemView.findViewById(R.id.tv_auto_image_slider);
            //this.itemView = itemView;
        }
    }

    public interface BannerListener {
        void bannerListener();
    }

}
