package com.instaplay.flictrick.Adapter.AdapterVideo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class EpisodeChildAdapter extends RecyclerView.Adapter<EpisodeChildAdapter.ChildViewHolder> {
    private ArrayList<ContentEpisodeData> itemList;
    private ArrayList<SearchData> AllContent;
    Context mContext;
    private int selected_position = -1;
    ClickListener clickListener;
    String rootcategory, rootCatId, CategoryType = "";
    SubscriptionPackage subscriptionPackage;

    public EpisodeChildAdapter(ArrayList<ContentEpisodeData> list, Context context, String cat, ClickListener listener) {
        this.mContext = context;
        this.itemList = list;
        this.rootcategory = cat;
        this.clickListener = listener;

    }

    public EpisodeChildAdapter(ArrayList<SearchData> list, Context context, String cat, ClickListener listener, String type) {
        this.mContext = context;
        this.AllContent = list;
        this.rootcategory = cat;
        this.clickListener = listener;
        this.CategoryType = type;

    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_adapter, parent, false);
        return new EpisodeChildAdapter.ChildViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodeChildAdapter.ChildViewHolder holder, @SuppressLint("RecyclerView") int pos) {


        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

       // for other category
        if (!CategoryType.equals("")) {
            SearchData searchData = AllContent.get(pos);

            ArrayList<ContentEpisodeData> itemList =searchData.getContentEpisodeData();
            for (int i = 0; i < itemList.size(); i++) {
               // Log.d("test", "" + itemList.size());
                if (CategoryType.equals("Documentary")){
                    Glide.with(mContext).load(itemList.get(i).getBannerPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).apply(requestOptions).into(holder.smallThumbnailImageView);

                }else{
                    Glide.with(mContext).load(itemList.get(i).getThumbnailPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).apply(requestOptions).into(holder.smallThumbnailImageView);

                }
                int finalI1 = i;
                holder.smallThumbnailImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("clickedEpisode11", itemList.get(finalI1).getEpisodePath());

                            clickListener.onClickListener(itemList.get(finalI1).getEpisodePath(),itemList.get(finalI1).getVideoTitle(),searchData.getDescription(),null,null);




                    }


                });

            }


        } else {
                //for web series
            if (itemList.get(pos).getEpisodePath() == null) {
                holder.titleTextView.setVisibility(View.GONE);
                holder.smallThumbnailImageView.setVisibility(View.GONE);
            } else {
                holder.titleTextView.setText(itemList.get(pos).getEpisodeNumber());
                Glide.with(mContext).load(itemList.get(pos).getThumbnailPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).apply(requestOptions).into(holder.smallThumbnailImageView);
                //}
            }
            holder.smallThumbnailImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.d("clickedEpisode1", itemList.get(pos).getEpisodePath());
                    if (itemList != null) {
                        clickListener.listener(itemList.get(pos).getEpisodePath());

                    }


                }


            });

        }
        //ParentItemTitle.setVisibility(View.VISIBLE);
        //imageView.setVisibility(View.VISIBLE);

    }





    @Override
    public int getItemCount() {
        if (!CategoryType.equals("")) {
            return AllContent.size();
        } else {
            return itemList.size();
        }

    }

    class ChildViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView, video_path;
        private ImageView smallThumbnailImageView;

        public ChildViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.video_title);
            // video_path = itemView.findViewById(R.id.video_path);
            smallThumbnailImageView = itemView.findViewById(R.id.thumbnail);
        }
    }


    public interface ClickListener {
        void listener(String urlPath);
        void onClickListener(String url,String title,String des,String rootId,String subCat);

    }
}

