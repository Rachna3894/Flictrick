package com.instaplay.flictrick.Adapter.AdapterVideo;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class WebSeriesAdapter extends BaseAdapter {

    private ArrayList<ContentEpisodeData> results;
    private ArrayList<SearchData> AllContent;
    private Context mContext;
    private String rootCategory;
    private String Type = "", rootcatId;
    ClickVideo videoListener;
    SubscriptionPackage subscriptionPackage;

    public WebSeriesAdapter(ArrayList<SearchData> list, Context context, String cat, ClickVideo listener, String type, SubscriptionPackage aPackage, String rootCatId) {
        this.mContext = context;
        this.AllContent = list;
        this.rootCategory = cat;
        this.videoListener = listener;
        this.Type = type;
        this.subscriptionPackage = aPackage;
        this.rootcatId = rootCatId;
        //this.AllContent= dataArrayList;

    }


    @Override
    public int getCount() {
        return AllContent.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        TextView titleTextView, video_path;
        ImageView smallThumbnailImageView;
        View itemView = View.inflate(mContext.getApplicationContext(), R.layout.video_adapter, null);

        titleTextView = itemView.findViewById(R.id.video_title);
        video_path = itemView.findViewById(R.id.video_path);
        smallThumbnailImageView = itemView.findViewById(R.id.thumbnail);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

        SearchData searchData = AllContent.get(position);
        ArrayList<ContentEpisodeData> episodeData = searchData.getContentEpisodeData();
        for (int i = 0; i < episodeData.size(); i++) {

            if (searchData.getTitle().isEmpty()) {
                titleTextView.setText(episodeData.get(i).getVideoTitle());
            } else {
                titleTextView.setText(searchData.getTitle());
            }

            Log.d("SlideCategory",rootCategory);
            if (rootCategory.equals("Web Series")) {
                Log.d("checking12", "" + episodeData.get(0).getBannerPath());
                Glide.with(mContext).load(episodeData.get(0).getBannerPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).apply(requestOptions).into(smallThumbnailImageView);

            } else {
                video_path.setText(episodeData.get(i).getEpisodePath());
                Glide.with(mContext).load(episodeData.get(i).getThumbnailPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).
                        apply(requestOptions).into(smallThumbnailImageView);

            }
        }
        // }


        //int pos =i;

        smallThumbnailImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String status = "";
                status = new SessionManagement(mContext).SubscriptionStatus();

                // if (Type.equals("Search") && !searchData.getSubRootCategoryName().isEmpty()) {
                if (status.equals("Subscription Is Active")) {

                    if (rootCategory.equals("Web Series")) {
                        if (episodeData.get(0).getTrailerPath() == null) {
                            Log.d("TESTED", episodeData.get(position).getEpisodePath());
                            videoListener.listener(episodeData.get(position).getEpisodePath(), searchData.getTitle(), searchData.getDescription(), rootcatId, searchData.getSubRootCategoryId());
                        } else {
                            videoListener.listener(episodeData.get(0).getTrailerPath(), searchData.getTitle(), searchData.getDescription(), rootcatId, searchData.getSubRootCategoryId());
                        }
                    } else {
                        Log.d("Else_Condition", video_path.getText().toString());
                        videoListener.listener(video_path.getText().toString(), searchData.getTitle(), searchData.getDescription(), rootcatId, searchData.getSubRootCategoryId());

                    }


                } else {
                    Intent intent = new Intent(mContext, SubscriptionActivity.class);
                    intent.putExtra("package", subscriptionPackage);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            }
        });


        return itemView;
    }

    public interface ClickVideo {
        void listener(String s, String title, String des, String rootId, String subRootId);
    }
}
