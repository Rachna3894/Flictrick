package com.instaplay.flictrick.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.instaplay.flictrick.Activity.Fragment.CategoryFragment;

public class TabAdapter extends FragmentStatePagerAdapter {

        private Context myContext;
        int totalTabs;

        public TabAdapter(Context context, FragmentManager fm, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }



        @Override
        public Fragment getItem(int position) {

            CategoryFragment homeFragment = new CategoryFragment();
            return homeFragment;

        }


    @Override
    public int getCount() {
        return totalTabs;
    }
};
