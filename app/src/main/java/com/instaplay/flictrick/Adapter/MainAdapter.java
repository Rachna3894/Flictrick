package com.instaplay.flictrick.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Activity.WebSeriesVideoActivity;
import com.instaplay.flictrick.Adapter.AdapterVideo.WebSeriesAdapter;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class MainAdapter extends BaseAdapter {
    Context mContext;
    private ArrayList<SearchData> AllContent;
    private String rootCategory;
    private String Type="",rootcatId;
    WebSeriesAdapter.ClickVideo videoListener;
    SubscriptionPackage subscriptionPackage;
    ClickListener clickListener;

    public MainAdapter(ArrayList<SearchData> list, Context context, String cat, String type, SubscriptionPackage aPackage) {
        this.mContext = context;
        this.AllContent = list;
        this.rootCategory = cat;
       // this.clickListener = listener;
        this.Type= type;
        this.subscriptionPackage= aPackage;

    }


    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        View listitemView;
        TextView ParentItemTitle;
        ImageView imageView;

        listitemView = LayoutInflater.from(mContext).inflate(R.layout.video_adapter, viewGroup, false);

        ParentItemTitle = listitemView.findViewById(R.id.video_title);
        imageView = listitemView.findViewById(R.id.thumbnail);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

        SearchData searchData = AllContent.get(pos);
        ArrayList<ContentEpisodeData> episodeData = searchData.getContentEpisodeData();
        for (int i = 0; i < episodeData.size(); i++) {

            if (searchData.getTitle().isEmpty()) {
                ParentItemTitle.setText(episodeData.get(i).getVideoTitle());
            } else {
                ParentItemTitle.setText(searchData.getTitle());
            }
            if (episodeData.get(i).getBannerPath() != null) {
                Glide.with(mContext).load(episodeData.get(0).getBannerPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).apply(requestOptions).
                        into(imageView);
                Log.d("thumbNail", "" + episodeData.get(i).getBannerPath());
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String status = "";
                    status = new SessionManagement(mContext).SubscriptionStatus();
                    if (status.equals("Subscription Is Active")) {
                      //  Log.d("TESTED",rootCategory);
                        if (searchData.getRootCategoryName().equals("Web Series")) {
                            Intent intent = new Intent(mContext, WebSeriesVideoActivity.class);
                            intent.putExtra("VideoDesc", searchData.getDescription());
                            intent.putExtra("rootCategoryId", searchData.getRootCategoryId());
                            intent.putExtra("subRootCategoryId", searchData.getSubRootCategoryId());
                            intent.putExtra("VideoCast", searchData.getTitle());
                            intent.putExtra("VideoDesc", searchData.getDescription());
                            intent.putExtra("Category", searchData.getRootCategoryName());
                            intent.putExtra("type", "Search");

                            intent.putExtra("url", episodeData.get(1).getEpisodePath());

                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }else{
                            Intent intent = new Intent(mContext, ShowAllContent.class);
                            intent.putExtra("VideoDesc", searchData.getDescription());
                            intent.putExtra("rootCategoryId", searchData.getRootCategoryId());
                            intent.putExtra("subRootCategoryId", searchData.getSubRootCategoryId());
                            intent.putExtra("VideoCast", searchData.getCast());
                            intent.putExtra("category", searchData.getRootCategoryName());
                            intent.putExtra("VideoTitle",searchData.getTitle());
                            intent.putExtra("type", "related");
                            intent.putExtra("subtitle", searchData.getVideoSubtitleFilePath());
                           // intent.putExtra("type", "Search");
                            intent.putExtra("url_related", episodeData.get(0).getEpisodePath());
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }


                    }else{
                        Intent intent = new Intent(mContext, SubscriptionActivity.class);
                        intent.putExtra("package", subscriptionPackage);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }
            });
        }


        return listitemView;
    }

    // This method returns the number
    // of items we have added in the
    // ParentItemList i.e. the number
    // of instances we have created
    // of the ParentItemList

    @Override
    public int getCount() {
        return AllContent.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



    public interface ClickListener{
        void listener(String s);
    }


}
