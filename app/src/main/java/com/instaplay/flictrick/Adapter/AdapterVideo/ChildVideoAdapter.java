package com.instaplay.flictrick.Adapter.AdapterVideo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Activity.WebSeriesVideoActivity;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.SubscriptionActivity;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.Data;
import com.instaplay.flictrick.Model.Video.SubCatData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class ChildVideoAdapter extends RecyclerView.Adapter<ChildVideoAdapter.ChildViewHolder> {
    private ArrayList<SubCatData> results;
    private ArrayList<Data> AllContent;
    Context mContext;
    String rootcategory, rootCatId;
    SubscriptionPackage subscriptionPackage;

    public ChildVideoAdapter(ArrayList<SubCatData> list, Context context, SubscriptionPackage aPackage, String cat, ArrayList<Data> dataArrayList,String RootId) {
        this.mContext = context;
        this.results = list;
        this.subscriptionPackage = aPackage;
        this.rootcategory = cat;
        this.AllContent = dataArrayList;
        this.rootCatId= RootId;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_adapter, parent, false);
        return new ChildVideoAdapter.ChildViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildVideoAdapter.ChildViewHolder holder, @SuppressLint("RecyclerView") int position) {
        SubCatData content = results.get(position);
        ArrayList<ContentEpisodeData> episodeData = content.getContentEpisodeData();
        //Log.d("qwerty123",""+content.getSubRootCategoryName() +" "+episodeData.size());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(R.drawable.ic_launcher_background);

        for (int i = 0; i < episodeData.size(); i++) {
            if (rootcategory.equals("Web Series")|| rootcategory.equals("Documentary")) {
                Glide.with(mContext).load(episodeData.get(0).getBannerPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).
                        apply(requestOptions).into(holder.smallThumbnailImageView);
            } else {
                Glide.with(mContext).load(episodeData.get(i).getThumbnailPath()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).
                        apply(requestOptions).into(holder.smallThumbnailImageView);
            }
        }


            holder.smallThumbnailImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //String status = "Subscription Is Active";
                    String status = "";
                   status = new SessionManagement(mContext).SubscriptionStatus();
                    if (status.equals("Subscription Is Active")) {
                        if (rootcategory.equals("Web Series")) {
                            //while clicking on the video direct from the home screen
                            Intent intent = new Intent(mContext, WebSeriesVideoActivity.class);
                            intent.putExtra("VideoDesc", content.getDescription());
                            intent.putExtra("subtitle", content.getVideoSubtitleFilePath());
                            intent.putExtra("Package", subscriptionPackage);
                            intent.putExtra("rootCategoryId",rootCatId);
                            intent.putExtra("subRootCategoryId",content.getSubRootCategoryId());
                            intent.putExtra("VideoCast", content.getTitle());
                            intent.putExtra("type", "Home");
                            intent.putExtra("VideoDesc", content.getDescription());
                            intent.putExtra("Category", rootcategory);
                            intent.putExtra("url", episodeData.get(1).getEpisodePath());
                            /*if (episodeData.get(0).getTrailerPath()==null) {
                                intent.putExtra("url", episodeData.get(1).getEpisodePath());
                                Log.d("testingPath", "" + episodeData.get(1).getEpisodePath());

                            } else {
                                intent.putExtra("url", episodeData.get(0).getTrailerPath());
                                Log.d("testingTrailer", "" + episodeData.get(0).getTrailerPath()+""+rootCatId);

                            }*/

                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        } else {
                            Log.d("for other category", "" + episodeData.get(0).getEpisodePath());
                            //while clicking on the show all icon
                            Intent intent = new Intent(mContext, ShowAllContent.class);
                            intent.putExtra("rootCategoryId",rootCatId);
                            intent.putExtra("subRootCategoryId",content.getSubRootCategoryId());
                            intent.putExtra("aPackage_related", subscriptionPackage);
                            intent.putExtra("url_related", episodeData.get(0).getEpisodePath());
                            //intent.putParcelableArrayListExtra("Content_related", results);
                            intent.putExtra("type", "related");
                            //intent.putExtra("VideoTitle", content.getCast());
                            intent.putExtra("VideoCast", content.getTitle());
                            intent.putExtra("VideoDesc", content.getDescription());
                            intent.putExtra("category", rootcategory);
                            intent.putExtra("subtitle", content.getVideoSubtitleFilePath());
                            mContext.startActivity(intent);

                        }

                    } else {

                        Intent intent = new Intent(mContext, SubscriptionActivity.class);
                        //intent.putExtra("url", episodeData.get(finalI).getEpisodePath());
                        intent.putExtra("package", subscriptionPackage);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }
            });




    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    class ChildViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView, video_path;
        private ImageView smallThumbnailImageView;

        public ChildViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.video_title);
           // video_path = itemView.findViewById(R.id.video_path);
            smallThumbnailImageView = itemView.findViewById(R.id.thumbnail);
        }
    }


    public interface ClickListener {
        void listener(String s);
    }
}

