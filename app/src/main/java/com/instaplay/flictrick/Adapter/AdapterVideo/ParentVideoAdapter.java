package com.instaplay.flictrick.Adapter.AdapterVideo;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.instaplay.flictrick.Activity.ShowAllContent;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.Data;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.Model.Video.SubCatData;
import com.instaplay.flictrick.R;

import java.util.ArrayList;

public class ParentVideoAdapter extends RecyclerView.Adapter<ParentVideoAdapter.ViewHolder> /*implements Filterable*/ {
    Context mContext;
    String cat;
    SubscriptionPackage subscriptionPackage;
    @NonNull
    private RecyclerView.RecycledViewPool
            viewPool = new RecyclerView
            .RecycledViewPool();
    private ArrayList<Data> itemList;
    private ArrayList<SearchData>searchDataList;
    @Override
    public ParentVideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.adapter_main, parent, false);
        return new ParentVideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentVideoAdapter.ViewHolder holder, final int position) {
        Data parentItem = itemList.get(position);
        ArrayList<SubCatData> subCatData = itemList.get(position).getSubCatData();


            holder.relativeLayout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ShowAllContent.class);
                    intent.putExtra("type","ShowAll");
                    intent.putExtra("AllData",itemList);
                    intent.putParcelableArrayListExtra("Content",  subCatData);
                    intent.putExtra("VideoTitle",parentItem.getRootCategoryName());
                    intent.putExtra("rootCategoryId",parentItem.getRootCategoryId());
                    intent.putExtra("package",subscriptionPackage);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);



                }
            });



        // }

        // Create a layout manager
        // to assign a layout
        // to the RecyclerView.

        // Here we have assigned the layout
        // as LinearLayout with vertical orientation
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(
                holder.ChildRecyclerView
                        .getContext(),
                LinearLayoutManager.HORIZONTAL,
                false);
        // Since this is a nested layout, so
        // to define how many child items
        // should be prefetched when the
        // child RecyclerView is nested
        // inside the parent RecyclerView,
        // we use the following method
        // layoutManager.setInitialPrefetchItemCount(itemList.get(position).getEpisodeList().size());
        // Create an instance of the child
        // item view adapter and set its
        // adapter, layout manager and RecyclerViewPool
        //VideoAdapter childItemAdapter = new VideoAdapter(parentItem.getSubCatData(),mContext);
        //contentList.add(position,itemList.get(position).getContent());
        holder.ChildRecyclerView.setVisibility(View.VISIBLE);
        if (cat.equals(parentItem.getRootCategoryName())){


// than notify your adapter about change in list using  notifyItemMoved() method

            Log.d("AllcontentList",""+itemList.size());
            holder.imageView.setVisibility(View.VISIBLE);
            holder.ParentItemTitle.setText(parentItem.getRootCategoryName());
            ChildVideoAdapter childItemAdapter = new ChildVideoAdapter(subCatData, mContext, subscriptionPackage, parentItem.getRootCategoryName(),itemList,parentItem.getRootCategoryId());
            holder.ChildRecyclerView.setLayoutManager(layoutManager);
            holder.ChildRecyclerView.setAdapter(childItemAdapter);
            holder.ChildRecyclerView.setRecycledViewPool(viewPool);

        }else if (cat.equals("Home")){
            holder.ParentItemTitle.setText(parentItem.getRootCategoryName());
            holder.imageView.setVisibility(View.VISIBLE);
            ChildVideoAdapter childItemAdapter = new ChildVideoAdapter(subCatData, mContext, subscriptionPackage, parentItem.getRootCategoryName(),itemList,parentItem.getRootCategoryId());
            holder.ChildRecyclerView.setLayoutManager(layoutManager);
            holder.ChildRecyclerView.setAdapter(childItemAdapter);
            holder.ChildRecyclerView.setRecycledViewPool(viewPool);
        } else if (cat.equals("Search")){
            if (subCatData!=null){
                holder.ParentItemTitle.setText(parentItem.getRootCategoryName());
                holder.imageView.setVisibility(View.VISIBLE);
                ChildVideoAdapter childItemAdapter = new ChildVideoAdapter(subCatData, mContext, subscriptionPackage, parentItem.getRootCategoryName(),itemList,parentItem.getRootCategoryId());
                holder.ChildRecyclerView.setLayoutManager(layoutManager);
                holder.ChildRecyclerView.setAdapter(childItemAdapter);
                holder.ChildRecyclerView.setRecycledViewPool(viewPool);
            }/*else{
                holder.ParentItemTitle.setText(searchData.getRootCategoryName());
                holder.imageView.setVisibility(View.VISIBLE);
                SearchAdapter childItemAdapter = new SearchAdapter(searchDataList, mContext, parentItem.getRootCategoryName());
                holder.ChildRecyclerView.setLayoutManager(layoutManager);
                holder.ChildRecyclerView.setAdapter(childItemAdapter);
                holder.ChildRecyclerView.setRecycledViewPool(viewPool);
            }*/

        }




    }

    // This method returns the number
    // of items we have added in the
    // ParentItemList i.e. the number
    // of instances we have created
    // of the ParentItemList
    @Override
    public int getItemCount() {

        return itemList.size();
    }


    public void setResults(ArrayList<Data> results, Context context,SubscriptionPackage aPackage,String category) {
        this.itemList = results;
        this.mContext = context;
        this.subscriptionPackage= aPackage;
        this.cat= category;
        notifyDataSetChanged();
    }

    public void setSearchResults(ArrayList<Data> results, Context context, String text) {
        this.itemList = results;
        this.mContext = context;
        this.cat= text;
        //this.searchDataList = searchData;
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView ChildRecyclerView;
        private TextView ParentItemTitle;
        private TextView imageView;
        private RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ChildRecyclerView = itemView.findViewById(R.id.child_recyclerview);
            ParentItemTitle = itemView.findViewById(R.id.parent_item_title);
            imageView = itemView.findViewById(R.id.img);
            relativeLayout= itemView.findViewById(R.id.layout);
        }
    }
}

