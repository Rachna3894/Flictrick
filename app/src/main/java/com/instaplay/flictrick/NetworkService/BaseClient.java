package com.instaplay.flictrick.NetworkService;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.Utils;
import com.instaplay.flictrick.Model.Banner.BannerResponse;
import com.instaplay.flictrick.Model.Category.CategoryResponse;
import com.instaplay.flictrick.Model.DataResponse;
import com.instaplay.flictrick.Model.ForgotResponse;
import com.instaplay.flictrick.Model.Login.LoginModel;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.ProfileModelResponse;
import com.instaplay.flictrick.Model.ProfileRequest;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.Model.Register.RegistrationModel;
import com.instaplay.flictrick.Model.Video.SearchContentRequest;
import com.instaplay.flictrick.Model.Video.SearchRequest;
import com.instaplay.flictrick.Model.Video.SearchResponse;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseClient {
    //public static final String BASE_URL = "https://sportapi.clicknplay24.com/";
    public static final String BASE_URL = "https://flictrick.com/api/";
    public static final String PAYMENT_BASE_URL = "https://flictrick.com/api/payment/paypal";
           /* "https://flictrick.com/api/payment/paypal/savePaymentDetailsBeforePayment.php/processPaymentDetails";*/

    private APIClient bookSearchService;
    private MutableLiveData<DataResponse> volumesResponseLiveData;
    private MutableLiveData<SearchResponse> SearchResponseLiveData;
    private MutableLiveData<RegisterResponse> registerResponse;
    private MutableLiveData<LoginResponse> loginResponse;
    private MutableLiveData<ForgotResponse> forgotResponse;
    private MutableLiveData<ProfileModelResponse.ProfileResponse> profileResponseLiveData;
    private MutableLiveData<CategoryResponse> categoryResponseLiveData;
    private MutableLiveData<BannerResponse> bannerResponseMutableLiveData;
    private MutableLiveData<Boolean> progressbarObservable;


        public BaseClient() {
        registerResponse = new MutableLiveData<RegisterResponse>();
        volumesResponseLiveData = new MutableLiveData<DataResponse>();
        SearchResponseLiveData = new MutableLiveData<SearchResponse>();
        forgotResponse = new MutableLiveData<ForgotResponse>();
        loginResponse = new MutableLiveData<LoginResponse>();
        profileResponseLiveData = new MutableLiveData<>();
        categoryResponseLiveData = new MutableLiveData<>();
        bannerResponseMutableLiveData= new MutableLiveData<>();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        bookSearchService = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(APIClient.class);

    }

    public void RegisterUser(String email, String name, String mobile,String gender ,String pass,String question, String answer) {
        Gson gson = new GsonBuilder()
                 .setLenient()
                 .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        RegistrationModel model = new RegistrationModel();
        model.setEmail(email);
        model.setName(name);
        model.setAnswer(answer);
        model.setQuestion(question);
        model.setMobile(mobile);
        model.setGender(gender);
       // model.set(gender);
        byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        Log.d("encryptedPass", new String(bytesEncoded));
        model.setPassword(new String(bytesEncoded));
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.Create_new_User("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    registerResponse.postValue(response.body());
                }else{
                   // registerResponse.postValue();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("Error2", t.getMessage());
                registerResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }



   /* public void ForceLogout(String phoneNumber, String password, Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Log.d("DeviceId",phoneNumber+" PASS" +password );
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LoginModel model = new LoginModel();
        model.setMobile(phoneNumber);
         byte[] bytesEncoded = Base64.encode(password.getBytes(), 0);
        model.setPass(new String(bytesEncoded).trim());
        model.setSource("app");
        model.setDeviceId(Utils.getDeviceId(context));
        APIClient api = retrofit.create(APIClient.class);
        Call<LoginResponse> responseCall = api.ForceLogoutUser("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==",model);
        responseCall.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    loginResponse.postValue(response.body());
                    Log.d("ForceLogout", response.body().getMessage());
                }else{
                    Log.d("RESPONSE12", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("Error3", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }*/

    public void LogoutUser(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        // byte[] passencode= pass.getBytes()
        model.setToken(token);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        model.setDevice(Utils.getDeviceId(context));
        Log.d("12345",Utils.getDeviceId(context)+token);
        APIClient api = retrofit.create(APIClient.class);
        Call<LoginResponse> responseCall = api.LogoutUser(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null) {
                    Log.d("LogoutResponse", response.body().getMessage());
                    //LoginData data = response.body().getData();
                    if (response.code() == 200) {
                        loginResponse.postValue(response.body());

                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("Error4", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }



    public void ViewProfileDetails(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        // byte[] passencode= pass.getBytes()
        model.setToken(token);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<ProfileModelResponse.ProfileResponse> responseCall = api.ProfileView(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<ProfileModelResponse.ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileModelResponse.ProfileResponse> call, Response<ProfileModelResponse.ProfileResponse> response) {
                if (response.body() != null) {

                    ProfileModelResponse mData = response.body().getData();
                    //ProfileModelResponse.ProfileResponse modelResponse= response.body().getData();
                    //List<String>cat=
                    Log.d("PROFILERESPONSE", "" + mData.getCategoryPreferences().size());
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        profileResponseLiveData.postValue(response.body());

                    } else {
                        Utils.promptDialog(context, response.body().getMessage());
                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<ProfileModelResponse.ProfileResponse> call, Throwable t) {
                Log.e("ViewProfile", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {
                    ViewProfileDetails(context);
                }
            }
        });
    }


    public void UpdateProfile(Context context, String name, String email, String mobile, String gender, String ques,String answer, List<String> pref) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        ProfileRequest model = new ProfileRequest();
        SharedPreferences prefs = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        // byte[] passencode= pass.getBytes()
        model.setToken(token);
        model.setQuestion(ques);
        model.setAnswer(answer);
        model.setEmail(email);
        model.setMobile(mobile);
        model.setGender(gender);
        model.setName(name);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        model.setCategoryPreference(pref);
        APIClient api = retrofit.create(APIClient.class);
        Call<ProfileModelResponse.ProfileResponse> responseCall = api.ProfileUpdate(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<ProfileModelResponse.ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileModelResponse.ProfileResponse> call, Response<ProfileModelResponse.ProfileResponse> response) {
                if (response.body() != null) {
                    Log.d("ProfileUpdate", response.body().getMessage());
                    //ProfileModel data = response.body().getData();
                    if (response != null) {
                        profileResponseLiveData.postValue(response.body());

                    } else {
                        Utils.promptDialog(context, response.body().getMessage());
                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<ProfileModelResponse.ProfileResponse> call, Throwable t) {
                Log.e("Error1", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().contains("timeout") || t.getMessage().contains("connect timed out")) {
                    UpdateProfile(context, name, email, mobile, gender, ques,answer, pref);
                } else if (t.getMessage().contains(" failed to connect to onetracky.com/139.59.67.0 (port 443)")) {
                    UpdateProfile(context, name, email, mobile, gender, ques,answer, pref);
                }
            }
        });
    }

    public void GetCategoryy(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        // byte[] passencode= pass.getBytes()
        model.setToken(token);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<CategoryResponse> responseCall = api.GetCategory("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        categoryResponseLiveData.postValue(response.body());
                    } else {
                        Utils.promptDialog(context, response.body().getMessage());
                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Log.e("Error5", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().contains("Read timed out") && t.getMessage().equals("time out") && t.getMessage().contains("failed to connect to flictrick.com")) {
                    GetCategoryy(context);
                }
            }
        });


    }


    public void ForgotPassword(Context context, String mobile, String question, String answer) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LoginModel model= new LoginModel();
        model.setAnswer(answer);
        model.setQuestion(question);
        model.setMobile(mobile);
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<ForgotResponse> responseCall = api.ForgotPassword("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        forgotResponse.postValue(response.body());

                    } else {
                        Utils.promptDialog(context, response.body().getMessage());
                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                Log.e("ErrorForgot", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }


    public LiveData<DataResponse> getVolumesResponseLiveData() {
        return volumesResponseLiveData;
    }

    public LiveData<SearchResponse> getSearchhResponseLiveData() {
        return SearchResponseLiveData;
    }

    public LiveData<SearchResponse>getVideoContent(){
        return SearchResponseLiveData;
    }

    public LiveData<RegisterResponse> getRegisterResponseLiveData() {
        return registerResponse;
    }

    public LiveData<LoginResponse> getLoginResponseLiveData() {
        return loginResponse;
    }

    public LiveData<ForgotResponse> getForgotResponseLiveData() {
        return forgotResponse;
    }
    public LiveData<LoginResponse> getLogoutResponseLiveData() {
        return loginResponse;
    }

    public LiveData<ProfileModelResponse.ProfileResponse> ViewProfileResponseLiveData() {
        return profileResponseLiveData;
    }


    public LiveData<CategoryResponse> CategoryResponseLiveData() {
        return categoryResponseLiveData;

    }

    public LiveData<BannerResponse> BannerResponseLiveData() {

        return bannerResponseMutableLiveData;
    }

    public void BannnerList(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        SharedPreferences prefs = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        LogoutModel logoutModel = new LogoutModel();
        logoutModel.setUid(String.valueOf(UserId));
        logoutModel.setToken(token);
        logoutModel.setSource("app");


        APIClient api = retrofit.create(APIClient.class);
        Call<BannerResponse> responseCall = api.BannerList("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<BannerResponse>() {
            @Override
            public void onResponse(Call<BannerResponse> call, Response<BannerResponse> response) {
                if (response.body() != null) {
                    Log.d("BannerRESPONSE", response.body().getMessage());
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        bannerResponseMutableLiveData.postValue(response.body());
                    } else {
                        Utils.promptDialog(context, response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<BannerResponse> call, Throwable t) {
                Log.e("ErrorForgot", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);

            }
        });
    }

    public void GetContent(Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        LogoutModel logoutModel = new LogoutModel();
        logoutModel.setSource("app");

        APIClient api = retrofit.create(APIClient.class);
        Call<DataResponse> responseCall = api.GetAllContent("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {

                if (response.isSuccessful()){
                   // Log.d("ContentResponseApi",""+response.body().getData().size());
                    //ArrayList<Data> data = response.body().getData();
                    volumesResponseLiveData.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                //Log.e("ContentError", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                /*if (t.getMessage().contains("timeout") || t.getMessage().contains("connect timed out")) {
                    BannnerList(context);
                } else if (t.getMessage().contains(" failed to connect to onetracky.com/139.59.67.0 (port 443)")) {
                    BannnerList(context);
                }*/
            }
        });
    }

    public void GetSearch(Context context,String keyword) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        SearchRequest logoutModel = new SearchRequest();
        logoutModel.setSource("app");
        logoutModel.setKeyword(keyword);

        APIClient api = retrofit.create(APIClient.class);
        Call<DataResponse> responseCall = api.SearchContent("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {

                if (response.isSuccessful()){
                    Log.d("ContentResponseApi",""+response.body().getData().size());
                    //ArrayList<Data> data = response.body().getData();
                    volumesResponseLiveData.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                Log.e("ContentError", t.getMessage());
                loginResponse.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().contains("timeout") || t.getMessage().contains("connect timed out")) {
                    BannnerList(context);
                } else if (t.getMessage().contains(" failed to connect to onetracky.com/139.59.67.0 (port 443)")) {
                    BannnerList(context);
                }
            }
        });
    }

    public void GetSearchbyTitle(Context context, String keyword) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        SearchRequest logoutModel = new SearchRequest();
        logoutModel.setSource("app");
        logoutModel.setKeyword(keyword);

        APIClient api = retrofit.create(APIClient.class);
        Call<SearchResponse> responseCall = api.SearchContentbyTitle("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.isSuccessful()) {
                    Log.d("ContentResponseTitle", "" + response.body().getMessage());
                    //ArrayList<Data> data = response.body().getData();
                    SearchResponseLiveData.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Log.e("ContentError", t.getMessage());
                if (t.getMessage().contains("timeout")||t.getMessage().contains("connect timed out")) {
                    BannnerList(context);
                } else if (t.getMessage().contains(" failed to connect to onetracky.com/139.59.67.0 (port 443)")) {
                    BannnerList(context);
                }
            }
        });
    }

    public void GetSubCatRelatedContent(String rootCatId) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        SearchContentRequest contentRequest = new SearchContentRequest();
        contentRequest.setSource("app");
        contentRequest.setRootCategoryId(rootCatId);
//        progressbarObservable.setValue(true);
        APIClient api = retrofit.create(APIClient.class);
        Call<SearchResponse> responseCall = api.SearchRelatedContent("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", contentRequest);
        responseCall.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.isSuccessful()) {
//                    Log.d("ContentResponseApi", "" + response.body().getSearchData().size());
                    //ArrayList<Data> data = response.body().getData();
                    SearchResponseLiveData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Log.e("ContentError", t.getMessage());
                if (t.getMessage().contains("timeout") || t.getMessage().contains("connect timed out")) {

                } else if (t.getMessage().contains(" failed to connect to onetracky.com/139.59.67.0 (port 443)")) {

                }
            }
        });
    }
}