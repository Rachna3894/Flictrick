package com.instaplay.flictrick.NetworkService;

import com.instaplay.flictrick.Model.Banner.BannerResponse;
import com.instaplay.flictrick.Model.Category.CategoryResponse;
import com.instaplay.flictrick.Model.DataResponse;
import com.instaplay.flictrick.Model.DeviceModel;
import com.instaplay.flictrick.Model.ForgotResponse;
import com.instaplay.flictrick.Model.Login.LoginModel;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Payment.PaymentDoneResponse;
import com.instaplay.flictrick.Model.Payment.PaymentModel;
import com.instaplay.flictrick.Model.Payment.PaymentRequest;
import com.instaplay.flictrick.Model.Payment.PaymentResponse;
import com.instaplay.flictrick.Model.ProfileModelResponse;
import com.instaplay.flictrick.Model.ProfileRequest;
import com.instaplay.flictrick.Model.Register.RegistrationModel;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.Model.Subscription.SubscriptionRequest;
import com.instaplay.flictrick.Model.Subscription.SubscriptionResponse;
import com.instaplay.flictrick.Model.Video.HelpSupport;
import com.instaplay.flictrick.Model.Video.SearchContentRequest;
import com.instaplay.flictrick.Model.Video.SearchRequest;
import com.instaplay.flictrick.Model.Video.SearchResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIClient {

    // @GET("video1.php")
    @POST("getAllContent.php")
    Call<DataResponse> GetAllContent(@Header("Authorization") String header, @Body LogoutModel contentType);

    @POST("user/registration.php")
    Call<RegisterResponse> Create_new_User(@Header("Authorization") String header, @Body RegistrationModel details);

    @POST("user/login.php")
    Call<LoginResponse> LoginUser(@Header("Authorization") String header, @Body LoginModel details);

    @POST("user/login.php")
    Call<RegisterResponse> UserLoginRestrict(@Header("Authorization") String header, @Body LoginModel details);

    @POST("user/logout.php")
    Call<LoginResponse> LogoutUser(@Header("Authorization") String header, @Body LogoutModel details);

    @POST("user/force_logout.php")
    Call<LoginResponse> ForceLogoutUser(@Header("Authorization") String header, @Body LoginModel details);

    @POST("profile/profileViewById.php")
    Call<ProfileModelResponse.ProfileResponse> ProfileView(@Header("Authorization") String header, @Body LogoutModel details);

    @POST("profile/updateProfile.php")
    Call<ProfileModelResponse.ProfileResponse> ProfileUpdate(@Header("Authorization") String header, @Body ProfileRequest details);

    @POST("profile/rootCategoryList.php")
    Call<CategoryResponse> GetCategory(@Header("Authorization") String header, @Body LogoutModel details);

    @POST("user/forgot_password.php")
    Call<ForgotResponse> ForgotPassword(@Header("Authorization") String header, @Body LoginModel password);

    @POST("banner/bannerList.php")
    Call<BannerResponse> BannerList(@Header("Authorization") String header, @Body LogoutModel model);

    //Api for show package price as per the country
    @POST("subscription/subscriptionPlanDetails.php")
    Call<SubscriptionResponse>SubscriptionPlan(@Header("Authorization") String header , @Body SubscriptionRequest subscription);

    //Api will tell the user is active or not
    @POST("subscription/subscriptionPlanDetailsByUser.php")
    Call<RegisterResponse>SubscriptionStatus(@Header("Authorization") String header , @Body LogoutModel subscription);

    @POST("payment/savePaymentDetailsByUserBeforePayment.php")
    Call<PaymentResponse>PaymentDetailsSave(@Header( "Authorization") String header, @Body PaymentModel paymentModel) ;

    @POST("payment/paypal/savePaymentDetailsByUserBeforePayment.php/processPaymentDetails")
    Call<PaymentResponse> PaypalPaymentDetailsSave(@Header("Authorization") String header, @Body PaymentModel paymentModel);

    @POST("payment/paypal/updatePaymentDetails.php/updatePaymentDetails")
    Call<PaymentDoneResponse> PaypalPaymentDetailsUpdate(@Header("Authorization") String header, @Body PaymentRequest paymentModel);

    @POST("payment/updatePaymentDetails.php")
    Call<PaymentDoneResponse> PaymentDetailsUpdate(@Header("Authorization") String header, @Body PaymentRequest paymentModel);

    @POST("search.php")
    Call<DataResponse>SearchContent(@Header("Authorization") String header, @Body SearchRequest contentType);

    @POST("search.php")
    Call<SearchResponse> SearchContentbyTitle(@Header("Authorization") String header, @Body SearchRequest contentType);

    @POST("relatedContent.php")
    Call<SearchResponse> SearchRelatedContent(@Header("Authorization") String header, @Body SearchContentRequest contentType);

    @POST("support.php")
    Call<ForgotResponse>SubmitQuery(@Header("Authorization")String header, @Body HelpSupport helpSupport);

    @POST("user/get_device_status.php")
    Call<LoginResponse>GetDeviceStatus(@Header("Authorization")String header,@Body DeviceModel deviceModel);

    @POST("profile/verifyPassword.php")
    Call<RegisterResponse>VerifyPassword(@Header("Authorization")String header,@Body LogoutModel model);

    @POST("profile/deleteUser.php")
    Call<RegisterResponse> DeleteUser(@Header("Authorization") String header, @Body LogoutModel model);


}
