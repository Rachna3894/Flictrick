package com.instaplay.flictrick.ViewModel;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.ProfileModelResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;

import java.util.List;

public class ProfileViewModel extends AndroidViewModel {


    BaseClient repositiory;
    private LiveData<ProfileModelResponse.ProfileResponse> loginResponseLiveData;


    public ProfileViewModel(Application application){
        super(application);
    }
    public void init() {
        repositiory = new BaseClient();
        loginResponseLiveData = repositiory.ViewProfileResponseLiveData();
    }


    public void GetViewProfile(Context context){
        repositiory.ViewProfileDetails(context);
    }

    public void GetUpdateProfile(Context context, String name, String email, String mobile, String gender, String ques,String answer, List<String> pref){
        repositiory.UpdateProfile(context, name, email, mobile, gender, ques,answer, pref);
    }


    public LiveData<ProfileModelResponse.ProfileResponse> getProfileLiveData() {

        return loginResponseLiveData;
    }

    public LiveData<ProfileModelResponse.ProfileResponse> UpdateProfileLiveData() {

        return loginResponseLiveData;
    }
}
