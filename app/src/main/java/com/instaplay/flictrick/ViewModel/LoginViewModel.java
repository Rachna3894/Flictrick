package com.instaplay.flictrick.ViewModel;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.ForgotResponse;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.ProfileModelResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;

public class LoginViewModel extends AndroidViewModel {

    BaseClient repositiory;
    Context context;
    private LiveData<LoginResponse> loginResponseLiveData;
    private LiveData<ForgotResponse> forgotResponseLiveData;
    private LiveData<ProfileModelResponse.ProfileResponse> profileResponseLiveData;


    public LoginViewModel(Application application) {
        super(application);
    }

    public void init() {
        repositiory = new BaseClient();
        loginResponseLiveData = repositiory.getLoginResponseLiveData();
        forgotResponseLiveData = repositiory.getForgotResponseLiveData();
       // loginResponseLiveData= repositiory.getLogoutResponseLiveData();
        profileResponseLiveData= repositiory.ViewProfileResponseLiveData();

    }

    public void ForceLogout(String phoneNumber,String pass,Context context) {
        //repositiory.ForceLogout(phoneNumber,pass,context);
    }

    public void LogoutUser(Context context){
        repositiory.LogoutUser(context);
    }

    public void ForgotPassword(Context context,String mobile,String question,String answer) {
        repositiory.ForgotPassword(context,mobile,question,answer);
    }




    public LiveData<LoginResponse> getLoginResponseLiveData() {

        return loginResponseLiveData;
    }

    public LiveData<LoginResponse> getLogoutUser() {
        return loginResponseLiveData;
    }

    public LiveData<ForgotResponse> getForgotPassword() {
        return forgotResponseLiveData;
    }

}
