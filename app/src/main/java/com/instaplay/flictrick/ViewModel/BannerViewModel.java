package com.instaplay.flictrick.ViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.Banner.BannerResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;

public class BannerViewModel extends AndroidViewModel {

    BaseClient repositiory;
    private LiveData<BannerResponse> bannerResponseLiveData;

    public BannerViewModel(@NonNull Application application) {
        super(application);
    }
    public void init() {
        repositiory = new BaseClient();
        bannerResponseLiveData = repositiory.BannerResponseLiveData();
    }


    public void BannerList(Context context)
    {

        repositiory.BannnerList(context);
    }


    public LiveData<BannerResponse> GetBannerList() {

        return bannerResponseLiveData;
    }
}
