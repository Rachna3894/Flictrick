package com.instaplay.flictrick.ViewModel;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.DataResponse;
import com.instaplay.flictrick.Model.Video.SearchResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;
public class VideoViewModel extends AndroidViewModel {

    private BaseClient bookRepository;
    private LiveData<DataResponse> volumesResponseLiveData;
    private LiveData<SearchResponse> volumesSearchResponseLiveData;

    public VideoViewModel(Application application){
        super(application);
   }
    public void init() {
        bookRepository = new BaseClient();

        volumesResponseLiveData = bookRepository.getVolumesResponseLiveData();
        volumesSearchResponseLiveData = bookRepository.getSearchhResponseLiveData();
        volumesSearchResponseLiveData = bookRepository.getVideoContent();

    }



    public void GetVideoContent(Context context ) {

        bookRepository.GetContent(context);
    }

    public void GetSearchData(Context context,String keyword){
        bookRepository.GetSearch(context,keyword);

    }

    public void GetSearchbyTitle(Context context, String keyword) {
        bookRepository.GetSearchbyTitle(context, keyword);

    }

    public void GetSubCatRelatedContent( String rootCategoryId) {
        bookRepository.GetSubCatRelatedContent( rootCategoryId);

    }


    public LiveData<DataResponse> getVolumesResponseLiveData() {
        return volumesResponseLiveData;
    }

    public LiveData<DataResponse>searchContent(){
        return volumesResponseLiveData;
    }

    public LiveData<SearchResponse> searchContentbyTitle() {
        return volumesSearchResponseLiveData;
    }

    public LiveData<SearchResponse> VideoRelatedContent() {
        return volumesSearchResponseLiveData;
    }


}
