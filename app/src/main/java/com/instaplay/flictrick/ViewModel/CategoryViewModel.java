package com.instaplay.flictrick.ViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.Category.CategoryResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;

public class CategoryViewModel extends AndroidViewModel {
    BaseClient repositiory;
    private LiveData<CategoryResponse> categoryResponseLiveData;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
    }
    public void init() {
        repositiory = new BaseClient();
        categoryResponseLiveData = repositiory.CategoryResponseLiveData();
    }


    public void CategoryPreference(Context context){
        repositiory.GetCategoryy(context);
    }

    public LiveData<CategoryResponse> getPreferenceCategory() {
        return categoryResponseLiveData;
    }
}
