package com.instaplay.flictrick.ViewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.NetworkService.BaseClient;

public class RegisterViewModel extends AndroidViewModel {
    BaseClient repositiory;
    private LiveData<RegisterResponse> loginResponseLiveData;


    public RegisterViewModel(Application application){
        super(application);
    }
    public void init() {
        repositiory = new BaseClient();
        loginResponseLiveData = repositiory.getRegisterResponseLiveData();
    }

    public void CreateUserAccount(String email,String name,String mobile,String gender,String pass,String question,String answer) {

        repositiory.RegisterUser(email,name,mobile,gender,pass,question,answer);
    }

    public LiveData<RegisterResponse> getRegisterResponseLiveData() {

        return loginResponseLiveData;
    }

}
