package com.instaplay.flictrick.Paypal;

import android.app.Application;

import com.instaplay.flictrick.BuildConfig;
import com.paypal.checkout.PayPalCheckout;
import com.paypal.checkout.config.CheckoutConfig;
import com.paypal.checkout.config.Environment;
import com.paypal.checkout.config.PaymentButtonIntent;
import com.paypal.checkout.config.SettingsConfig;
import com.paypal.checkout.config.UIConfig;
import com.paypal.checkout.createorder.CurrencyCode;
import com.paypal.checkout.createorder.UserAction;

public class App extends Application {

    //private String TEST_CLIENT_ID="AXGR1aDTik2gNK_kSidHe6lC8enwscBfID45vVIwkA-5q7IDzxueEvPaErZp9bpsALlrdfgQUmJStpKm";
    private String LIVE_CLIENT_ID="AZqOaxNBb1u4tTqmoKv52YKWQsgV9C8tJZQDIosLtGBbK9o_gOgBo18Q6AeurIk4t6M8gBm1QW-KSpeL";
    private String SECRET_KEY ="ED-FgIS7b-1Q6JsQNToTYei0bZwQ8Kx4LeEq1s23NFMls0Mv8RN81wsquwjwbDUrtd3n2GyUux1NCfiA";

    @Override
    public void onCreate() {
        super.onCreate();
        PayPalCheckout.setConfig(new CheckoutConfig(
                this,
                LIVE_CLIENT_ID,
                Environment.LIVE,
                CurrencyCode.USD,
                UserAction.PAY_NOW,
                PaymentButtonIntent.CAPTURE,
                new SettingsConfig(true, false),
                new UIConfig(true),
                BuildConfig.APPLICATION_ID + "://paypalpay"
        ));

    }

}
