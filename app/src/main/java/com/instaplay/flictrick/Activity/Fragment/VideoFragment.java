package com.instaplay.flictrick.Activity.Fragment;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.HomeActivity;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Activity.SessionManagement;
import com.instaplay.flictrick.Activity.Utils;
import com.instaplay.flictrick.Activity.contract.TashieLoader;
import com.instaplay.flictrick.Adapter.AdapterVideo.ParentVideoAdapter;
import com.instaplay.flictrick.Adapter.SliderAdapter;
import com.instaplay.flictrick.Interface.TabListener;
import com.instaplay.flictrick.Model.Banner.BannerModel;
import com.instaplay.flictrick.Model.Banner.BannerResponse;
import com.instaplay.flictrick.Model.DataResponse;
import com.instaplay.flictrick.Model.DeviceModel;
import com.instaplay.flictrick.Model.Login.LoginData;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Subscription.SubscriptionRequest;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Subscription.SubscriptionResponse;
import com.instaplay.flictrick.Model.Video.Data;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.ViewModel.BannerViewModel;
import com.instaplay.flictrick.ViewModel.CategoryViewModel;
import com.instaplay.flictrick.ViewModel.LoginViewModel;
import com.instaplay.flictrick.ViewModel.VideoViewModel;
import com.instaplay.flictrick.R;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoFragment extends Fragment {
    BannerViewModel bannerViewModel;
    RecyclerView recyclerView;
    VideoViewModel videoViewModel;
    SliderAdapter adapter;
    SwipeRefreshLayout pullToRefresh;
    ParentVideoAdapter parentVideoAdapter;
    private ProgressBar progressBar;
    SubscriptionPackage subscriptionPackage;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;
    CategoryViewModel categoryViewModel;
    ArrayList<BannerModel> bannerModelList;
    LinearLayout mainLayout;
    TashieLoader tashie;
    ArrayList<Data> dataList;
    SliderView sliderView;
    SharedPreferences prefs;
    String cat = "";
    Date myDate, myDate2;
    TextView textView;
    Animation move_anim;
    static String Keyword = "", expireyDate = "";
    LoginViewModel loginViewModel;
    NotificationManager notificationManager;
    NotificationChannel notificationChannel;
    Notification.Builder builder;
    boolean isConnected = false;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    // TabLayout tabLayout;
    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    public static VideoFragment newInstance(String newText) {
        // newText = Keyword;
        VideoFragment fragment = new VideoFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoViewModel = ViewModelProviders.of(this).get(VideoViewModel.class);
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        bannerViewModel = ViewModelProviders.of(this).get(BannerViewModel.class);
        notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ((HomeActivity) getActivity()).passVal(new TabListener() {
            @Override
            public void onClick(int position, String category) {
                cat = category;
                if (isConnected) {
                    VideoList(cat);
                }


            }


        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.parent_recyclerview);
        mainLayout = view.findViewById(R.id.mainLayout);
        sliderView = view.findViewById(R.id.imageSlider);
        textView = view.findViewById(R.id.msg);
        move_anim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);
        textView.setSelected(true);
        adapter = new SliderAdapter(getActivity());
        prefs = getActivity().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();


        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getContext());
        connectionLiveData.observe(getActivity(), new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case HomeActivity.WifiData:
                            isConnected = connection.getIsConnected();
                            //Log.d("Internet status", String.valueOf(isConnected));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationMethod();
                            }
                            BannnerList();
                            GetDeviceActive();
                            GetSubscriptionPlan();
                            break;
                        case HomeActivity.MobileData:
                            BannnerList();
                            GetDeviceActive();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationMethod();
                            }
                            GetSubscriptionPlan();
                            isConnected = connection.getIsConnected();
                            break;
                    }
                } else {
                    // isConnected= connection.getIsConnected();
                    Utils.internetAlert(getContext());
                }
            }
        });





/*
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isConnected){
                    Log.d("Refresh","1234");

                    pullToRefresh.post(new Runnable() {
                                           @Override
                                           public void run() {
                                               pullToRefresh.setRefreshing(true);
                                               VideoList("Home");
                                               BannnerList();
                                               GetSubscriptionPlan();
                                           }
                                       }
                    );

                }else {
                    Utils.internetAlert(getContext());
                }

                pullToRefresh.setColorSchemeColors(Color.YELLOW);
            }

        });
*/


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentVideoAdapter = new ParentVideoAdapter();
                mainLayout.setVisibility(View.VISIBLE);
                tashie = new TashieLoader(getActivity(), 3, 10, 10, ContextCompat.getColor(getActivity(), R.color.white));
                tashie.setAnimDuration(500);
                tashie.setNoOfDots(3);
                tashie.setAnimDelay(100);
                tashie.setInterpolator(new LinearInterpolator());
                mainLayout.addView(tashie);

            }
        });

       /* LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        paginationAdapter = new PaginationAdapter(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(paginationAdapter);*/

        /*recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                //loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });*/

        //loadFirstPage();
    }

    /*private void loadNextPage() {

        movieService.getMovies().enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {
                parentVideoAdapter.removeLoadingFooter();
                isLoading = false;

                List<Data> results = response.body();
                parentVideoAdapter.addAll(results);

                if (currentPage != TOTAL_PAGES) parentVideoAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
*/

   /* private void loadFirstPage() {

        movieService.getMovies().enqueue(new Callback<List<Data>>() {
            @Override
            public void onResponse(Call<List<Data>> call, Response<List<Data>> response) {
                List<Data> results = response.body();
                progressBar.setVisibility(View.GONE);
                parentVideoAdapter.addAll(results);

                if (currentPage <= TOTAL_PAGES) parentVideoAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<Data>> call, Throwable t) {

            }

        });
    }*/

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void NotificationMethod() {
        //try {
        expireyDate = prefs.getString("endDate", "");
        if (expireyDate!="") {
            int yy = Integer.parseInt(expireyDate.substring(0, 4));
            int mm = Integer.parseInt(expireyDate.substring(5, 7));
            int dd = Integer.parseInt(expireyDate.substring(8, 10));

            Log.d("test" + expireyDate, "date" + yy + " month " + mm + "dd " + dd);
            LocalDate expiryDate = LocalDate.of(yy, mm, dd);

            // Calculate today's date
            LocalDate currentDate = LocalDate.now();

            // Calculate 7 days before the expiry date
            LocalDate sevenDaysBeforeExpiry = expiryDate.minus(7, ChronoUnit.DAYS);

            // Check if the current date is within 7 days before the expiry date
            if (currentDate.isAfter(sevenDaysBeforeExpiry) && currentDate.isBefore(expiryDate)) {
                // Show notification (this is a simple example, you can replace it with your notification mechanism)

                textView.setText("Your plan will expire on " + expireyDate );

                NotificationDialog(getActivity(), "Your plan will expire on " + expireyDate + ". Please renew soon!");
            } else if (currentDate.isAfter(sevenDaysBeforeExpiry)) {
                textView.setText("Your plan has expired on " + expireyDate );
                /*textView.setAnimation(move_anim);
                move_anim.setRepeatCount(Animation.INFINITE);*/
                NotificationDialog(getActivity(), "Your plan has expired on " + expireyDate + " Please renew soon!");
            } else {
                System.out.println("No notification");
            }
        }

    }

    public void BannnerList() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel logoutModel = new LogoutModel();
        try {

            int UserId = prefs.getInt("userId_key", 0);
            String token = prefs.getString("token_key", "");
            logoutModel.setUid(String.valueOf(UserId));
            logoutModel.setToken(token);
            logoutModel.setSource("app");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        APIClient api = retrofit.create(APIClient.class);
        Call<BannerResponse> responseCall = api.BannerList("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<BannerResponse>() {
            @Override
            public void onResponse(Call<BannerResponse> call, Response<BannerResponse> response) {
                if (response.body() != null) {
                    Log.d("BannerRESPONSE", response.body().getMessage());
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        //dialog.dismiss();
                        bannerModelList = response.body().getData();
                        adapter.renewItems(getActivity(), bannerModelList, "Banner", subscriptionPackage);
                        // Utils.Alert(getActivity(), "Hi rachna");
                        // Log.d("bannerLIST", "" + bannerModelList.size());
                    } else {
                        Utils.promptDialog(getActivity(), response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<BannerResponse> call, Throwable t) {
               // Log.e("ErrorForgot", t.getMessage());
                if (t.getMessage().contains("failed to connect to flictrick.com")) {
                    Utils.Alert(getActivity(), "Check your internet connection");
                }

            }
        });
    }
  /*  public void BannerListMethod() {
        bannerViewModel.init();
        bannerViewModel.GetBannerList().observe(getActivity(), new Observer<BannerResponse>() {
            @Override
            public void onChanged(BannerResponse bannerResponse) {
                if (bannerResponse.getStatus() == true) {
                    bannerModelList = bannerResponse.getData();
                    adapter.renewItems(getActivity(), bannerModelList, "Banner", subscriptionPackage);
                    Log.d("bannerLIST", "" + bannerModelList.size());

                }

               *//* getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        GetSubscriptionPlan();
                    }
                });*//*


                // BannerViewModel bannerViewModel= responsebannerResponse.;
                //videoViewModel.GetVideoContent("4");
            }

        });
        bannerViewModel.BannerList(getActivity());


    }*/


    public void VideoList(String cat) {
        videoViewModel.init();
        videoViewModel.getVolumesResponseLiveData().observe(getActivity(), new Observer<DataResponse>() {
            @Override
            public void onChanged(DataResponse volumesResponse) {
                if (volumesResponse != null) {

                    mainLayout.setVisibility(View.GONE);
                    //data= volumesResponse.getData();
                    dataList = volumesResponse.getData();
                    int pos = 0 + 1;
                    //pos=
                    Collections.swap(dataList, 0, 1);
                    parentVideoAdapter.notifyItemMoved(0, 1);

                    Collections.swap(dataList, 1, 2);
                    parentVideoAdapter.notifyItemMoved(1, 2);

                    Collections.swap(dataList, 2, 3);
                    parentVideoAdapter.notifyItemMoved(2, 3);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(parentVideoAdapter);
                    parentVideoAdapter.setResults(dataList, getActivity(), subscriptionPackage, cat);
                    Log.d("videoContent", "" + cat);


                }
            }
        });


        //bannerViewModel.BannerList(getActivity());
        videoViewModel.GetVideoContent(getActivity());


    }


    private void GetDeviceActive() {
        Gson gson = new GsonBuilder().setLenient().create();
        //SharedPreferences prefs = getActivity().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        // String token = prefs.getString("token_key", "");
        DeviceModel logoutModel = new DeviceModel();
        logoutModel.setUid(String.valueOf(UserId));
        logoutModel.setDeviceId(Utils.getDeviceId(getActivity()));
        logoutModel.setSource("app");
        Log.d("DeviceId", getDeviceId());
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BaseClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        APIClient api = retrofit.create(APIClient.class);
        Call<LoginResponse> responseCall = api.GetDeviceStatus("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel);
        responseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null) {

                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        LoginData data = response.body().getData();
                        if (response.body().getMessage().equals("This deviceId is not active.Please logout.")) {
                            Log.d("Check_Device_status", response.body().getMessage() + " token " + data.getToken());
                            String token = "";
                            token = data.getToken();
                            if (token != "") {
                                Log.d("Login_status", data.getToken());
                                new SessionManagement(getActivity()).UpdateToken(data.getToken());
                            }
                            promptDialog(getContext(), response.body().getMessage());
                        } else {
                            String token = "";
                            token = data.getToken();
                            if (!token.isEmpty()) {
                                Log.d("Login_status1", data.getToken());
                                new SessionManagement(getActivity()).UpdateToken(data.getToken());
                            }

                        }

                    } else {
                        Log.d("LoginError", "" + response.code());
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ErrorActiveDevice", t.getMessage());
                if (t.getMessage().contains("failed to connect to flictrick.com")) {
                    Utils.Alert(getActivity(), "Check your internet connection");
                }

            }
        });
    }


    public void Logout() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.init();
        loginViewModel.getLogoutUser().observe(this, new Observer<LoginResponse>() {
            @Override
            public void onChanged(LoginResponse loginResponse) {
                if (loginResponse != null) {

                    if (loginResponse.getCode() == 200) {
                        Toast.makeText(getActivity(), "Logout Successfully", Toast.LENGTH_SHORT).show();
                        new SessionManagement(getActivity()).LogoutDetails();
                        //startActivity(new Intent(getActivity(), LoginActivity.class));
                    } else {
                        Utils.promptDialog(getActivity(), loginResponse.getMessage());
                    }
                }
            }
        });
        loginViewModel.LogoutUser(getActivity());
    }


    public void NotificationDialog(final Context context, String resMsg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(false);
        errorDialog.setIcon(R.drawable.ic_baseline_notifications_24);
        errorDialog.setTitle("Subscription");
        errorDialog.setMessage(resMsg);
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        //Logout();
                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();


    }

    public void promptDialog(final Context context, String resMsg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(false);
        // errorDialog.setTitle("Message");
        errorDialog.setMessage(resMsg);
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Logout();
                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();


    }


    public String getDeviceId() {
        return Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.logout) {


        } else if (item.getItemId() == R.id.profile) {

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPause() {
        super.onPause();


        if (pullToRefresh != null) {
            pullToRefresh.setRefreshing(false);
            pullToRefresh.destroyDrawingCache();
            pullToRefresh.clearAnimation();
        }
    }


    public void GetSubscriptionPlan() {
        Gson gson = new GsonBuilder().setLenient().create();
        String country = new SessionManagement(getActivity()).GetUserCountry();
        SubscriptionRequest subscription = new SubscriptionRequest();
        subscription.setSource("app");
        subscription.setCountry(country);
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BaseClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        APIClient api = retrofit.create(APIClient.class);
        Call<SubscriptionResponse> responseCall = api.SubscriptionPlan("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", subscription);
        responseCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                if (response.body() != null) {
                    subscriptionPackage = response.body().getData();
                    if (bannerModelList != null) {
                        adapter.renewItems(getActivity(), bannerModelList, "Banner", subscriptionPackage);
                       /* recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(parentVideoAdapter);
                        parentVideoAdapter.setResults(dataList, getActivity(), subscriptionPackage, cat);*/
                    }

                }
            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                //Log.e("Error1", t.getMessage());
                // volumesResponseLiveData.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("time out")) {
                    //Getvideo("4");
                } else if (t.getMessage().contains("failed to connect to sportapi.clicknplay24.com/139.59.67.0 (port 443)")) {
                    if (t.getMessage().contains("failed to connect to flictrick.com")) {
                        Utils.Alert(getActivity(), "Check your internet connection");
                    }
                }

            }
        });
    }


}