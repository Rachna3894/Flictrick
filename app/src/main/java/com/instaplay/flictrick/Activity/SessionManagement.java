package com.instaplay.flictrick.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;

public class SessionManagement {
    public static final String SHARED_PREFS = "shared_prefs";
    // key for storing email.
    public static final String PhoneNo = "number_key";
    public static final String USERID = "userId_key";
    public static final String LOGGEDIN = "loggedIn";
    public static final String EMAIL = "email_key";
    public static final String SUBSCRIPTION = "subscpt_status";
    public static final String USER_STATUS = "status_key";
    public static final String ORDER_ID = "orderId";
    public static final String  SUBCRIPT_START_DATE ="startDate";
    public static final String  SUBCRIPT_END_DATE ="endDate";
    public static final  String COUNTRY="country";
    public static final  String CUSTOMERID="customerId";
    public static final  String AMOUNT="amount";
    public static final  String PACKAGE="package";
    public static final  String SOURCE="source";
    public static final String NAME = "name";
    public static final String CURRENCY="currency";
    public static final String PAYMENTSTATUS="paymentstatus";

    // key for storing token.
    public static final String TOKEN = "token_key";
    // variable for shared preferences.
    public static SharedPreferences sharedpreferences;
    Context context;


    public SessionManagement(Context cxt) {
        this.context = cxt;
        sharedpreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);

    }

    public void LoginData( String pack,String SubexpireyDate) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PACKAGE,pack);
        editor.putString(SUBCRIPT_END_DATE,SubexpireyDate);
        editor.commit();
    }

    public void LoginDetails(String token, int UserId, boolean loggedIn, String emailId, String phone,String active) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        //editor.putString(Number, number);
        editor.putString(TOKEN, token);
        editor.putInt(USERID, UserId);
        editor.putString(EMAIL, emailId);
        editor.putString(PhoneNo,phone);
        editor.putString(USER_STATUS, active);

        //editor.putString(USER_NAME, name);
        editor.putBoolean(LOGGEDIN, loggedIn);
        editor.apply();
        // starting new activity.
        Intent i = new Intent(context, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        //context.();
    }

    public void UpdateDetails(String number, String emailId, String name) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(PhoneNo, number);
        editor.putString(EMAIL, emailId);
        editor.putString(USER_STATUS, name);
        editor.commit();
        // starting new activity.
        //context.();
    }

    public void updateSubscriptionstatus(String subscriptionActive) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(SUBSCRIPTION, subscriptionActive);
        editor.commit();
    }

    public void UpdateUserCountry(String country){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(COUNTRY, country);
        editor.commit();
    }



    public void LogoutDetails() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
        Intent i = new Intent(context, LoginActivity.class);
        i.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(i);


    }

    public void SaveOrderId(String number,String date) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(ORDER_ID, number);
        editor.putString(SUBCRIPT_END_DATE,date);
        // to save our data with key and value.
        editor.apply();
    }

    public void SavePaymentDetails(String Package){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PACKAGE,Package);

        // to save our data with key and value.
        editor.apply();
    }

    public String SubscriptionStatus() {
        sharedpreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String status = sharedpreferences.getString(SUBSCRIPTION, "");
        return status;

    }

    public String GetUserCountry() {
        sharedpreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        String status = sharedpreferences.getString(COUNTRY, "");
        return status;

    }


    public static void promptDialog(Context context, String resMsg) {
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Message");
        builder1.setCancelable(true);
        builder1.setMessage(resMsg);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new SessionManagement(context).LogoutDetails();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static void promptDialogWithoutLogout(Context context, String resMsg) {
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Alert");
        builder1.setCancelable(true);
        builder1.setMessage(resMsg);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void SaveCustomerId(String customerId) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(CUSTOMERID, customerId);
        // to save our data with key and value.
        editor.apply();
    }

    public void UpdateToken(String token) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // below two lines will put values for
        // email and password in shared preferences.
        editor.putString(TOKEN, token);
        // to save our data with key and value.
        editor.commit();
    }
}
