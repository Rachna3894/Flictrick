package com.instaplay.flictrick.Activity.Fragment;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.instaplay.flictrick.Activity.ExpandableHeightGridview;
import com.instaplay.flictrick.Activity.Utils;
import com.instaplay.flictrick.Activity.contract.TashieLoader;
import com.instaplay.flictrick.Adapter.AdapterVideo.ParentVideoAdapter;
import com.instaplay.flictrick.Adapter.MainAdapter;
import com.instaplay.flictrick.Model.DataResponse;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Video.ContentEpisodeData;
import com.instaplay.flictrick.Model.Video.Data;
import com.instaplay.flictrick.Model.Video.SearchData;
import com.instaplay.flictrick.Model.Video.SearchResponse;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.VideoViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String searchKeyword;
    private VideoViewModel videoViewModel;
    private RecyclerView recyclerView;
    TextView textView;
    private RelativeLayout relativeLayout;
    private ExpandableHeightGridview gridview;
    SubscriptionPackage subscriptionPackage;
    private ArrayList<Data> dataList;
    private ArrayList<SearchData> searchDataList;
    boolean isConnected = false;
    LinearLayout mainLayout;
    TashieLoader tashie;
    private ParentVideoAdapter parentVideoAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        Log.d("SDFG", param1);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoViewModel = ViewModelProviders.of(this).get(VideoViewModel.class);

      /*  ConnectionLiveData connectionLiveData = new ConnectionLiveData(getContext());
        connectionLiveData.observe(getActivity(), new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case HomeActivity.WifiData:
                            isConnected = true;
                            break;
                        case HomeActivity.MobileData:
                            isConnected = true;
                            break;
                    }
                } else {
                    isConnected = false;
                    Utils.internetAlert(getContext());
                }
            }
        });*/

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView = view.findViewById(R.id.parent_recyclerview);
        mainLayout = view.findViewById(R.id.mainLayout);
        gridview = view.findViewById(R.id.searchGrid);
        textView = view.findViewById(R.id.no_data);
        relativeLayout= view.findViewById(R.id.layout);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parentVideoAdapter = new ParentVideoAdapter();
                mainLayout.setVisibility(View.VISIBLE);
                tashie = new TashieLoader(getActivity(), 3, 30, 10, ContextCompat.getColor(getActivity(), R.color.white));
                tashie.setAnimDuration(500);
                tashie.setNoOfDots(3);
                tashie.setAnimDelay(100);
                tashie.setInterpolator(new LinearInterpolator());
                mainLayout.addView(tashie);

            }
        });
        if (getArguments() != null) {
            searchKeyword = getArguments().getString("SearchWord");
            subscriptionPackage =getArguments().getParcelable("Package");
            Log.d("SDFG", searchKeyword);
            if (Utils.isNetworkAvailable(getActivity())) {
                if (dataList==null){
                    SearchByTitle(searchKeyword);

                }else{
                    SearchContentList(searchKeyword);
                }

            } else {
                Utils.internetAlert(getActivity());
            }

        }
        return view;

    }

    public ArrayList<Data> SearchContentList(String keyword) {
        videoViewModel.init();
        videoViewModel.searchContent().observe(getActivity(), new Observer<DataResponse>() {
            @Override
            public void onChanged(DataResponse volumesResponse) {
                if (volumesResponse != null) {
                    dataList = volumesResponse.getData();
                    if (dataList != null) {
                        recyclerView.setVisibility(View.VISIBLE);
                        gridview.setVisibility(View.GONE);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        parentVideoAdapter = new ParentVideoAdapter();
                        mainLayout.setVisibility(View.GONE);
                        recyclerView.setAdapter(parentVideoAdapter);
                        Log.d("12345", "" + dataList.size());
                        parentVideoAdapter.setSearchResults(dataList, getActivity(), "Search");
                        //parentVideoAdapter.setResults(dataList,getActivity(), null,"Search");
                    }else {
                        Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                        textView.setVisibility(View.VISIBLE);
                        mainLayout.setVisibility(View.GONE);
                        relativeLayout.setVisibility(View.GONE);
                        gridview.setVisibility(View.GONE);

                    }


                }
            }
        });
        videoViewModel.GetSearchData(getContext(), keyword);
        return dataList;

    }

    public void SearchByTitle(String keyword) {

        videoViewModel.init();
        videoViewModel.searchContentbyTitle().observe(getActivity(), new Observer<SearchResponse>() {
            @Override
            public void onChanged(SearchResponse volumesResponse) {
                if (volumesResponse != null) {
                    gridview.setVisibility(View.VISIBLE);

                    searchDataList = volumesResponse.getSearchData();
                    if (searchDataList != null) {
                            ArrayList<ContentEpisodeData> searchData = searchDataList.get(0).getContentEpisodeData();
                            for (int i = 0; i < searchDataList.size(); i++) {
                                if (searchData != null) {
                                    gridview.setExpanded(true);
                                    recyclerView.setVisibility(View.GONE);
                                    textView.setVisibility(View.GONE);
                                    // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    //Log.d("asdfg", "" + searchDataList.size());
                                    mainLayout.setVisibility(View.GONE);
                                    MainAdapter bannerVideoAdapter = new MainAdapter(searchDataList,
                                            getApplicationContext(),
                                            searchDataList.get(i).getRootCategoryName(),
                                             "Search", subscriptionPackage);
                                    gridview.setAdapter(bannerVideoAdapter);
                                    //gridEpisode!!.expanded = true

                              /*  SearchAdapter parentVideoAdapter = new SearchAdapter(searchDataList, getActivity(),searchDataList.get(i).getRootCategoryName());
                                recyclerView.setAdapter(parentVideoAdapter);
                                parentVideoAdapter.notifyDataSetChanged();*/
                                }
                                    else {
                                    SearchContentList(searchKeyword);
                                }


                        }

                    }else {
                        textView.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);


                    }


                }else {
                    textView.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);


                }
            }
        });
        videoViewModel.GetSearchbyTitle(getContext(), keyword);

    }



}