package com.instaplay.flictrick.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.instaplay.flictrick.R;


public class SplashActivity extends Activity /*implements View.OnClickListener*/{

    boolean UserLoggedin;
    ImageView txtWhatsApp;
    LinearLayout txtLoginIn;
    private LoginManager loginManager;
    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
          SharedPreferences prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
                UserLoggedin = prefs.getBoolean("loggedIn", false);
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (UserLoggedin){
                                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                                startActivity(i);
                               //GetDeviceActive();
                            } else {
                                Intent i = new Intent(getApplicationContext(), RegistrationActivity.class);
                                startActivity(i);
                                //startActivity(new Intent(getApplicationContext(),RegistrationActivity.class));
                                //facebookLogin();
                            }
                        }
                    },1000);
                }else {
                    Utils.internetAlert(SplashActivity.this);
                }


    }





    /*private void facebookLogin() {

        loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();

        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                       *//* GraphRequest request = GraphRequest.newMeRequest(

                                loginResult.getAccessToken(),

                                new GraphRequest.GraphJSONObjectCallback() {

                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response)
                                    {

                                        if (object != null) {
                                            try {
                                                String name = object.getString("name");
                                                String email = object.getString("email");
                                                String fbUserID = object.getString("id");

                                                Log.d("HelloFacebook",name +"qwerty"+email +"`1"+fbUserID);

                                               // disconnectFromFacebook();

                                                // do action after Facebook login success
                                                // or call your API
                                            }
                                            catch (JSONException | NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString(
                                "fields",
                                "id, name, email, gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();*//*
                    }


                    @Override
                    public void onCancel() {
                        startActivity(new Intent(getApplicationContext(),SplashActivity.class));
                        finish();
                        Log.v("LoginScreen", "---onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        // here write code when get error
                        Log.v("LoginScreen", "----onError: "
                                + error.getMessage());
                    }
                });
    }*/

    /*public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info
                    = getPackageManager().getPackageInfo(
                    "com.mobpro.flictrick",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(
                                md.digest(),
                                Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e){
        }
        catch (NoSuchAlgorithmException e) {
        }
    }*/

   /* @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.viaFacebook:
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    loginManager.logInWithReadPermissions(
                            SplashActivity.this,
                            Arrays.asList(
                                    "email", "public_profile "));

                    // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("name", "profile_picture"));
                } else {
                    Utils.internetAlert(getApplicationContext());
                }
                break;
            case R.id.whatsApp:

                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }

    }*/

    /*public void disconnectFromFacebook()
    {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions/",
                null,
                HttpMethod.DELETE,
                new GraphRequest
                        .Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse)
                    {
                        LoginManager.getInstance().logOut();
                    }
                })
                .executeAsync();
    }*/

}