package com.instaplay.flictrick.Activity;

import static android.content.Context.UI_MODE_SERVICE;

import android.app.UiModeManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.LocaleList;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AlertDialog;
import com.instaplay.flictrick.BuildConfig;
import com.instaplay.flictrick.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

public class Utils {
    public static final String FEATURE_FIRE_TV = "amazon.hardware.fire_tv";
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^[a-zA-Z0-9]{8}$");
    public static final String[] supportedExtensionsVideo = new String[]{"3gp", "avi", "m4v", "mkv", "mov", "mp4", "ts", "webm"};
    public static final String[] supportedExtensionsSubtitle = new String[]{"srt", "ssa", "ass", "vtt", "ttml", "dfxp", "xml"};



    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static float pxToDp(float px) {
        return px / Resources.getSystem().getDisplayMetrics().density;
    }


    public static boolean validateEmail(EditText etMail) {

        // Extract input from EditText
        String emailInput = etMail.getText().toString().trim();

        // if the email input field is empty
        if (emailInput.isEmpty()) {
            etMail.setError("Please enter the email Id");
            return false;
        }

        // Matching the input email to a predefined email pattern
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            etMail.setError("Please enter a valid email address");
            return false;
        } else {
            etMail.setError(null);
            return true;
        }
    }


    public static boolean isValidPhoneNumber(String phoneNumber,EditText editText) {
        // Regex to check valid phone number.
        String pattern = "^[+]{1}(?:[0-9\\-\\(\\)\\/" +
                "\\.]\\s?){6,15}[0-9]{1}$";

        // If the phone number is empty return false
        if (phoneNumber.isEmpty()) {
           // editText.setError("Phone field can't be empty");
            return false;
        }

        // Return true if the phone number
        // matched the Regex
        if (Pattern.matches(pattern, phoneNumber)) {
           // Log.d("pattern"+pattern,editText.getText().toString());
            return true;
        } else {
            //editText.setError("Please check the phone number or country code");
            return false;
        }
    }
    public static boolean checkForMobile(EditText edNumber) {
        Context c;
        // EditText mEtMobile=(EditText)findViewById(R.id.etMobile);
        String mStrMobile = edNumber.getText().toString();
       /* if (android.util.Patterns.PHONE.matcher(mStrMobile).matches()) {
            return true;
        }*/

        if (mStrMobile.length() != 10 || mStrMobile.length() != 9 || mStrMobile.length() == 7) {
            edNumber.setError("Phone number isn't valid");
            return true;

        } else if (mStrMobile == null && mStrMobile.isEmpty()) {
            edNumber.setError("Field can't be empty");
            return false;

        }

        return false;


    }


    public static String getDeviceId(Context mContext)
    {
        return  Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }



    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) for (int i = 0; i < info.length; i++)
                if (info[i].isConnectedOrConnecting()) {

                    return true;
                }

        }
        return false;
    }


    public static boolean validatePassword(EditText edPass) {
        String passwordInput = edPass.getText().toString().trim();
        // if password field is empty
        // it will display error message "Field can not be empty"
        if (passwordInput.isEmpty()) {
            edPass.setError("Field can not be empty");
            return false;
        }


        // if password does not matches to the pattern
        // it will display an error message "Password is too weak"

        if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            edPass.setError("Password is too weak");
            return false;
        } else {
            edPass.setError(null);
            return true;
        }

    }

    public static boolean isValidateAge(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            editText.setError("Field can not be empty");
            return false;
        }
        return true;
    }


    public static void promptDialog(final Context context, String resMsg) {
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builder1.setTitle("Message");
        builder1.setCancelable(false);
        builder1.setMessage(resMsg);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                            context.startActivity(new Intent(context, LoginActivity.class));



                    }
                });



    }

    public static void popUp(final Context context, String resMsg) {
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert).setCancelable(true);
        builder1.setTitle("Message");
        builder1.setCancelable(true);
        builder1.setMessage(resMsg);
        builder1.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new SessionManagement(context).LogoutDetails();
                        context.startActivity(new Intent(context, LoginActivity.class));


                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }



    public static void internetAlert(final Context context) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert).setCancelable(true);
        errorDialog.setTitle("No internet connection");
        errorDialog.setMessage("You are offline please check your internet connection.");
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        // ((Activity)(context)).finish();


                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();

    }

    public static void Alert(final Context context, String msg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(false);
        errorDialog.setTitle("Message");
        errorDialog.setMessage(msg);
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("AlertMsg",msg);
                        dialog.dismiss();
                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();

    }





   /* public static boolean isValidPhoneNumber(EditText edPhone) {
        String phoneNumber = edPhone.getText().toString();
        if (phoneNumber.length()==12){
            return true;
        }else{
            edPhone.setError("Please enter 10 digit number");
        }
        return false;
    }*/




    public static String getFileName(Context context, Uri uri) {
        String result = null;
        try {
            if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
                try (Cursor cursor = context.getContentResolver().query(uri, new String[]{OpenableColumns.DISPLAY_NAME}, null, null, null)) {
                    if (cursor != null && cursor.moveToFirst()) {
                        final int columnIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        if (columnIndex > -1)
                            result = cursor.getString(columnIndex);
                    }
                }
            }
            if (result == null) {
                result = uri.getPath();
                int cut = result.lastIndexOf('/');
                if (cut != -1) {
                    result = result.substring(cut + 1);
                }
            }
            if (result.indexOf(".") > 0)
                result = result.substring(0, result.lastIndexOf("."));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isVolumeMax(final AudioManager audioManager) {
        return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    public static boolean isVolumeMin(final AudioManager audioManager) {
        int min = Build.VERSION.SDK_INT >= 28 ? audioManager.getStreamMinVolume(AudioManager.STREAM_MUSIC) : 0;
        return audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == min;
    }


    public enum Orientation {
        VIDEO(0, R.string.video_orientation_video),
        SYSTEM(1, R.string.video_orientation_system),
        UNSPECIFIED(2, R.string.video_orientation_system);

        public final int value;
        public final int description;

        Orientation(int type, int description) {
            this.value = type;
            this.description = description;
        }
    }


    public static Orientation getNextOrientation(Orientation orientation) {
        switch (orientation) {
            case VIDEO:
                return Orientation.SYSTEM;
            case SYSTEM:
            default:
                return Orientation.VIDEO;
        }
    }



    public static void log(final String text) {
        if (BuildConfig.DEBUG) {
            Log.d("JustPlayer", text);
        }
    }

    public static void setViewMargins(final View view, int marginLeft, int marginTop, int marginRight, int marginBottom) {
        final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        view.setLayoutParams(layoutParams);
    }



    public static boolean isTvBox(Context context) {
        final PackageManager pm = context.getPackageManager();

        // TV for sure
        UiModeManager uiModeManager = (UiModeManager) context.getSystemService(UI_MODE_SERVICE);
        if (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
            return true;
        }

        if (pm.hasSystemFeature(FEATURE_FIRE_TV)) {
            return true;
        }

        // Missing Files app (DocumentsUI) means box (some boxes still have non functional app or stub)
        if (!hasSAFChooser(pm)) {
            return true;
        }

        // Legacy storage no longer works on Android 11 (level 30)
        if (Build.VERSION.SDK_INT < 30) {
            // (Some boxes still report touchscreen feature)
            if (!pm.hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN)) {
                return true;
            }

            if (pm.hasSystemFeature("android.hardware.hdmi.cec")) {
                return true;
            }

            if (Build.MANUFACTURER.equalsIgnoreCase("zidoo")) {
                return true;
            }
        }

        // Default: No TV - use SAF
        return false;
    }

    public static boolean hasSAFChooser(final PackageManager pm) {
        final Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");
        return intent.resolveActivity(pm) != null;
    }

    public static int normRate(float rate) {
        return (int) (rate * 100f);
    }


    public static boolean isPiPSupported(Context context) {
        PackageManager packageManager = context.getPackageManager();
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && packageManager.hasSystemFeature(PackageManager.FEATURE_PICTURE_IN_PICTURE);
    }

    public static Uri getMoviesFolderUri() {
        Uri uri = null;
        if (Build.VERSION.SDK_INT >= 26) {
            final String authority = "com.android.externalstorage.documents";
            final String documentId = "primary:" + Environment.DIRECTORY_MOVIES;
            uri = DocumentsContract.buildDocumentUri(authority, documentId);
        }
        return uri;
    }

    public static boolean isProgressiveContainerUri(final Uri uri) {
        String path = uri.getPath();
        if (path == null) {
            return false;
        }
        path = path.toLowerCase();
        for (String extension : supportedExtensionsVideo) {
            if (path.endsWith(extension)) {
                return true;
            }
        }
        return false;
    }

    public static String[] getDeviceLanguages() {
        final List<String> locales = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 24) {
            final LocaleList localeList = Resources.getSystem().getConfiguration().getLocales();
            for (int i = 0; i < localeList.size(); i++) {
                locales.add(localeList.get(i).getISO3Language());
            }
        } else {
            final Locale locale = Resources.getSystem().getConfiguration().locale;
            locales.add(locale.getISO3Language());
        }
        return locales.toArray(new String[0]);
    }

    public static ComponentName getSystemComponent(Context context, Intent intent) {
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
        if (resolveInfos.size() < 2) {
            return null;
        }
        int systemCount = 0;
        ComponentName componentName = null;
        for (ResolveInfo resolveInfo : resolveInfos) {
            int flags = resolveInfo.activityInfo.applicationInfo.flags;
            boolean system = (flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            if (system) {
                systemCount++;
                componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
            }
        }
        if (systemCount == 1) {
            return componentName;
        }
        return null;
    }

    public static float normalizeScaleFactor(float scaleFactor, float min) {
        return Math.max(min, Math.min(scaleFactor, 2.0f));
    }


    public static boolean isTablet(Context context) {
        return context.getResources().getConfiguration().smallestScreenWidthDp >= 720;
    }

    public static <K, V> void orderByValue(LinkedHashMap<K, V> m, final Comparator<? super V> c) {
        List<Map.Entry<K, V>> entries = new ArrayList<>(m.entrySet());
        Collections.sort(entries, (lhs, rhs) -> c.compare(lhs.getValue(), rhs.getValue()));
        m.clear();
        for (Map.Entry<K, V> e : entries) {
            m.put(e.getKey(), e.getValue());
        }
    }


}
