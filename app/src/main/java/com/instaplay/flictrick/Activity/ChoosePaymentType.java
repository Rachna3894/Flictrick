package com.instaplay.flictrick.Activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Model.Payment.PaymentData;
import com.instaplay.flictrick.Model.Payment.PaymentDoneResponse;
import com.instaplay.flictrick.Model.Payment.PaymentModel;
import com.instaplay.flictrick.Model.Payment.PaymentRequest;
import com.instaplay.flictrick.Model.Payment.PaymentResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.paypal.checkout.approve.Approval;
import com.paypal.checkout.approve.OnApprove;
import com.paypal.checkout.createorder.CreateOrder;
import com.paypal.checkout.createorder.CreateOrderActions;
import com.paypal.checkout.createorder.CurrencyCode;
import com.paypal.checkout.createorder.OrderIntent;
import com.paypal.checkout.createorder.UserAction;
import com.paypal.checkout.order.Amount;
import com.paypal.checkout.order.AppContext;
import com.paypal.checkout.order.CaptureOrderResult;
import com.paypal.checkout.order.OnCaptureComplete;
import com.paypal.checkout.order.OrderRequest;
import com.paypal.checkout.order.PurchaseUnit;
import com.paypal.checkout.paymentbutton.PaymentButtonContainer;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.googlepaylauncher.GooglePayEnvironment;
import com.stripe.android.googlepaylauncher.GooglePayLauncher;
import com.stripe.android.paymentsheet.PaymentSheet;
import com.stripe.android.paymentsheet.PaymentSheetResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChoosePaymentType extends AppCompatActivity  {
    CardView cardStripe,cardPaypal;
    String Package="",Price="",Currency="",CurrentDate,ExpiryDate;
    ProgressDialog dialog;
    public static final int MobileData = 2;
    public static final int WifiData = 1;
    boolean isConnect= false;
    SharedPreferences prefs;
    String Secret_key,token, email, Phnumber, currentDate, expiryDate, PaymentStatus="",resourceId,paymentId;
    String Publisher_key = "",OrderId;
    int Uid;
    Toolbar toolbar;
    PaymentButtonContainer paymentButtonContainer;
    private String ClientSecretKey, CustomerID, EphericalKey,P_Id;
    PaymentSheet paymentSheet;
    LinearLayout  linearLayout;
    GooglePayLauncher googlePayLauncher;
    PaymentSheet.GooglePayConfiguration googlePayConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment_type);
        cardStripe = findViewById(R.id.stripeCard);
        toolbar= findViewById(R.id.toolbar);
        paymentButtonContainer = findViewById(R.id.payment_button_container);
        linearLayout= findViewById(R.id.stripeLayout);
        Package =getIntent().getStringExtra("Package");
        Price =getIntent().getStringExtra("Price");
        Currency =getIntent().getStringExtra("Currency");
        CurrentDate =getIntent().getStringExtra("CurrentDate");
        ExpiryDate =getIntent().getStringExtra("ExpiryDate");
        Publisher_key= getString(R.string.Publisher_key);
        PaymentConfiguration.init(this, Publisher_key);
        Log.d("Currency12",Currency);

       /* linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ClickStrip","yup");
                if (isConnect) {
                    dialog= new ProgressDialog(ChoosePaymentType.this);
                    dialog.setTitle("Please wait");
                    dialog.setMessage("Loading...");
                    dialog.setCancelable(false);
                    dialog.show();
                    GetCustomerId(dialog);
                } else {
                    Utils.isNetworkAvailable(getApplicationContext());
                }
            }
        });*/


        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Currency.equals("USD")||Currency.equals("usd")){
                paymentButtonContainer.setup(
                        new CreateOrder() {
                            @Override
                            public void create(@NotNull CreateOrderActions createOrderActions) {

                                ArrayList<PurchaseUnit> purchaseUnits = new ArrayList<>();
                                purchaseUnits.add(
                                        new PurchaseUnit.Builder()
                                                .amount(
                                                        new Amount.Builder()
                                                                .currencyCode(CurrencyCode.USD)
                                                                .value(Price)
                                                                .build()
                                                ).description("Monthly Pack")
                                                .build()
                                );
                                OrderRequest order = new OrderRequest(
                                        OrderIntent.CAPTURE,
                                        new AppContext.Builder()
                                                .userAction(UserAction.PAY_NOW)
                                                .build(), purchaseUnits
                                );

                                Log.d(TAG, "create:" + String.format("CaptureOrderResult: %s", purchaseUnits.get(0).component1()));

                                createOrderActions.create(order, (CreateOrderActions.OnOrderCreated) null);


                            }
                        },
                        new OnApprove() {
                            @Override
                            public void onApprove(@NotNull Approval approval) {
                                Log.d(TAG, String.format("Approveal: %s", approval.getData()));
                                //SavePaymentDetailsBeforeForPaypal(approval.getData().getOrderId());
                                SavePaypalPaymentDeatils(approval.getData().getOrderId());
                                approval.getOrderActions().capture(new OnCaptureComplete() {

                                    @Override
                                    public void onCaptureComplete(@NotNull CaptureOrderResult result) {
                                        Log.d(TAG, String.format("CaptureComplete: %s", result));
                                        String[] paymetDetails = String.format("CaptureComplete: %s", result).split(",");
                                        if (paymetDetails[0].contains("Success")) {
                                            Log.d("splitData", paymetDetails[0] + "1" + paymetDetails[16]);
                                            String[] afterspilt = paymetDetails[0].split("=");
                                            String resourceId = afterspilt[2];
                                            String[] afterspilt1 = paymetDetails[16].split("=");
                                            String paymentId = afterspilt1[3];
                                            //String OrderId = prefs.getString("orderId","");
                                            Log.d("afterSplit", "abc " + afterspilt[2] + " abcd " + afterspilt1[3]);
                                            Log.d("resoucreID ", resourceId + "Paymentid " + paymentId);
                                            PayPalUpdatePaymentDetailsAfterDone("Completed", paymentId, resourceId);
                                            showAlertDialogButtonClicked("Success", Price, Package);
                                        } else {
                                            if (paymetDetails[0].contains("Error")) {
                                                showAlertDialogButtonClicked("Error", Price, Package);
                                            }
                                        }
                                        // PayPalUpdatePaymentDetailsAfterDone();
                                        // Toast.makeText(ChoosePaymentType.this, String.format("CaptureOrderResult: %s", result), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }

                );
            }else {
                    Alert(getApplicationContext(),"Sorry, we are not currently supporting INR as Currency.");
                }
        }
        });



        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()){
                        case WifiData:
                            isConnect=connection.getIsConnected();
                            break;
                        case MobileData:
                            isConnect=connection.getIsConnected();
                            break;
                    }
                } else {
                    isConnect=connection.getIsConnected();
                    Utils.internetAlert(ChoosePaymentType.this);
                }
            }
        });

        paymentSheet = new PaymentSheet(this, paymentSheetResult -> {
            onPaymentResult(paymentSheetResult);
        });

        googlePayConfiguration =
                new PaymentSheet.GooglePayConfiguration(
                        PaymentSheet.GooglePayConfiguration.Environment.Production,
                        "US"
                );
        googlePayLauncher = new GooglePayLauncher(
                this,
                new GooglePayLauncher.Config(
                        GooglePayEnvironment.Production,
                        "US",
                        "Flictrick"
                ),
                this::onGooglePayReady,
                this::onGooglePayResult
        );






    }

    public void Alert(final Context context, String msg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(ChoosePaymentType.this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(false);
        errorDialog.setTitle("Message");
        errorDialog.setMessage(msg);
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("EnterDialog", "ok");
                        Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(new Intent(getApplicationContext(),HomeActivity.class));

                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();

    }
    private void onGooglePayReady(boolean isReady) {
        cardStripe.setEnabled(isReady);
        // implemented below
    }
    private void onGooglePayResult(@NotNull GooglePayLauncher.Result result) {
        if (result instanceof GooglePayLauncher.Result.Completed) {
            // Payment succeeded, show a receipt view
        } else if (result instanceof GooglePayLauncher.Result.Canceled) {
            // User canceled the operation
        } else if (result instanceof GooglePayLauncher.Result.Failed) {
            // Operation failed; inspect `result.getError()` for more details
        }
    }
    private void paymentFlow() {
        paymentSheet.presentWithPaymentIntent(
                ClientSecretKey,  // Client SecretKey
                new PaymentSheet.Configuration(
                        "Flictrick",
                        new PaymentSheet.CustomerConfiguration(
                                CustomerID, EphericalKey), googlePayConfiguration));

        SavePaymentDetailsBeforeDone();

    }
    public void GetCustomerId(ProgressDialog progressDialog) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/customers", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    CustomerID = object.getString("id");
                    Log.d("CustomerID",CustomerID);
                    //Toast.makeText(SubscriptionActivity.this, CustomerID, Toast.LENGTH_SHORT).show();
                    new SessionManagement(getApplicationContext()).SaveCustomerId(CustomerID);
                    getEphemeralKey(CustomerID,progressDialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " +Secret_key);
                return headers;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void getEphemeralKey(String customerID, ProgressDialog progressDialog) {
        // Log.d("CustomerID", customerID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/ephemeral_keys", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    EphericalKey = object.getString("id");
                    Log.d("qwerty", EphericalKey +customerID);
                    progressDialog.dismiss();
                    getClientSecret(customerID);
                    //Toast.makeText(SubscriptionActivity.this, EphericalKey, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ChoosePaymentType.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " +Secret_key);
                headers.put("Stripe-Version", "2023-08-16");//header
                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", CustomerID);//passing customer in the form endcoded url body
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void getClientSecret(String customerID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/payment_intents", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);

                    ClientSecretKey = object.getString("client_secret");
                    P_Id= object.getString("id");
                    if (ClientSecretKey != null) {
                        paymentFlow();
                    }

                    // Toast.makeText(SubscriptionActivity.this, ClientSecretKey, Toast.LENGTH_SHORT).show();
                    Log.d("qwerty123", ClientSecretKey);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " + Secret_key);
                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                int price = Integer.parseInt(Price);
                int val = price * 100;
                Log.d("CustomerID1",customerID);
                params.put("customer", customerID);
                params.put("amount", String.valueOf(val));
                params.put("currency", Currency);
                // params.put("automatic_payment_methods[enabled]", "true");
                Log.d("Price", String.valueOf(val));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }


    public void onPaymentResult(PaymentSheetResult paymentSheet) {
        if (paymentSheet instanceof PaymentSheetResult.Canceled) {
            Log.d(TAG, "Canceled");
            // UpdatePaymentDetailsAfterDone("Canceled");
            //showAlertDialogButtonClicked("Canceled", priceType, Package);
        } else if (paymentSheet instanceof PaymentSheetResult.Failed) {
            showAlertDialogButtonClicked("Failed", Price, Package);
            UpdatePaymentDetailsAfterDone("Failed");
            Log.e(TAG, "Got_error: ", ((PaymentSheetResult.Failed) paymentSheet).getError());
        } else if (paymentSheet instanceof PaymentSheetResult.Completed) {
            showAlertDialogButtonClicked("Success", Price, Package);
            UpdatePaymentDetailsAfterDone("Success");
            Log.d(TAG, "Completed");
            //Log.d(TAG,PaymentSheetResult.);
        }
    }


    public void showAlertDialogButtonClicked(String status, String price, String Package) {
        // Create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        //builder.setTitle("Name");
        final View customLayout = getLayoutInflater().inflate(R.layout.payment_popup, null);
        builder.setView(customLayout);
        ImageView imageView = customLayout.findViewById(R.id.img);
        TextView paymentMsg = customLayout.findViewById(R.id.paymentMsg);
        TextView paymentprice = customLayout.findViewById(R.id.paymentprice);
        TextView paymentDone = customLayout.findViewById(R.id.paymentDone);

        if (status.equals("Success")) {
            imageView.setImageResource(R.drawable.tick);
            paymentMsg.setText("Payment Successful\n");
            paymentprice.setText("Your payment of " +  Currency + " " + Price + " for " + Package + " plan has completed");
        } else if (status.equals("Failed")){
            imageView.setImageResource(R.drawable.ic_cross);
            paymentMsg.setText("Payment Failed");
            paymentprice.setText("Your payment of " +  Currency + " " + Price + " for " + Package + " plan has failed ");
        } else if (status.equals("Canceled")) {
            imageView.setImageResource(R.drawable.ic_cross);
            paymentMsg.setText("Payment Canceled");
            paymentprice.setText("Your payment of " +  Currency + " " + Price + " for " + Package + " plan has canceled ");
        }


        paymentDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }





     public void SavePaymentDetailsBeforeDone() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        token = prefs.getString("token_key", "");
        email = prefs.getString("email_key", "");
        Phnumber = prefs.getString("number_key", "");
        Log.d("currency", Price);
        PaymentModel paymentModel = new PaymentModel();
        paymentModel.setPaymentSource("card");
        paymentModel.setAmount(Price);
        paymentModel.setMobile(Phnumber);
        paymentModel.setToken(token);
        paymentModel.setEmail(email);
        paymentModel.setCustomerId(CustomerID);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setCurrency(Currency);
        paymentModel.setPack(Package);
        paymentModel.setSource("app");

        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentResponse> responseCall = api.PaymentDetailsSave("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                if (response.isSuccessful()) {
                    PaymentData model = response.body().getData();
                    new SessionManagement(getApplicationContext()).SaveOrderId(model.getOrderId(),expiryDate);
                    Log.d("PaymentStatus", response.body().getMessage() +"OrderId "+model.getOrderId());
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("PaymentError", t.getMessage());
            }
        });
    }

    private void SavePaypalPaymentDeatils(String ResourceId) {
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        token = prefs.getString("token_key", "");
        email = prefs.getString("email_key", "");
        Phnumber = prefs.getString("number_key", "");
        JSONObject object= new JSONObject();
        try {
            object.put("source","app");
            object.put("token",token);
            object.put("uid",Uid);
            object.put("mobile",Phnumber);
            object.put("email",email);
            object.put("paymentSource","card");
            object.put("amount",Price);
            object.put("pack",Package);
            object.put("currency",Currency);
            object.put("resourceId",ResourceId);
            object.put("paymentStatus","created");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String mRequestBody = object.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://flictrick.com/api/payment/paypal/savePaymentDetailsBeforePayment.php/processPaymentDetails", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);

                    JSONObject object1 = object.getJSONObject("data");
                    OrderId= object1.getString("orderId");

                    new SessionManagement(getApplicationContext()).SaveOrderId(OrderId,expiryDate);


                    // Toast.makeText(SubscriptionActivity.this, ClientSecretKey, Toast.LENGTH_SHORT).show();
                    Log.d("qwerty123", OrderId);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("ErrorVoleey",error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);


    }

    /* public void SavePaymentDetailsBeforeForPaypal(String ResourceId){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        token = prefs.getString("token_key", "");
        email = prefs.getString("email_key", "");
        Phnumber = prefs.getString("number_key", "");
        Log.d("currency", ResourceId);
        PaymentModel paymentModel = new PaymentModel();
        paymentModel.setPaymentSource("card");
        paymentModel.setAmount(Price);
        paymentModel.setMobile(Phnumber);
        paymentModel.setToken(token);
        paymentModel.setResourceId(ResourceId);
        paymentModel.setEmail(email);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setCurrency(Currency);
        paymentModel.setPack(Package);
        paymentModel.setSource("app");

        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentResponse> responseCall = api.PaypalPaymentDetailsSave("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                if (response.isSuccessful()) {
                    PaymentData model = response.body().getData();
                    new SessionManagement(getApplicationContext()).SaveOrderId(model.getOrderId());
                    Log.d("PaymentStatus", response.body().getMessage() +"OrderId "+model.getOrderId());
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("PaymentError", t.getMessage());
            }
        });
    }*/


    public void PayPalUpdatePaymentDetailsAfterDone(String status,String paymentId,String resourceId) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        String orderId = prefs.getString("orderId","");
        Log.d("datetime2 " + CurrentDate, "expiry" + ExpiryDate + "currency" + Currency +"package"+Package +"OrderId" +orderId);
        PaymentRequest paymentModel = new PaymentRequest();
        paymentModel.setSubEndDate(ExpiryDate);
        paymentModel.setSubStartDate(CurrentDate);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setPaymentStatus(status);
        paymentModel.setOrderId(orderId);
        paymentModel.setSource("app");
        paymentModel.setCurrency(Currency);
        paymentModel.setAmount(Price);
        paymentModel.setPaymentId(paymentId);
        paymentModel.setResourceId(resourceId);
        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentDoneResponse> responseCall = api.PaypalPaymentDetailsUpdate("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentDoneResponse>() {
            @Override
            public void onResponse(Call<PaymentDoneResponse> call, retrofit2.Response<PaymentDoneResponse> response) {
                //String message = response.body().getMessage();
                if (response.isSuccessful()) {
                  // new SessionManagement(getApplicationContext()).SavePaymentDetails(model.getOrderId());
                    Log.d("PaymenentUpdateStatus", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                Log.d("PaymenentError", t.getMessage());
            }
        });
    }
    public void UpdatePaymentDetailsAfterDone(String status) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        String orderId = prefs.getString("orderId","");

        Log.d("datetime " + currentDate, "expiry" + expiryDate + "currency" + Currency +"package"+Package +"OrderId" +orderId);
        Log.d("CustomerID2",CustomerID);
        PaymentRequest paymentModel = new PaymentRequest();
        paymentModel.setCustomerId(CustomerID);
        paymentModel.setSubEndDate(expiryDate);
        paymentModel.setSubStartDate(currentDate);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setPaymentStatus(status);
        paymentModel.setOrderId(orderId);
        paymentModel.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentDoneResponse> responseCall = api.PaymentDetailsUpdate("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentDoneResponse>() {
            @Override
            public void onResponse(Call<PaymentDoneResponse> call, retrofit2.Response<PaymentDoneResponse> response) {
                //String message = response.body().getMessage();
                if (response.isSuccessful()) {
                  // new SessionManagement(getApplicationContext()).SavePaymentDetails(model.getOrderId());
                    Log.d("PaymenentUpdateStatus", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                Log.d("PaymenentError", t.getMessage());
            }
        });
    }


}