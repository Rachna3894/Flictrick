package com.instaplay.flictrick.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.LoginViewModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SettingAdapter extends BaseAdapter {


    private ArrayList<String> results;
    Context mContext;
    SubscriptionPackage subscriptionPackage;
    LoginViewModel loginViewModel;
    LogoutListener mlistener;

    public SettingAdapter(ArrayList<String> list, Context context,SubscriptionPackage aPackage,LogoutListener listener) {
        this.mContext = context;
        this.results = list;
        this.subscriptionPackage= aPackage;
        this.mlistener= listener;

    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int i) {
        return results.size();
    }

    @Override
    public long getItemId(int i) {
        return results.size();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        TextView titleTextView;
        ImageView smallThumbnailImageView,icon;
        View listitemView = view;
        if (listitemView == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            listitemView = LayoutInflater.from(mContext).inflate(R.layout.setting_adapter, viewGroup, false);
        }
        titleTextView = listitemView.findViewById(R.id.text);
        smallThumbnailImageView = listitemView.findViewById(R.id.thumbnail);
        icon= listitemView.findViewById(R.id.icon);
        String content = results.get(position);
        titleTextView.setText(content);
        //Log.d("test",subscriptionPackage.getStatus());
        //Logout(mContext);
        if (content.equals("Profile")) {
            smallThumbnailImageView.setVisibility(View.VISIBLE);
            icon.setImageResource(R.drawable.profile_icon);
        }
        else if (content.equals("Subscription")) {
            icon.setImageResource(R.drawable.ic_premium_icon);
        }
        else if (content.equals("Terms & Conditions")) {
            icon.setImageResource(R.drawable.ic_file);
        }else if (content.equals("Help & Support")) {
            icon.setImageResource(R.drawable.ic_info_icon);
        }else if (content.equals("Privacy Policy")) {
            icon.setImageResource(R.drawable.ic_privacy_icon);
        }else if (content.equals("Account Deletion")) {
            icon.setImageResource(R.drawable.ic_baseline_delete_24);
        }else if (content.equals("About Us")) {
            icon.setImageResource(R.drawable.ic_baseline_info_24);
        }if (content.equals("Logout")) {
            smallThumbnailImageView.setVisibility(View.GONE);
            icon.setImageResource(R.drawable.ic_logout_black);
        }

        listitemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (content.equals("Profile")) {
                        Intent intent = new Intent(mContext, ProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //intent.putExtra("url", content.getEpisodePath());
                        mContext.startActivity(intent);
                    } else if (content.equals("Subscription")) {
                        String status = "";
                        status = new SessionManagement(mContext).SubscriptionStatus();
                        if (status.equals("Subscription Is Active")) {
                            Intent intent = new Intent(mContext, SubscriptionDetails.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }else {
                            Intent intent = new Intent(mContext, SubscriptionActivity.class);
                            //intent.putExtra("url", episodeData.get(finalI).getEpisodePath());
                            intent.putExtra("package", subscriptionPackage);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }

                    } else if (content.equals("Terms & Conditions")) {
                        Uri uri = Uri.parse("https://flictrick.com/Terms-Of-Use-Flictrick.html"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } else if (content.equals("Help & Support")) {
                       // Uri uri = Uri.parse("https://help.flictrick.com/"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(mContext,Help_Support.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } else if (content.equals("Logout")) {
                        mlistener.LogoutClick();
                     // LogoutUser();
                    } else if (content.equals("Privacy Policy")) {
                        Uri uri = Uri.parse("https://flictrick.com/privacy-Flictrick.html"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);

                    }else if(content.equals("Account Deletion")){
                        Intent intent = new Intent(mContext,DeleteAccount.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);

                    }
                        else if (content.equals("About Us")){
                        Uri uri = Uri.parse("https://flictrick.com/About-us-Flictrick.html"); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);

                    }
                }
            });




        return listitemView;
    }

    public void LogoutUser() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = mContext.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        // byte[] passencode= pass.getBytes()
        model.setToken(token);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<LoginResponse> responseCall = api.LogoutUser(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    //LoginData data = response.body().getData();
                    if (response.code() == 200) {
                        Toast.makeText(mContext, response.message(), Toast.LENGTH_SHORT).show();
                        mContext.startActivity(new Intent(mContext,LoginActivity.class));
                       // Utils.popUp(mContext, response.body().getMessage());
                    } else {
                        Toast.makeText(mContext, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    // isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("Error4", t.getMessage());
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }




interface LogoutListener{
        void LogoutClick();
}


}
