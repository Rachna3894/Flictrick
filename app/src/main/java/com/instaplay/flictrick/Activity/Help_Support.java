package com.instaplay.flictrick.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Model.ForgotResponse;
import com.instaplay.flictrick.Model.Video.HelpSupport;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Help_Support extends AppCompatActivity {
    EditText edQery, edEmail;
    TextView btnSubmit;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support);
        edEmail = findViewById(R.id.ed_email);
        edQery = findViewById(R.id.ed_query);
        btnSubmit = findViewById(R.id.btnSubmit);
        toolbar= findViewById(R.id.toolbar);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AccountSetting.class));
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    if (edEmail.getText().toString().isEmpty()) {
                        edEmail.setError("Enter the email Id");
                    } else if (edQery.getText().toString().isEmpty()) {
                        edQery.setError("Enter your Query");
                    } else {
                        PostHelp_Support(edEmail.getText().toString(), edQery.getText().toString());
                    }

                }

            }

        });
    }


    public void PostHelp_Support(String query, String sender) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        HelpSupport model = new HelpSupport();
        model.setQuery(query);
        model.setSender(sender);

        APIClient api = retrofit.create(APIClient.class);
        Call<ForgotResponse> responseCall = api.SubmitQuery(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    if (response.code() == 200) {
                        edEmail.setText("");
                        edQery.setText("");
                        Utils.Alert(Help_Support.this, "Thank You! Our support representative will contact you via email");
                    } else {
                        Utils.promptDialog(getApplicationContext(), response.body().getMessage());
                    }


                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                Log.e("Error4", t.getMessage());
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }


}