package com.instaplay.flictrick.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.hbb20.CountryCodePicker;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.RegisterViewModel;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    EditText ed_first_name, ed_email, ed_phone, question1,ed_age,ed_pass;
    TextView txtQuest, btnSubmit,ed_dob,linkLogin;
    String dob,gender,CountryCode,pass,last_name;
    RegisterViewModel model;
    CheckBox checkboxmale,checkboxfemale,checkboxother;
    CountryCodePicker ccp;
    Spinner spinner;
    int age=0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ccp= findViewById(R.id.cc);
        ed_pass =findViewById(R.id.ed_pass);
        ed_email = findViewById(R.id.ed_email);
        //spinner = findViewById(R.id.spinCode);
        ed_first_name = findViewById(R.id.ed_first_name);
        ed_phone = findViewById(R.id.ed_phone);
        question1 = findViewById(R.id.ed_answer);
        txtQuest = findViewById(R.id.txtQuest);
        btnSubmit = findViewById(R.id.btnRegister);
        linkLogin= findViewById(R.id.linkLogin);
        checkboxfemale = findViewById(R.id.checkboxfemale);
        checkboxmale= findViewById(R.id.checkboxmale);
        checkboxother= findViewById(R.id.checkboxother);
        linkLogin.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);


        ccp.setDialogBackgroundColor(R.color.white);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Log.d("PhoneCOde", ccp.getSelectedCountryCode());
            }
        });
        RegisterResponse();
        genderSelection();


    }
    public void ShowHidePass(View view){

        if(view.getId()==R.id.show_pass_btn){

            if(ed_pass.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.hide_password);

                //Show Password
                ed_pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.show_password);

                //Hide Password
                ed_pass.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
    }

    private void genderSelection(){

        checkboxfemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender = "Female";
                    checkboxmale.setChecked(false);
                    checkboxother.setChecked(false);
                }
            }
        });

        checkboxmale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender = "Male";
                    checkboxfemale.setChecked(false);
                    checkboxother.setChecked(false);
                }
            }
        });

        checkboxother.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gender = "Others";
                    checkboxfemale.setChecked(false);
                    checkboxmale.setChecked(false);
                }
            }
        });
    }




/*
    public void showExpenseDate(TextView ed_dob ) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //String[] mons = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
                //String mName = mons[monthOfYear];
                dob = String.valueOf(year) + "-" + String.valueOf((monthOfYear + 1))
                        + "-" + String.valueOf(dayOfMonth);
                ed_dob.setText(dob);
                Log.d("djkjiksd", dob);

            }
        }, yy, mm, dd);

        datePicker.show();
    }
*/
public  void ForceAlert(final Context context, String msg) {
    final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
            .setCancelable(false);
    errorDialog.setTitle("Message");
    errorDialog.setMessage(msg);
    errorDialog.setPositiveButton("OK",
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                   Intent intent= new Intent(getApplicationContext(),LoginActivity.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
    AlertDialog alert11 = errorDialog.create();
    alert11.show();

}
    public void RegisterResponse() {
        model = ViewModelProviders.of(this).get(RegisterViewModel.class);
        model.init();
        model.getRegisterResponseLiveData().observe(this, new Observer<RegisterResponse>() {
            @Override
            public void onChanged(RegisterResponse loginResponse) {
               Log.d("qwerty",loginResponse.getMessage());
                if (loginResponse.getCode() == 200){
                    Log.d("register",loginResponse.getMessage());
                    ForceAlert(RegistrationActivity.this, loginResponse.getMessage());
                   // startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                } else {
                    Utils.Alert(RegistrationActivity.this, loginResponse.getMessage());

                }

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    if (!Utils.validateEmail(ed_email) && !Utils.checkForMobile(ed_phone) && !Utils.validatePassword(ed_pass)&& !question1.getText().toString().isEmpty()) {
                        return;
                    }
                    model.CreateUserAccount( ed_email.getText().toString().trim(),ed_first_name.getText().toString().trim(), ccp.getSelectedCountryCode() + "" +ed_phone.getText().toString().trim(), gender,ed_pass.getText().toString().trim() ,txtQuest.getText().toString(),question1.getText().toString());
                } else {
                    Utils.internetAlert(getApplicationContext());
                }
                break;
            case R.id.linkLogin:
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                break;


        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        super.onBackPressed();
    }
}
