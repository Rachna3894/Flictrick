package com.instaplay.flictrick.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.Subscription.SubscriptionRequest;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Subscription.SubscriptionResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.LoginViewModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AccountSetting extends AppCompatActivity implements SettingAdapter.LogoutListener{
    ListView setting;
    SettingAdapter settingAdapter;
    Toolbar toolbar;
    ArrayList<String> list;
    SubscriptionPackage subscriptionPackage;
    LoginViewModel loginViewModel;
    public static final int MobileData = 2;
    public static final int WifiData = 1;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setting = findViewById(R.id.settingList);
        toolbar= findViewById(R.id.toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
            }
        });
        list = new ArrayList<>();
        //GetSubscriptionPlan();
        list.add("Profile");
        list.add("Subscription");
        list.add("Terms & Conditions");
        //list.add("Preferences");
        list.add("Help & Support");
        list.add("Privacy Policy");
        list.add("Account Deletion");
        list.add("About Us");
        list.add("Logout");
        //setting = (ListView) findViewById(R.id.settingList);
        settingAdapter = new SettingAdapter(list, getApplicationContext(), subscriptionPackage,AccountSetting.this);
        setting.setAdapter(settingAdapter);
        settingAdapter.notifyDataSetChanged();


        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case WifiData:
                            //Toast.makeText(getActivity(), String.format("Wifi turned ON"),Toast.LENGTH_SHORT).show();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    GetSubscriptionPlan();

                                }
                            });
                            break;
                        case MobileData:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    GetSubscriptionPlan();

                                }
                            });
                            break;
                    }
                } else {
                    Utils.internetAlert(AccountSetting.this);
                }
            }
        });

    }

    public void GetSubscriptionPlan() {
        //progressDialog = new ProgressDialog(context);
        //progressDialog.setMessage("Loading");
        //progressDialog.setCancelable(false);
        // progressDialog.show();
        Gson gson = new GsonBuilder().setLenient().create();
        String country = new SessionManagement(getApplicationContext()).GetUserCountry();
        SubscriptionRequest subscription = new SubscriptionRequest();
        subscription.setSource("app");
        subscription.setCountry(country);
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BaseClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        APIClient api = retrofit.create(APIClient.class);
        Call<SubscriptionResponse> responseCall = api.SubscriptionPlan("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", subscription);
        responseCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                if (response.body() != null) {
                    subscriptionPackage = response.body().getData();
                    Log.d("subscriptionPackage", response.body().getMessage());
                    settingAdapter = new SettingAdapter(list, getApplicationContext(), subscriptionPackage,AccountSetting.this);
                    setting.setAdapter(settingAdapter);
                    settingAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                Log.e("subscripStatus", t.getMessage());
                // volumesResponseLiveData.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {
                    //Getvideo("4");
                } else if (t.getMessage().contains("failed to connect to sportapi.clicknplay24.com/139.59.67.0 (port 443)")) {
                    //Getvideo("4");
                }

            }
        });
    }


    @Override
    public void LogoutClick() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               LogoutAlert(AccountSetting.this,"Do you want to exit ?");


            }
        },100);

    }

    public void Logout() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.init();
        loginViewModel.getLogoutUser().observe(this, new Observer<LoginResponse>() {
            @Override
            public void onChanged(LoginResponse loginResponse) {
                if (loginResponse != null) {
                    if (loginResponse.getCode() == 200) {
                        Toast.makeText(AccountSetting.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        new SessionManagement(getApplicationContext()).LogoutDetails();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    } else {
                        Utils.promptDialog(AccountSetting.this, loginResponse.getMessage());
                    }
                }
            }
        });
        loginViewModel.LogoutUser(getApplicationContext());
    }


    public void LogoutAlert(final Context context, String msg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(true);
        errorDialog.setTitle("Message");
        errorDialog.setMessage(msg);
        errorDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Utils.isNetworkAvailable(getApplicationContext())){
                            Logout();
                            //context.startActivity(new Intent(context, LoginActivity.class));
                        }else{
                            Utils.internetAlert(AccountSetting.this);
                        }


                    }
                });
        errorDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
