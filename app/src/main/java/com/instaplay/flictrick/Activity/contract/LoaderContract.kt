package com.instaplay.flictrick.Activity.contract

import android.util.AttributeSet

/**
 * Created by suneet on 9/18/17.
 */
interface LoaderContract {
    fun initAttributes(attrs: AttributeSet)
}
