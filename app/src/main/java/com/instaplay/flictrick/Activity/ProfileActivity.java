package com.instaplay.flictrick.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Adapter.MultiSpinner;
import com.instaplay.flictrick.Model.Category.CategoryModel;
import com.instaplay.flictrick.Model.Category.CategoryResponse;
import com.instaplay.flictrick.Model.ProfileModelResponse;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.CategoryViewModel;
import com.instaplay.flictrick.ViewModel.ProfileViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {
    EditText ed_name, ed_answer, ed_gender, ed_phone, ed_email;
    TextView textView;
    ProfileViewModel loginViewModel;
    CategoryViewModel categoryViewModel;
    TextView btnProfileEdit, btnResetpass, txtQues;
    List<CategoryModel> model;
    String name = "", email = "", age = "", number = "", ques = "", answer = "";
    Toolbar toolbar;
    private ArrayAdapter<String> adapter;
    MultiSpinner multiSelectSpinner;
    List<String> selectedItemsId;
    CheckBox check_male, check_female, check_other;
    ProgressDialog dialog;
    String Gender;
    String[] langArray;
    boolean[] selectedLanguage;


    @SuppressLint("MissingInflatedId")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ed_name = findViewById(R.id.ed_name);
        txtQues = findViewById(R.id.txtQuest);
      //  textView = findViewById(R.id.textView);
        ed_answer = findViewById(R.id.answer);
        toolbar = findViewById(R.id.toolbar);
        btnProfileEdit = findViewById(R.id.profile_edit);
        ed_phone = findViewById(R.id.ed_phoneNumber);
        ed_email = findViewById(R.id.ed_email_login);
        check_male = findViewById(R.id.checkboxmale);
        check_female = findViewById(R.id.checkboxfemale);
        check_other = findViewById(R.id.checkboxothers);
        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item);
        selectedItemsId = new ArrayList<>();
        // get spinner and set adapter
        multiSelectSpinner = (MultiSpinner) findViewById(R.id.spinnerMulti);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
            }
        });



        check_female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Gender = "Female";
                    check_male.setChecked(false);
                    check_other.setChecked(false);
                }
            }
        });

        check_male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Gender = "Male";
                    check_female.setChecked(false);
                    check_other.setChecked(false);
                }
            }
        });

        check_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Gender = "Others";
                    check_female.setChecked(false);
                    check_male.setChecked(false);
                }
            }
        });


        btnProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    name = ed_name.getText().toString();
                    email = ed_email.getText().toString();
                    number = ed_phone.getText().toString();
                    ques = txtQues.getText().toString();
                    answer = ed_answer.getText().toString();

                    // age = ed_age.getText().toString();
                    //String gender= "";
                    loginViewModel.GetUpdateProfile(getApplicationContext(), name.trim(), email.trim(), number, Gender, ques.replace("?", ""), answer, selectedItemsId);


                } else {
                    Utils.internetAlert(ProfileActivity.this);
                }

            }
        });



        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case HomeActivity.WifiData:
                            ProfileView();
                            //ViewProfileDetails(getApplicationContext());
                            break;
                        case HomeActivity.MobileData:
                            ProfileView();
                           // ViewProfileDetails(getApplicationContext());
                            break;
                    }
                } else {

                    Utils.internetAlert(getApplicationContext());
                }
            }
        });

        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        categoryViewModel.init();
        categoryViewModel.getPreferenceCategory().observe(this, new Observer<CategoryResponse>() {
            @Override
            public void onChanged(CategoryResponse categoryResponse) {
                if (categoryResponse != null) {
                    model = categoryResponse.getData();
                    Log.d("categorySize", "" + model.size());
                    if (model.size() > 0) {
                        for (int i = 0; i < model.size(); i++) {
                            Log.d("preferenceGet", "" + model.size());
                           // selectedItemsId.add(model.get(i).getRootCategoryName());
                            adapter.add(model.get(i).getRootCategoryName());

                        }
                        multiSelectSpinner.setAdapter(adapter, false, onSelectedListener);
                        boolean[] selectedItems = new boolean[adapter.getCount()];
                        selectedItems[0] = false;
                        multiSelectSpinner.setSelected(selectedItems);

                        //loginViewModel.GetViewProfile(getApplicationContext());

                    } else {
                        Utils.promptDialog(getApplicationContext(), categoryResponse.getMessage());
                    }
                }
            }


        });


        loginViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        loginViewModel.init();
        loginViewModel.UpdateProfileLiveData().observe(this, new Observer<ProfileModelResponse.ProfileResponse>() {
            @Override
            public void onChanged(ProfileModelResponse.ProfileResponse profileResponse) {
                ProfileModelResponse profile = profileResponse.getData();

                if (profile != null) {
                    List<String> categories = profile.getCategoryPreferences();
                    Log.d("Gender12", profile.getGender());
                    if (profile.getGender().equals("male")) {
                        check_male.setChecked(true);
                    } else {
                        check_female.setChecked(true);
                    }


                    Utils.Alert(ProfileActivity.this, profileResponse.getMessage());
                   // multiSelectSpinner.setAdapter(adapter, false, onSelectedListener);
                    new SessionManagement(getApplicationContext()).UpdateDetails(profile.getMobile(), profile.getEmail(), profile.getName());

                }

                // select second item

            }
        });


    }


    private void ProfileView() {
        dialog = new ProgressDialog(ProfileActivity.this);
        dialog.setMessage("Please wait....");
        dialog.setCancelable(false);
        dialog.show();
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        Log.d("CustomerID", token);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("uid", String.valueOf(UserId));
        params.put("source", "app");
        params.put("token", token);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, "https://flictrick.com/api/profile/profileViewById.php", new JSONObject(params), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response!=null) {
                    categoryViewModel.CategoryPreference(getApplicationContext());
                    try {
                        dialog.dismiss();
                        String message = response.getString("message");
                        if (message.equals("Invalid Token")){
                            Utils.promptDialog(ProfileActivity.this,"User Already login on other device");
                        }else {
                            JSONObject jsonData = response.getJSONObject("data");
                            String email = jsonData.getString("email");
                            String name = jsonData.getString("name");
                            String answer = jsonData.getString("answer");
                            String gender = jsonData.getString("gender");
                            String phone = jsonData.getString("mobile");
                            Log.d("Geneder",gender);
                            if (gender.equals("Male")) {
                                check_male.setChecked(true);
                                check_female.setChecked(false);
                                check_other.setChecked(false);
                            } else if (gender.equals("Female")){
                                check_female.setChecked(true);
                                check_male.setChecked(false);
                                check_other.setChecked(false);
                            } else {
                                check_female.setChecked(false);
                                check_male.setChecked(false);
                                check_other.setChecked(true);
                            }
                            ed_email.setText(email);
                            ed_answer.setText(answer);
                            ed_name.setText(name);
                            ed_phone.setText(phone);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(ProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            // Do something here with the selected items
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    try {
                        Log.d("preferenceList", model.get(i).getId());
                        selectedItemsId.add(model.get(i).getId());
                    } catch (NumberFormatException e) {

                    }
                    builder.append(adapter.getItem(i)).append(" ");

                }
            }

            Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_SHORT).show();
        }
    };
}