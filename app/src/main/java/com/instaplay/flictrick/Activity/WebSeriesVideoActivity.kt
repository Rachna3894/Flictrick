package com.instaplay.flictrick.Activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.util.MimeTypes
import com.google.common.collect.ImmutableList
import com.google.gson.GsonBuilder
import com.instaplay.flictrick.Adapter.AdapterVideo.EpisodeAdapter
import com.instaplay.flictrick.Adapter.AdapterVideo.EpisodeChildAdapter
import com.instaplay.flictrick.Adapter.AdapterVideo.ParentVideoAdapter
import com.instaplay.flictrick.Adapter.AdapterVideo.WebSeriesAdapter
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage
import com.instaplay.flictrick.Model.Video.*
import com.instaplay.flictrick.NetworkService.APIClient
import com.instaplay.flictrick.NetworkService.BaseClient
import com.instaplay.flictrick.R
import com.instaplay.flictrick.ViewModel.VideoViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [WebSeriesVideoActivity.newInstance] factory method to
 * create an instance of this fragment.
 */
class WebSeriesVideoActivity : AppCompatActivity(), EpisodeChildAdapter.ClickListener, WebSeriesAdapter.ClickVideo {
    // TODO: Rename and change types of parameters
    var gridEpisode: GridView? = null
    var recyclerView: RecyclerView? = null
    var EpisoderecyclerView: RecyclerView? = null
    var Trailer: String = ""
    var title: String? = null
    var subTitle:String?= null
    var type: String? = null
    var rootCatId: String =""
    var subRootCatId: String? = null
    var description: String? = null
    var categoryType: String? = null
    var bannerVideoAdapter: WebSeriesAdapter? = null
    var childVideoAdapter: ParentVideoAdapter? = null
    var episodeAdapter: EpisodeAdapter? = null
    var toolbar: androidx.appcompat.widget.Toolbar? = null
    var txtCast: TextView? = null
    var txtTitle: TextView? = null

    var txtDescription: TextView? = null
    var videoname: TextView? = null
    var episodeData: ArrayList<ContentEpisodeData>? = null
    private var playbackPosition: Long = 0
    private var playWhenReady = true
    var exoPlayer: ExoPlayer? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var subscriptionPackage: SubscriptionPackage? = null
    var playerView: StyledPlayerView? = null
    private var currentPlayer: StyledPlayerView? = null
    var relatedList: ArrayList<Data>? = null
    var concatenatingMediaSource: ConcatenatingMediaSource? = null
    var videoViewModel: VideoViewModel? = null
    var dataList: ArrayList<SearchData>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webseries_related_video)
        toolbar = findViewById(R.id.toolbar)
        txtCast = findViewById(R.id.txtCast);
        txtTitle = findViewById(R.id.catType);
        videoname= findViewById(R.id.videoname);
        txtDescription = findViewById(R.id.txtDescription);
        recyclerView = findViewById(R.id.related_recyclerView)
        EpisoderecyclerView = findViewById(R.id.episode_recyclerView)
        gridEpisode = findViewById(R.id.gridEpisode);
        playerView = findViewById(R.id.playerView)
        Trailer = intent.getStringExtra("url").toString();
        type = intent.getStringExtra("type").toString();
        title = intent?.getStringExtra("VideoCast").toString()
        subTitle = intent?.getStringExtra("subtitle").toString()
        rootCatId = intent?.getStringExtra("rootCategoryId").toString()
        subRootCatId = intent?.getStringExtra("subRootCategoryId").toString()
        description = intent?.getStringExtra("VideoDesc").toString()
        categoryType = intent?.getStringExtra("Category").toString()
        subscriptionPackage = intent?.getParcelableExtra("Package")
        Log.d("qwerty.." + subRootCatId, rootCatId!!+subTitle)

        toolbar?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                if (fragmentManager.backStackEntryCount == 0) {
                    finish()
                } else {
                    fragmentManager.popBackStack()
                }
            }
        })

        currentPlayer = playerView
        setFullScreenListener(applicationContext)

        runOnUiThread( kotlinx.coroutines.Runnable {
            if (rootCatId != "") {
                txtCast!!.text = title
                txtDescription!!.text = description

                VideoRelatedContent(rootCatId!!, subRootCatId!!)
                // to get the file name by the videoPath
                /* val regex = """(.+)/(.+)\.(.+)""".toRegex()
                 val matchResult = regex.matchEntire(Trailer)

                 if (matchResult != null) {
                     val (directory, fileName, extension) = matchResult.destructured
                     videoname!!.text=fileName
                 }*/


            }
        })






    }




    fun VideoRelatedContent(rootCatId: String, subRootId: String) {
        videoViewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java)
        videoViewModel!!.init()
        videoViewModel!!.VideoRelatedContent().observe(this@WebSeriesVideoActivity,
            object : Observer<SearchResponse?> {
                override fun onChanged(volumesResponse: SearchResponse?) {

                    //TODO("Not yet implemented")
                    if (volumesResponse != null) {
                        dataList = volumesResponse?.data
                        Log.d("abcdf", "" + dataList?.size + "rootCategory" + categoryType)
                        if (dataList != null) {
                            if (type!!.equals("Search") || type!!.equals("Home")) {
                                //recyclerView!!.visibility = View.GONE
                                gridEpisode!!.visibility = View.VISIBLE
                                episodeAdapter = EpisodeAdapter()
                                linearLayoutManager =
                                    LinearLayoutManager(applicationContext,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                EpisoderecyclerView!!.setLayoutManager(linearLayoutManager)
                                EpisoderecyclerView!!.adapter = episodeAdapter
                                episodeAdapter!!.setSearchResults(dataList,
                                    applicationContext,
                                    subRootId,this@WebSeriesVideoActivity)

                                txtTitle!!.text= categoryType;
                                bannerVideoAdapter =
                                    WebSeriesAdapter(
                                        dataList,
                                        getApplicationContext(),
                                        categoryType,
                                        this@WebSeriesVideoActivity,
                                        type,
                                        subscriptionPackage,rootCatId);
                                gridEpisode!!.setAdapter(bannerVideoAdapter)
                            } else {
                                episodeAdapter = EpisodeAdapter()
                                linearLayoutManager =
                                    LinearLayoutManager(applicationContext,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                EpisoderecyclerView!!.setLayoutManager(linearLayoutManager)
                                EpisoderecyclerView!!.adapter = episodeAdapter
                                episodeAdapter!!.setSearchResults(dataList,
                                    applicationContext,
                                    subRootId,this@WebSeriesVideoActivity)
                            }


                        }

                    }


                }
            })
        videoViewModel!!.GetSubCatRelatedContent(rootCatId)
    }


    private fun preparePlayer() {
        exoPlayer = ExoPlayer.Builder(applicationContext).setSeekBackIncrementMs(5000)
            .setSeekForwardIncrementMs(5000).build()
        exoPlayer?.let { exo ->
            exo?.playWhenReady = true
            playerView?.player = exo
            //exo.setMediaSource(setMediaType())
            exo.seekTo(playbackPosition)
            exo.playWhenReady = playWhenReady

            val assetSrtUri = Uri.parse((subTitle))
            val subtitle = MediaItem.SubtitleConfiguration.Builder(assetSrtUri)
                .setMimeType(MimeTypes.TEXT_VTT)
                .setLanguage("en")
                .setSelectionFlags(C.SELECTION_FLAG_AUTOSELECT)
                .build()

            val assetVideoUri = Uri.parse((Trailer))
            val mediaItem = MediaItem.Builder()
                .setUri(assetVideoUri)
                .setSubtitleConfigurations(ImmutableList.of(subtitle))
                .build()

            exoPlayer!!.setMediaItem(mediaItem)
            exo.prepare()
           // exoPlayer.addListener(playerView.)
        }
    }

    private fun releasePlayer() {
        currentPlayer?.player = null
        exoPlayer?.let { player ->
            playbackPosition = player.currentPosition
            playWhenReady = player.playWhenReady
            player.release()
            exoPlayer = null
        }
    }

    private fun setMediaType(): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        Log.d("SUBTILE", subTitle.toString());
        val subtitle = MediaItem.SubtitleConfiguration.Builder(Uri.parse(subTitle))
            .setMimeType(MimeTypes.TEXT_VTT) // The correct MIME type (required).
            .setLanguage("app") // MUST, The subtitle language (optional).
            .setSelectionFlags(C.SELECTION_FLAG_AUTOSELECT) //MUST,  Selection flags for the track (optional).
            .build()

        val subtitleSource = SingleSampleMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(subtitle, C.TIME_UNSET)

        val mediaItem: MediaItem = MediaItem.Builder()
            .setUri(Trailer)//videouri
            .setSubtitleConfigurations(ImmutableList.of(subtitle))
            .build()

        val mediaSource = ProgressiveMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)

        // merge subtitle source and media source
        val mediaSourceWithsubtitle = MergingMediaSource(mediaSource, subtitleSource)
        exoPlayer?.setMediaSource(mediaSourceWithsubtitle)
       // exoPlayer?.setMediaSource(mediaSource)

           // Log.d("Trailer1", "" + Trailer)

         return  mediaSource;
    }



    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT <= 23) {
            runOnUiThread( kotlinx.coroutines.Runnable {
                preparePlayer()
            })

            currentPlayer?.onResume()
        }
        //currentPlayer?.onResume()

    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT > 23) {
            //currentPlayer?.player = null
            releasePlayer()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer?.stop()
        releasePlayer()
    }

    override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT > 23) {
            runOnUiThread( kotlinx.coroutines.Runnable {
                preparePlayer()
            })
            currentPlayer?.onResume()
        }

    }

    @SuppressLint("SourceLockedOrientationActivity")
    private fun setFullScreenListener(context: Context) {
        // Creating a new Player View and place it inside a Full Screen Dialog.
        val fullScreenPlayerView = StyledPlayerView(this)
        fullScreenPlayerView.setShowNextButton(false)
        fullScreenPlayerView.setShowPreviousButton(false)
        val dialog = object : Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            @Deprecated("Deprecated in Java")
            override fun onBackPressed() {
                // User pressed back button. Exit Full Screen Mode.
                playerView?.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_fullscreen)
                    ?.setImageResource(R.drawable.ic_baseline_fullscreen_24)
                exoPlayer?.let {
                    StyledPlayerView.switchTargetView(it,
                        fullScreenPlayerView,
                        playerView)
                }
                currentPlayer = playerView
                this@WebSeriesVideoActivity.requestedOrientation =
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                super.onBackPressed()
            }

        }
        dialog.addContentView(
            fullScreenPlayerView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        // Adding Full Screen Button Click Listeners.
        playerView?.setFullscreenButtonClickListener {
            // If full Screen Dialog is not visible, make player full screen.
            if (!dialog.isShowing) {
                dialog.show()
                fullScreenPlayerView.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_minimal_fullscreen)
                    .setImageResource(R.drawable.ic_baseline_fullscreen_exit_24)
                exoPlayer?.let {
                    StyledPlayerView.switchTargetView(it,
                        playerView,
                        fullScreenPlayerView)/////////////////////////
                }
                currentPlayer = fullScreenPlayerView
                this@WebSeriesVideoActivity.requestedOrientation =
                    ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
        }
    }

    override fun listener(s: String?) {
        Trailer = s.toString()
        Log.d("listner12", "qwer" + s)
        if (exoPlayer != null) {
            exoPlayer!!.release()
            preparePlayer()
            currentPlayer?.onResume()
        }

    }

    override fun onClickListener(
        url: String?,
        title: String?,
        des: String?,
        rootId: String?,
        subCat: String?,
    ) {
        runOnUiThread( kotlinx.coroutines.Runnable {
           // GetSubCatRelatedContent(rootCatId!!, subrootId!!)
            txtCast!!.text = title
            txtDescription!!.text = des
            Log.d("hey", "path" + url)
            Trailer = url.toString();
            if (exoPlayer != null) {
                exoPlayer!!.release()
                preparePlayer()
                currentPlayer?.onResume()
            }

        })
    }

    override fun listener(s: String?, title: String?, des: String?,rootCatId: String,subrootId: String) {
        //TODO("Not yet implemented")
        Trailer = s.toString()
        txtCast!!.text = title
        txtDescription!!.text = des
        Log.d("bannerclick"+subrootId, "path" + rootCatId)


        runOnUiThread( kotlinx.coroutines.Runnable {
            GetSubCatRelatedContent(rootCatId!!, subrootId!!)
            if (exoPlayer != null) {
                exoPlayer!!.release()
                preparePlayer()
                currentPlayer?.onResume()
            }

        })

    }


    fun GetSubCatRelatedContent(rootCatId: String?,subrootId: String) {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        // volumesResponseLiveData = new MutableLiveData<>();
        val retrofit = Retrofit.Builder()
            .baseUrl(BaseClient.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        val contentRequest = SearchContentRequest()
        contentRequest.source = "app"
        contentRequest.rootCategoryId = rootCatId
        val api = retrofit.create<APIClient>(APIClient::class.java)
        val responseCall =
            api.SearchRelatedContent("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", contentRequest)
        responseCall.enqueue(object : Callback<SearchResponse> {
            override fun onResponse(
                call: Call<SearchResponse>,
                response: Response<SearchResponse>,
            ) {
                if (response.isSuccessful) {
                        dataList = response.body()?.data

                        if (dataList != null) {
                            if (type!!.equals("Search") || type!!.equals("Home")) {
                                Log.d("abcdf", "" + dataList?.size + "rootCategory" + rootCatId)
                                recyclerView!!.visibility = View.GONE
                                gridEpisode!!.visibility = View.VISIBLE
                                episodeAdapter = EpisodeAdapter()
                                linearLayoutManager =
                                    LinearLayoutManager(applicationContext,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                EpisoderecyclerView!!.setLayoutManager(linearLayoutManager)
                                EpisoderecyclerView!!.adapter = episodeAdapter
                                episodeAdapter!!.setSearchResults(dataList,
                                    applicationContext,
                                    subrootId,this@WebSeriesVideoActivity)
                                bannerVideoAdapter =
                                    WebSeriesAdapter(
                                        dataList,
                                        getApplicationContext(),
                                        categoryType,
                                        this@WebSeriesVideoActivity,
                                        type,
                                        subscriptionPackage,rootCatId);
                                gridEpisode!!.setAdapter(bannerVideoAdapter)
                            } else {
                                episodeAdapter = EpisodeAdapter()
                                linearLayoutManager =
                                    LinearLayoutManager(applicationContext,
                                        LinearLayoutManager.VERTICAL,
                                        false);
                                EpisoderecyclerView!!.setLayoutManager(linearLayoutManager)
                                EpisoderecyclerView!!.adapter = episodeAdapter
                                episodeAdapter!!.setSearchResults(dataList,
                                    applicationContext,
                                    subrootId,this@WebSeriesVideoActivity)
                            }


                        }


                    //ArrayList<Data> data = response.body().getData();

                }
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                Log.e("ContentError", t.message!!)

            }
        })
    }
}