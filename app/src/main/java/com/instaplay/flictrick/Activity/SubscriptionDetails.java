package com.instaplay.flictrick.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Subscription.SubscriptionRequest;
import com.instaplay.flictrick.Model.Subscription.SubscriptionResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubscriptionDetails extends AppCompatActivity {
    TextView txtPackage,txtOrderId,txtPrice,txtStartDate,txtEnd,txtPaymentStatus,txtName,txtEmail,txtPhone,moveToSubscribe,txtExp,txSubscrip;
   // SharedPreferences sharedPreferences;
    //String orderId,price,currency="",Package,startDate,startEnd,status,source,name="",email,phone,subActiveStatus;
    Toolbar toolbar;
    ProgressDialog dialog;
    SubscriptionPackage subscriptionPackage;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_details);
        txtPrice= findViewById(R.id.price);
        txtName= findViewById(R.id.edName);
        txSubscrip= findViewById(R.id.subscriptionDetails);
        txtExp= findViewById(R.id.txtExp);
        txtEmail = findViewById(R.id.edEmail);
        txtPhone = findViewById(R.id.edPhone);
        moveToSubscribe = findViewById(R.id.moveToSubscribe);
        txtOrderId = findViewById(R.id.txtOrderId);
        txtStartDate = findViewById(R.id.startDate);
        txtEnd = findViewById(R.id.endDate);
        txtPackage = findViewById(R.id.packagename);
        txtPaymentStatus =  findViewById(R.id.txtPaymentStatus);
        toolbar= findViewById(R.id.toolbar);
        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()){
                        case HomeActivity.WifiData:
                           // ViewProfileDetails(getApplicationContext());
                           GetSubscriptionPlan();
                            ProfileView();
                            break;
                        case HomeActivity.MobileData:
                           // ViewProfileDetails(getApplicationContext());
                            GetSubscriptionPlan();
                            ProfileView();
                            break;
                    }
                } else {
                    Utils.internetAlert(SubscriptionDetails.this);
                }
            }
        });

            toolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(),AccountSetting.class));
                }
            });

         moveToSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(), SubscriptionActivity.class);
                intent.putExtra("package",subscriptionPackage);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });



    }

    private void ProfileView() {
        dialog = new ProgressDialog(SubscriptionDetails.this);
        dialog.setMessage("Please wait....");
        dialog.setCancelable(false);
        dialog.show();
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        Log.d("CustomerID", token);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("uid", String.valueOf(UserId));
        params.put("source", "app");
        params.put("token", token);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, "https://flictrick.com/api/profile/profileViewById.php", new JSONObject(params), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response!=null) {
                    try {
                        dialog.dismiss();

                        String message = response.getString("message");
                        if (message.equals("Invalid Token")){
                                Utils.promptDialog(SubscriptionDetails.this,"User Already login on other device");
                        }else {
                            JSONObject jsonData = response.getJSONObject("data");
                            JSONObject subscription = jsonData.getJSONObject("Subscription");
                            if (subscription != null) {
                                new SessionManagement(getApplicationContext()).SavePaymentDetails(subscription.getString("pack"));
                                txtOrderId.setText("OrderId : " + subscription.getString("orderId"));
                                txtPackage.setText("Payment Method : " + subscription.getString("source"));
                                txtPrice.setText("Package : " + subscription.getString("currency").toUpperCase(Locale.ROOT) + " " + subscription.getString("amount") + "/" + subscription.getString("pack"));
                                txtStartDate.setText(" Payment Date : " + subscription.getString("subscriptionStartDate"));
                                txtEnd.setText(" Expiry Date : " + subscription.getString("subscriptionEndDate"));
                                txtPaymentStatus.setText("Payment Status : " + subscription.getString("paymentStatus"));
                                txtExp.setText("Plan will expire on " + subscription.getString("subscriptionEndDate"));

                            }
                            if (!jsonData.getString("name").isEmpty()) {
                                txtName.setText("Name : " + jsonData.getString("name"));
                            } else {
                                txtName.setText("");
                            }
                            txtPhone.setText("Phone No. : " + jsonData.getString("mobile"));
                            txtEmail.setText("Email : " + jsonData.getString("email"));
                            //   new SessionManagement(getApplicationContext()).SavePaymentDetails(subscription.getString("orderId"), subscription.getString("subscriptionStartDate"), subscription.getString("subscriptionEndDate"), subscription.getString("amount"), subscription.getString("pack"), subscription.getString("currency"), subscription.getString("source"), subscription.getString("paymentStatus"), name);

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //Toast.makeText(SubscriptionDetails.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void GetSubscriptionPlan() {

        Gson gson = new GsonBuilder().setLenient().create();
        String country = new SessionManagement(getApplicationContext()).GetUserCountry();
        SubscriptionRequest subscription = new SubscriptionRequest();
        subscription.setSource("app");
        subscription.setCountry(country);
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BaseClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        APIClient api = retrofit.create(APIClient.class);
        Call<SubscriptionResponse> responseCall = api.SubscriptionPlan("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", subscription);
        responseCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                if (response.body() != null) {
                    subscriptionPackage = response.body().getData();
                    //adapter.renewItems(getActivity(), bannerModelList, "Banner",subscriptionPackage);
                    //country = subscriptionResponse.getCountry();
                    Log.d("subscriptionPackage", response.body().getMessage());
                    //volumesResponseLiveData.postValue(response.body());
                    //isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                Log.e("Error1", t.getMessage());
                // volumesResponseLiveData.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {
                    //Getvideo("4");
                } else if (t.getMessage().contains("failed to connect to flictrick.com/139.59.67.0 (port 443)")) {
                    //Getvideo("4");
                }

            }
        });
    }


}