package com.instaplay.flictrick.Activity.InternetConnection;

public class ConnectionModel {
        private int type;
        private boolean isConnected;

        public ConnectionModel(int type, boolean isConnected) {
            this.type = type;
            this.isConnected = isConnected;
        }

        public int getType() {
            return type;
        }

        public boolean getIsConnected() {
            return isConnected;
        }

}
