package com.instaplay.flictrick.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.hbb20.CountryCodePicker;
import com.instaplay.flictrick.Adapter.CustomAdapter;
import com.instaplay.flictrick.Model.ForgotResponse;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.LoginViewModel;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    LoginViewModel model;
    Spinner spinner;
    //String CountryCode, IpAddress;
    CustomAdapter adapter;
    EditText edPhone, edPassword;
    CountryCodePicker ccp;
    TextView btnlogin, txtQuest,btnlinkLogin;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ccp= findViewById(R.id.cc);
        edPhone = findViewById(R.id.ed_phone);
        edPassword = findViewById(R.id.ed_answer);
        btnlogin = findViewById(R.id.btnlogin);
        txtQuest = findViewById(R.id.txtQuest);
        btnlinkLogin = findViewById(R.id.linkTologin);
        //spinner = findViewById(R.id.spinCode);
        btnlogin.setOnClickListener(this);
        btnlinkLogin.setOnClickListener(this);

        ForgotResponse();
        //for country code display


        ccp.setDialogBackgroundColor(R.color.white);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Log.d("PhoneCOde", ccp.getSelectedCountryCode());
            }
        });



    }








    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:
                //Utils.validatePhoneNumber(edPhone.getText().toString(),CountryCode);
                //Utils.validatePhoneNumber(edPhone.getText().toString(),CountryCode,edPhone)
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    if ( edPhone.getText().toString().isEmpty()) {
                        edPhone.setError("Phone field Number can't be empty");
                    }else if (edPassword.getText().toString().isEmpty()){
                        edPassword.setError("Answer field can't be empty");
                    }else {
                        model.ForgotPassword(getApplicationContext(),ccp.getSelectedCountryCode()+edPhone.getText().toString(),txtQuest.getText().toString(),edPassword.getText().toString());


                    }
                    // Toast.makeText(this, "clicked "+CountryCode.replace("+","").trim()+edPhone.getText().toString(), Toast.LENGTH_SHORT).show();

                } else {
                    Utils.internetAlert(ForgotPassword.this);
                }
                break;
            case R.id.linkTologin:
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));

        }
    }

    public void ForgotResponse() {
        model = ViewModelProviders.of(this).get(LoginViewModel.class);
        model.init();
        model.getForgotPassword().observe(this, new Observer<ForgotResponse>() {
            @Override
            public void onChanged(ForgotResponse loginResponse) {
                Log.d("qwerty",loginResponse.getMessage());
                if (loginResponse.getCode() == 200) {
                    byte[] decodeValue = Base64.decode(loginResponse.getData(), Base64.DEFAULT);
                    String password=new String(decodeValue);
                    //Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                     Utils.popUp(ForgotPassword.this, "Your Password is "+password);
                } else {
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    //Utils.promptDialog(getApplicationContext(), loginResponse.getMessage());

                }

            }
        });
    }

}
