package com.instaplay.flictrick.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeleteAccount extends AppCompatActivity {

    RadioGroup radioGroup;
    TextView btnContinue,btnCancel;
    CheckBox option1, option2, option3, option4, option5;
    String selectedText;
    boolean isChecking;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    int mCheckedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);
       // radioGroup = findViewById(R.id.grp_radio);
        option1 = findViewById(R.id.radio1);
        option2 = findViewById(R.id.radio2);
        option3 = findViewById(R.id.radio3);
        option4 = findViewById(R.id.radio4);
        option5 = findViewById(R.id.radio5);
        btnContinue = findViewById(R.id.btnContinue);
        btnCancel = findViewById(R.id.btnCancel);
        toolbar = findViewById(R.id.toolbar);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),AccountSetting.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerifyPassword(DeleteAccount.this);

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

       /* option1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    option1.setChecked(true);
                }else {
                    option1.setChecked(false);

                }

            }
        });
        option2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    option2.setChecked(true);
                }else {
                    option2.setChecked(false);

                }
            }
        });
        option3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    option3.setChecked(false);
                }else {
                    option3.setChecked(true);

                }
            }
        });
        option4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    option4.setChecked(true);
                }else {
                    option4.setChecked(false);

                }
            }
        });
        option5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    option5.setChecked(true);
                }else {
                    option5.setChecked(false);

                }
            }
        });
*/




    }
    public  void selected(View view) {
        String msg = "";

        if (option1.isChecked()){
            msg= option1.getText().toString();
        }if (option2.isChecked()){
            msg= option2.getText().toString();
        }if (option3.isChecked()){
            msg= option3.getText().toString();
        }if (option4.isChecked()){
            msg= option4.getText().toString();
        }if (option5.isChecked()){
            msg= option5.getText().toString();
        }


    }

    public void VerifyPassword(final Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        //alert.setTitle("Password");
        alert.setMessage("Enter Your Password");
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 0, 30, 0);

        EditText edittext = new EditText(DeleteAccount.this);
        edittext.setBackgroundResource(R.drawable.text_bg);
        edittext.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        layout.addView(edittext, params);

        alert.setView(layout);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String YouEditTextValue = edittext.getText().toString();
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    PasswordVerify(YouEditTextValue);
                } else {
                    Utils.internetAlert(DeleteAccount.this);
                }

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();


    }

    public void PasswordVerify(String pass) {
        progressDialog = new ProgressDialog(DeleteAccount.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        Log.d("encryptedPass", new String(bytesEncoded));
        model.setPassword(new String(bytesEncoded));
        model.setSource("app");
        model.setToken(token);
        model.setUid(String.valueOf(UserId));

        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.VerifyPassword(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.body() != null) {
                    Log.d("RESPONSE", response.body().getMessage());
                    if (response.code() == 200) {
                        if (response.body().getMessage().equals("Success")) {
                            DeleteVerify();
                            // Utils.Alert(DeleteAccount.this,"Account Verified");
                        } else {
                            progressDialog.dismiss();
                            Utils.Alert(DeleteAccount.this, response.body().getMessage());
                        }

                    }


                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("Error4", t.getMessage());
                progressDialog.dismiss();
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }

    public void DeleteVerify() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        SharedPreferences prefs = getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        model.setSource("app");
        model.setToken(token);
        model.setUid(String.valueOf(UserId));

        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.DeleteUser(" Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.code() == 200) {
                        Log.d("DeleteRESPONSE", response.body().getMessage());
                        Utils.popUp(DeleteAccount.this, "Account Deleted Successfully");


                    }


                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("Error4", t.getMessage());
                progressDialog.dismiss();
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }

}