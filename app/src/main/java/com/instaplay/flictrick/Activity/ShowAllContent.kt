package com.instaplay.flictrick.Activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.PictureInPictureParams
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.MediaItem.SubtitleConfiguration
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.SingleSampleMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.util.MimeTypes
import com.google.common.collect.ImmutableList
import com.google.gson.GsonBuilder
import com.instaplay.flictrick.Adapter.AdapterVideo.EpisodeAdapter
import com.instaplay.flictrick.Adapter.AdapterVideo.EpisodeChildAdapter
import com.instaplay.flictrick.Adapter.AdapterVideo.ShowAllVideoAdapter
import com.instaplay.flictrick.Model.DeviceModel
import com.instaplay.flictrick.Model.Login.LoginResponse
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage
import com.instaplay.flictrick.Model.Video.SearchData
import com.instaplay.flictrick.Model.Video.SearchResponse
import com.instaplay.flictrick.Model.Video.SubCatData
import com.instaplay.flictrick.NetworkService.APIClient
import com.instaplay.flictrick.NetworkService.BaseClient
import com.instaplay.flictrick.R
import com.instaplay.flictrick.ViewModel.LoginViewModel
import com.instaplay.flictrick.ViewModel.VideoViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class ShowAllContent : AppCompatActivity(), ShowAllVideoAdapter.onClickListener,
    EpisodeChildAdapter.ClickListener {
    var gridViewShowAll: ExpandableHeightGridview? = null
    var allVideoAdapter: ShowAllVideoAdapter? = null
    var swipeRefreshLayout: SwipeRefreshLayout? = null
    var list: ArrayList<SubCatData>? = null
    var data: ArrayList<SearchData>? = null
    var episodeList: ArrayList<SearchData>? = null
    var toolbar: Toolbar? = null
    var txtCat: TextView? = null
    var loginViewModel:LoginViewModel?=null
    // var layoutVideo: LinearLayout? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var layoutShowAll: ConstraintLayout? = null
    private var videoViewModel: VideoViewModel? = null
    var recyclerView: RecyclerView? = null
    var cast: TextView? = null
    var txtDescription: TextView? = null
    var subscriptionPackage: SubscriptionPackage? = null
    var cardView: Toolbar? = null
    var title: String? = null
    var category: String? = null
    var description: String? = null
    var url: String? = null
    var Subtitle: String? = null
    var rootCategroyId: String? = null
    var subRootCatId: String? = null
    var type: String? = null
    //var videoPosition : Int?= null
    private var playbackPosition: Long = 0
    private var playWhenReady = true
    var exoPlayer: ExoPlayer? = null
    var playerView: StyledPlayerView? = null
    var episodeAdapter: EpisodeAdapter? = null
    private var currentPlayer: StyledPlayerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_view_video)
        recyclerView = findViewById(R.id.related_recyclerView)
        txtCat= findViewById(R.id.txtCategory)
        //swipeRefreshLayout= findViewById(R.id.swipe)
        layoutShowAll = findViewById(R.id.layoutShowAll)
        gridViewShowAll = findViewById(R.id.gridShowAll)
        playerView = findViewById(R.id.playerView)
        toolbar = findViewById(R.id.toolbar);
        txtDescription = findViewById(R.id.txtDescription)
         cast = findViewById(R.id.cast)
        toolbar?.setOnClickListener {
            if (fragmentManager.backStackEntryCount == 0) {
                finish()
            } else {
                fragmentManager.popBackStack()
            }
        }


        /*swipeRefreshLayout?.setOnRefreshListener(OnRefreshListener {
            swipeRefreshLayout?.setRefreshing(false)
            if (type == "ShowAll") {
                ShowllIntentResponse()
            }
        })*/

        runOnUiThread {
            type = intent.getStringExtra("type")
            Log.d("typeee", type!!)
            GetDeviceActive()
            if (type != null) {
                if (type == "ShowAll") {

                    ShowllIntentResponse()
                } else if (type == "related") {
                    layoutShowAll!!.visibility = View.GONE
                   // swipeRefreshLayout!!.visibility= View.GONE
                    // playerView!!.visibility = View.VISIBLE
                    setFullScreenListener()
                    ShowRelatedIntentResponse()
                }
            }
        }


    }

    @Suppress("DEPRECATION")
    fun enterPIPMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
            && packageManager
                .hasSystemFeature(
                    PackageManager.FEATURE_PICTURE_IN_PICTURE)
        ) {
            playbackPosition = exoPlayer!!.currentPosition
            playerView!!.useController = false
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val params = PictureInPictureParams.Builder()
                this.enterPictureInPictureMode(params.build())
            } else {
                this.enterPictureInPictureMode()
            }
        }
    }

    fun VideoRelatedContent(rootCatId: String) {
        videoViewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java)
        videoViewModel!!.init()
        videoViewModel!!.VideoRelatedContent().observe(this@ShowAllContent,
            object : Observer<SearchResponse?> {

                override fun onChanged(volumesResponse: SearchResponse?) {
                    //TODO("Not yet implemented")
                    if (volumesResponse != null) {
                        episodeList = volumesResponse?.data
                        if (type!!.equals("ShowAll")) {
                            Log.d("ABCD", "" + episodeList?.size + "rootCategory" + title +"rootId"+rootCatId)
                            if (title==null){
                                allVideoAdapter =
                                    ShowAllVideoAdapter(episodeList,
                                        applicationContext,
                                        rootCategroyId,
                                        subscriptionPackage,
                                        episodeList,
                                        rootCategroyId,
                                        this@ShowAllContent)
                                gridViewShowAll!!.adapter = allVideoAdapter
                            }else{
                                allVideoAdapter =
                                    ShowAllVideoAdapter(episodeList,
                                        applicationContext,
                                        title,
                                        subscriptionPackage,
                                        episodeList,
                                        rootCategroyId,
                                        this@ShowAllContent)
                                gridViewShowAll!!.adapter = allVideoAdapter
                            }

                        } else {
                            Log.d("urlTrailer2", url + " cat12" + category + "Id" + rootCatId)
                            episodeAdapter = EpisodeAdapter()
                            linearLayoutManager =
                                LinearLayoutManager(applicationContext,
                                    LinearLayoutManager.VERTICAL,
                                    false);
                            recyclerView!!.setLayoutManager(linearLayoutManager)
                            recyclerView!!.adapter = episodeAdapter
                            episodeAdapter!!.setResultForOtherCategory(episodeList,
                                applicationContext,
                                category, this@ShowAllContent, category,rootCatId)


                        }


                    }
                }
            })

        videoViewModel!!.GetSubCatRelatedContent(rootCatId)
    }

    fun ShowllIntentResponse() {
        data = intent.getParcelableArrayListExtra("AllData")
        category = intent.getStringExtra("category")
        rootCategroyId = intent.getStringExtra("rootCategoryId")
        subscriptionPackage = intent.getParcelableExtra("package")
        title = intent.getStringExtra("VideoTitle")
        Subtitle = intent.getStringExtra("subtitle")
        Log.d("ShowAllData",rootCategroyId +"title" +title)

        gridViewShowAll!!.expanded = true
        txtCat!!.text= title
        //titletxtShow!!.text = title
        if (rootCategroyId == null) {
            // Log.d("asd11",""+"title "+title);
            layoutShowAll!!.visibility = View.VISIBLE
            //swipeRefreshLayout!!.visibility= View.VISIBLE
            playerView!!.visibility = View.GONE
            allVideoAdapter =
                ShowAllVideoAdapter(data,
                    applicationContext,
                    title,
                    subscriptionPackage,
                    episodeList,
                    rootCategroyId,
                    this@ShowAllContent)
            gridViewShowAll!!.adapter = allVideoAdapter

        } else {
            layoutShowAll!!.visibility = View.VISIBLE
            playerView!!.visibility = View.GONE
            VideoRelatedContent(rootCategroyId!!)
        }


    }

    fun ShowRelatedIntentResponse() {
        category = intent.getStringExtra("category")
        Subtitle = intent.getStringExtra("subtitle")
        url = intent.getStringExtra("url_related")
        rootCategroyId = intent.getStringExtra("rootCategoryId")
        subRootCatId = intent?.getStringExtra("subRootCategoryId")
        data = intent.getParcelableArrayListExtra("Content_related")
        subscriptionPackage = intent.getParcelableExtra("aPackage_related")
        title = intent.getStringExtra("VideoCast")
        description = intent.getStringExtra("VideoDesc")
        cast!!.text = title
        txtDescription!!.text = description

        if (data!=null){
            //Log.d("urlTrailer1", url + "catId"+rootCategroyId+ " categ" + category + "Id" + data!!.size)
            episodeAdapter = EpisodeAdapter()
            linearLayoutManager =
                LinearLayoutManager(applicationContext,
                    LinearLayoutManager.VERTICAL,
                    false);
            recyclerView!!.setLayoutManager(linearLayoutManager)
            recyclerView!!.adapter = episodeAdapter
            episodeAdapter!!.setResultForOtherCategory(data,
                applicationContext,
                category, this@ShowAllContent, category,rootCategroyId)
        }else{
            VideoRelatedContent(rootCategroyId!!)
        }

    }


    private fun preparePlayer() {
        exoPlayer = ExoPlayer.Builder(applicationContext).setSeekBackIncrementMs(5000)
            .setSeekForwardIncrementMs(5000).build()
        exoPlayer?.let { exo ->
            exo.playWhenReady = true
            playerView?.player = exo
           // exo.setMediaSource(setMediaType())
            exo.seekTo(playbackPosition)
           // playerView?.setShowSubtitleButton(true)
            exo.playWhenReady = playWhenReady
            //SetMediaItem();



            val assetSrtUri = Uri.parse((Subtitle))
            val subtitle = SubtitleConfiguration.Builder(assetSrtUri)
                .setMimeType(MimeTypes.TEXT_VTT)
                .setLanguage("en")
                .setSelectionFlags(C.SELECTION_FLAG_AUTOSELECT)
                .build()

            val assetVideoUri = Uri.parse((url))
            val mediaItem = MediaItem.Builder()
                .setUri(assetVideoUri)
                .setSubtitleConfigurations(ImmutableList.of(subtitle))
                .build()

            exoPlayer!!.setMediaItem(mediaItem)
            exo.prepare()
        }
    }

    private fun releasePlayer() {
        currentPlayer?.player = null
        exoPlayer?.let { player ->
            playbackPosition = player.currentPosition
            playWhenReady = player.playWhenReady
            player.release()
            exoPlayer = null
        }
    }



    private fun setMediaType(): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        Log.d("SUBTILE", Subtitle.toString());
        val subtitle = MediaItem.SubtitleConfiguration.Builder(Uri.parse(Subtitle))
            .setMimeType(MimeTypes.TEXT_VTT) // The correct MIME type (required).
            .setLanguage("app") // MUST, The subtitle language (optional).
            .setSelectionFlags(C.SELECTION_FLAG_AUTOSELECT) //MUST,  Selection flags for the track (optional).
            .build()

        val subtitleSource = SingleSampleMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(subtitle, C.TIME_UNSET)


        val mediaItem: MediaItem = MediaItem.Builder()
            .setUri(url)//videouri
            .setSubtitleConfigurations(ImmutableList.of(subtitle))
            .build()

        val mediaSource = ProgressiveMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)

        // merge subtitle source and media source
        val mediaSourceWithsubtitle = MergingMediaSource(mediaSource, subtitleSource)
        exoPlayer?.setMediaSource(mediaSourceWithsubtitle)

        return mediaSource;

    }

    private fun SetMediaSorce():MediaSource{
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        val assetSrtUri = Uri.parse(("https://flictrick.com/come-around_english-subtitles_1698566612.vtt"))
        val subtitle = SubtitleConfiguration.Builder(assetSrtUri)
            .setMimeType(MimeTypes.TEXT_VTT)
            .setLanguage("en")
            .setSelectionFlags(C.SELECTION_FLAG_AUTOSELECT)
            .build()

        val assetVideoUri = Uri.parse((url))
        val mediaItem = MediaItem.Builder()
            .setUri(assetVideoUri)
            .setSubtitleConfigurations(ImmutableList.of(subtitle))
            .build()
        val mediaSource = ProgressiveMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)
        return mediaSource;
    }

    private fun GetDeviceActive() {
        val gson = GsonBuilder().setLenient().create()
        val prefs: SharedPreferences =
            getSharedPreferences("shared_prefs", Context.MODE_PRIVATE)
        val UserId = prefs.getInt("userId_key", 0)
        // String token = prefs.getString("token_key", "");
        val logoutModel = DeviceModel()
        logoutModel.uid = UserId.toString()
        logoutModel.deviceId = Utils.getDeviceId(this@ShowAllContent)
        logoutModel.source = "app"
       // Log.d("DeviceId", getDeviceId())
        val retrofit = Retrofit.Builder().baseUrl(BaseClient.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()
        val api = retrofit.create<APIClient>(APIClient::class.java)
        val responseCall =
            api.GetDeviceStatus("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", logoutModel)
        responseCall.enqueue(object : Callback<LoginResponse?> {
            override fun onResponse(
                call: Call<LoginResponse?>,
                response: Response<LoginResponse?>,
            ) {
                if (response.body() != null) {
                    //ProfileModel data = response.body().getData();
                    if (response.code() == 200) {
                        val data = response.body()!!.data
                        if (response.body()!!.message == "This deviceId is not active.Please logout.") {
                            Log.d("Check_Device_status",
                                response.body()!!.message + "token " + data.token)
                            SessionManagement(this@ShowAllContent).UpdateToken(data.token)
                            promptDialog(this@ShowAllContent, response.body()!!.message)
                        } else {
                            if (data.token != null) {
                                SessionManagement(this@ShowAllContent).UpdateToken(data.token)
                            }
                            Log.d("Login_status", data.token)
                            //Utils.Alert(getActivity(),response.body().getMessage());
                        }
                    }else {
                        Log.d("LoginError", "" + response.code())
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                Log.e("ErrorActiveDevice", t.message!!)
            }
        })
    }


    fun Logout() {
        loginViewModel = ViewModelProviders.of(this).get<LoginViewModel>(LoginViewModel::class.java)
        loginViewModel!!.init()
        loginViewModel!!.getLogoutUser().observe(this, object : Observer<LoginResponse?> {
            override fun onChanged(loginResponse: LoginResponse?) {
                if (loginResponse != null) {
                    if (loginResponse.code == 200) {
                        Toast.makeText( this@ShowAllContent, "Logout Successfully", Toast.LENGTH_SHORT)
                            .show()
                        SessionManagement( this@ShowAllContent).LogoutDetails()
                        //startActivity(new Intent(getActivity(), LoginActivity.class));
                    } else {
                        Utils.promptDialog( this@ShowAllContent, loginResponse.message)
                    }
                }
            }
        })
        loginViewModel!!.LogoutUser( this@ShowAllContent)
    }


    fun promptDialog(context: Context?, resMsg: String?) {
        val errorDialog = AlertDialog.Builder(
            context!!, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
            .setCancelable(false)
        // errorDialog.setTitle("Message");
        errorDialog.setMessage(resMsg)
        errorDialog.setPositiveButton("OK"
        ) { dialog, id -> Logout() }
        val alert11 = errorDialog.create()
        alert11.show()
    }
    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT <= 23) {
            if (type?.equals("related") == true) {
                preparePlayer()
                currentPlayer?.onResume()
            }

        }
    }

    override fun onStop() {
        super.onStop()
        if (type?.equals("related") == true) {
            releasePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (type?.equals("related") == true) {
           // Log.d("BackClicked","hello")
            releasePlayer()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (type?.equals("related") == true) {
            releasePlayer()
        }
    }

    override fun onStart() {
        super.onStart()
        if (type?.equals("related") == true) {
            preparePlayer()
        }

    }

    private fun setFullScreenListener() {
        // Creating a new Player View and place it inside a Full Screen Dialog.
        val fullScreenPlayerView = StyledPlayerView(applicationContext)
        fullScreenPlayerView.setShowNextButton(false)
        fullScreenPlayerView.setShowPreviousButton(false)
        val dialog = object :
            Dialog(this@ShowAllContent, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            @Deprecated("Deprecated in Java")
            override fun onBackPressed() {
                Log.d("BackPressClicked","yes")
                if (type?.equals("related") == true){
                    VideoRelatedContent(rootCategroyId!!)
                }
                // User pressed back button. Exit Full Screen Mode.
                playerView?.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_fullscreen)
                    ?.setImageResource(R.drawable.ic_baseline_fullscreen_24)
                exoPlayer?.let {
                    StyledPlayerView.switchTargetView(it,
                        fullScreenPlayerView,
                        playerView)
                }
                currentPlayer = playerView
                this@ShowAllContent.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                super.onBackPressed()
            }

        }
        dialog.addContentView(
            fullScreenPlayerView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        // Adding Full Screen Button Click Listeners.
        playerView?.setFullscreenButtonClickListener {
            // If full Screen Dialog is not visible, make player full screen.
            if (!dialog.isShowing) {
                dialog.show()
                fullScreenPlayerView.findViewById<ImageButton>(com.google.android.exoplayer2.ui.R.id.exo_fullscreen)
                    .setImageResource(R.drawable.ic_baseline_fullscreen_exit_24)
                exoPlayer?.let {
                    StyledPlayerView.switchTargetView(it,
                        playerView,
                        fullScreenPlayerView)/////////////////////////
                }
                currentPlayer = fullScreenPlayerView
                this@ShowAllContent.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (fragmentManager.backStackEntryCount == 0) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                && packageManager
                    .hasSystemFeature(
                        PackageManager.FEATURE_PICTURE_IN_PICTURE)){
                enterPIPMode()
            }
            finish()
        } else {
            fragmentManager.popBackStack()
        }
    }

    /*override fun listener(s: String, title: String, des: String){
        if (s != null) {
            url = s
            // Log.d("OtherCat", "" + url)
            titletxt!!.text = title
            txtDescription!!.text = des
            if (type?.equals("related") == true) {
                if (exoPlayer != null) {
                    exoPlayer!!.release()
                    preparePlayer()
                    currentPlayer?.onResume()
                }
            }

        }
    }*/

    override fun listener(urlPath: String?) {
        //TODO("Not yet implemented")
    }


    @SuppressLint("SuspiciousIndentation")
    override fun onClickListener(
        s: String?,
        title: String?,
        des: String?,
        rootId: String?,
        subCat: String?,
    ) {
         cast!!.text = title
        txtDescription!!.text = des
        url = s
        if (exoPlayer != null) {
            exoPlayer!!.release()
            preparePlayer()
            currentPlayer?.onResume()

        }
        Log.d("shortFilmClick", url.toString())
        //TODO("Not yet implemented")
    }

    override fun ClickListener(path: String?, title: String?, des: String?) {
        cast!!.text = title
        txtDescription!!.text = des
        url = path
        if (exoPlayer != null) {
            exoPlayer!!.release()
            preparePlayer()
            currentPlayer?.onResume()

        }
    }


}