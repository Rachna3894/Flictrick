package com.instaplay.flictrick.Activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.Fragment.SearchFragment;
import com.instaplay.flictrick.Activity.Fragment.VideoFragment;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Interface.DialogInterface;
import com.instaplay.flictrick.Interface.TabListener;
import com.instaplay.flictrick.Model.Category.CategoryModel;
import com.instaplay.flictrick.Model.Category.CategoryResponse;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.Model.Subscription.SubscriptionRequest;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.Model.Subscription.SubscriptionResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.CategoryViewModel;
import com.instaplay.flictrick.ViewModel.LoginViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    VideoFragment homeFragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    CategoryViewModel categoryViewModel;
    List<CategoryModel> list;
    public TabLayout tabLayout;
    List<String> categoryList;
    public SearchView searchView;
    private EditText editText;
    ViewPager viewPager;
    LoginViewModel loginViewModel;
    SubscriptionPackage subscriptionPackage;
    ImageView btnProfile,btnSubmit;
    int position, UserId;
    String token,dialogMsg;
    ProgressDialog dialog;
    public static CardView cardView;
    public static final int MobileData = 2;
    public static final int WifiData = 1;
    TabListener listener;
    DialogInterface dialogInterface;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        tabLayout = findViewById(R.id.FtabLayout);
        btnProfile = findViewById(R.id.btnProfile);
        cardView = findViewById(R.id.cardview);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        editText= findViewById(R.id.search);
         btnSubmit= findViewById(R.id.btnSubmit);
        //createNotification("27-03-2024");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SearchFragment fragment = new SearchFragment();
                Bundle bundle = new Bundle();
                bundle.putString("SearchWord",editText.getText().toString());
                bundle.putParcelable("Package",subscriptionPackage);
                fragment.setArguments(bundle);

                // SearchFragment fragment = SearchFragment.newInstance("Hello",newText);
                fragmentTransaction.replace(R.id.flFragment, fragment).addToBackStack(fragment.getTag());
                //fragmentTransaction.replace(R.id.frameLayout, fragment).addToBackStack(fragment.getTag());
                fragmentTransaction.commit();
            }
        });

        //listener = (TabListener)HomeActivity.this;


        bottomNavigationView
                = findViewById(R.id.nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        homeFragment = new VideoFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flFragment, homeFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();

        OnClickTab(tabLayout);
        //CategoryAPI();


        /// background connection check//
        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case WifiData:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //GetDeviceActive();
                                    CategoryAPI();
                                    GetSubscriptionPlan();
                                    SubscriptionActiveAndNot(token, String.valueOf(UserId));

                                }
                            });
                            break;
                        case MobileData:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run(){
                                    //GetDeviceActive();
                                    CategoryAPI();
                                    GetSubscriptionPlan();
                                    SubscriptionActiveAndNot(token, String.valueOf(UserId));

                                }
                            });
                            break;
                    }
                } else {
                    Utils.internetAlert(HomeActivity.this);
                }
            }
        });

        SharedPreferences prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        UserId = prefs.getInt("userId_key", 0);
        token = prefs.getString("token_key", "");

       /* AccessToken accessToken= AccessToken.getCurrentAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (object != null) {
                            try {
                                String name = object.getString("name");
                                String email = object.getString("email");
                                String fbUserID = object.getString("id");
                                Log.d("qwertyLogin", name + " asd" + email + " id" + fbUserID);
                                //disconnectFromFacebook();

                                // do action after Facebook login success
                                // or call your API
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString(
                "fields",
                "id, name, email, gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
*/


        // CategoryAPI();


        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });


    }




    private void GetDeviceActive(){
        SharedPreferences prefs = getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("uid", String.valueOf(UserId));
        params.put("source", "app");
        params.put("deviceId", Utils.getDeviceId(getApplicationContext()));
        Log.d("DeviceId",Utils.getDeviceId(getApplicationContext()));
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, "https://flictrick.com/api/user/get_device_status.php", new JSONObject(params), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response!=null) {
                    try {
                        //dialog.dismiss();
                        String message = response.getString("message");
                        Log.d("Device status",message);
                        if (message.equals("This deviceId is not active.Please logout")){
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
                            Utils.promptDialog(getApplicationContext(),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //dialog.dismiss();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }



    public void SubscriptionActiveAndNot(String token, String UserId) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LogoutModel model = new LogoutModel();
        model.setToken(token);
        model.setUid(UserId);
        model.setSource("app");
        // byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.SubscriptionStatus("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.body().getCode() == 200) {
                    Log.d("Qwerty12", response.body().getMessage());
                    //Utils.promptDialog(HomeActivity.this, response.body().getMessage());
                    new SessionManagement(getApplicationContext()).updateSubscriptionstatus(response.body().getMessage());

                } else if (response.body().getMessage().contains("No Data Found")) {
                    new SessionManagement(getApplicationContext()).updateSubscriptionstatus("Subscription Is not Active");
                    // Utils.promptDialog(HomeActivity.this, response.body().getMessage());
                    Log.d("QwertyNoActive", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
               // Log.e("Subscription", t.getMessage());
                if (t.getMessage().contains("Failed to connect to flictrick.com/45.32.66.180:443")||t.getMessage().contains("timed out")) {
                    SubscriptionActiveAndNot(token, String.valueOf(UserId));
                }

            }
        });
    }

    public void createNotification(String date) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(this, VideoFragment.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Your flictrick subscription gone expire on "+date)
                .setContentText("Flictrick").setSmallIcon(R.drawable.icon)
                .setContentIntent(pIntent)
                .addAction(R.drawable.ic_baseline_notifications_24, "Subscription Reminder", pIntent).
                build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, noti);

    }



  /*  public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions/",
                null,
                HttpMethod.DELETE,
                new GraphRequest
                        .Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {
                        LoginManager.getInstance().logOut();
                    }
                })
                .executeAsync();
    }*/

    public void OnClickTab(TabLayout tabLayout) {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position = tab.getPosition();
                // homeFragment =  HomeFragment.newInstance(listener);
                listener.onClick(position, tab.getText().toString());
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.flFragment, homeFragment);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.commit();
                Log.d("TabPosition", "" + position);
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void passVal(TabListener fragmentCommunicator) {
        this.listener = fragmentCommunicator;

    }

    public void passDialog(DialogInterface dialogInterface){
        this.dialogInterface= dialogInterface;
    }





    public void CategoryAPI() {
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        categoryViewModel.init();
        tabLayout.removeAllTabs();
        // tabLayout.addTab(tabLayout.newTab().setText(null));
        categoryViewModel.getPreferenceCategory().observe(this, new Observer<CategoryResponse>() {
            @Override
            public void onChanged(CategoryResponse categoryResponse) {
                if (categoryResponse != null) {
                    list = categoryResponse.getData();

                    if (list != null) {
                        categoryList = new ArrayList<>();
                        categoryList.add("Home");
                        for (int i = 0; i < list.size(); i++) {
                            categoryList.add(list.get(i).getRootCategoryName());
                            //list.add(categoryList.get(i).getRootCategoryName());
                        }
                        for (int j = 0; j < categoryList.size(); j++) {
                            tabLayout.addTab(tabLayout.newTab().setText(categoryList.get(j)));

                        }

                        //categoryList.clear();
                        // Log.d("preferenceGet", "" + list.size());

                        // categoryList.add(new CategoryModel(list.get(i).getId(),list.get(i).getRootCategoryName(),list.get(i).getStatus()));

                    }

                }

              /*  TabAdapter adapter = new TabAdapter(getApplicationContext(), getSupportFragmentManager(), tabLayout.getTabCount());
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(1);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));*/
            }


        });
        categoryViewModel.CategoryPreference(getApplicationContext());

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        switch (item.getItemId()) {
            case R.id.navigation_home:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.flFragment, VideoFragment.newInstance())
                        .commit();
                break;

            case R.id.navigation_plans:
                Intent intent = new Intent(getApplicationContext(), SubscriptionActivity.class);
                intent.putExtra("package", subscriptionPackage);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;


            case R.id.navigation_account:
                Intent in= new Intent(getApplicationContext(),AccountSetting.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
                break;


        }
        return false;
    }


    public void GetSubscriptionPlan() {
        //progressDialog = new ProgressDialog(context);
        //progressDialog.setMessage("Loading");
        //progressDialog.setCancelable(false);
        // progressDialog.show();
        Gson gson = new GsonBuilder().setLenient().create();
        String country = new SessionManagement(getApplicationContext()).GetUserCountry();
        SubscriptionRequest subscription = new SubscriptionRequest();
        subscription.setSource("app");
        subscription.setCountry(country);
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BaseClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        APIClient api = retrofit.create(APIClient.class);
        Call<SubscriptionResponse> responseCall = api.SubscriptionPlan("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", subscription);
        responseCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                if (response.body() != null) {
                    subscriptionPackage = response.body().getData();
                    //adapter.renewItems(getActivity(), bannerModelList, "Banner",subscriptionPackage);
                    //country = subscriptionResponse.getCountry();
                    Log.d("subscriptionPackage", response.body().getMessage());
                    //volumesResponseLiveData.postValue(response.body());
                    //isLoading.postValue(true);
                }
            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                Log.e("Error1", t.getMessage());
                // volumesResponseLiveData.postValue(null);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {
                    //Getvideo("4");
                } else if (t.getMessage().contains("failed to connect to sportapi.clicknplay24.com/139.59.67.0 (port 443)")) {
                    //Getvideo("4");
                }

            }
        });
    }



}