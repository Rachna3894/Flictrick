package com.instaplay.flictrick.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbb20.CountryCodePicker;
import com.instaplay.flictrick.Adapter.CustomAdapter;
import com.instaplay.flictrick.Model.Login.LoginData;
import com.instaplay.flictrick.Model.Login.LoginModel;
import com.instaplay.flictrick.Model.Login.LoginResponse;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.instaplay.flictrick.ViewModel.LoginViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    LoginViewModel model;
    Spinner spinner;
    TextView spin;
    String CountryCode, IpAddress;
    CustomAdapter adapter;
    ProgressDialog progressDialog;
    EditText edPhone, edPassword;
    CountryCodePicker ccp;
    String pack = "",expireyDate="";
    TextView btnlogin, btnlinkRegister, btnForgot;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnlinkRegister = findViewById(R.id.linkRegister);
        edPhone = findViewById(R.id.ed_phone);
        edPassword = findViewById(R.id.ed_pass);
        btnlogin = findViewById(R.id.btnlogin);
        btnForgot = findViewById(R.id.btnForgot);
        ccp = findViewById(R.id.ccp);
        //spinner = findViewById(R.id.spinCode);
        btnlogin.setOnClickListener(this);
        btnlinkRegister.setOnClickListener(this);
        btnForgot.setOnClickListener(this);

        /*whatsApp variables
        setContentView(R.layout.activity_login_via_whatsapp);
        btnVerify = findViewById(R.id.btnVerify);
        edOtp = findViewById(R.id.edOtp);
        edNumber = findViewById(R.id.ed_number);
        spinner = findViewById(R.id.spinCode);
        txttimer= findViewById(R.id.timer);
        btnVerify.setOnClickListener(this);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        Login()*/

        GetIpAddress();

        //ccp.setCcpClickable(false);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                Log.d("PhoneCOde", ccp.getSelectedCountryCode());
            }
        });
        ccp.setDialogBackgroundColor(R.color.white);
        //for country code display


    }


    public void LoginWhileSubscribeDataNotGot(String phone, String pass) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        //String token = prefs.getString("token_key", "");
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobile", phone);
        params.put("deviceId", Utils.getDeviceId(getApplicationContext()));
        params.put("source", "app");
        Log.d("deviceId", Utils.getDeviceId(getApplicationContext()));
        byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        params.put("password", new String(bytesEncoded).trim());
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, "https://flictrick.com/api/user/login.php", new JSONObject(params), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response != null) {
                    try {
                        progressDialog.dismiss();

                        String pack = "",token="";
                        JSONObject jsonData = response.getJSONObject("data");
                        String email = jsonData.getString("email");
                        String status = jsonData.getString("status");
                        token = jsonData.getString("token");
                        String id = jsonData.getString("id");
                        String phone = jsonData.getString("mobile");

                        if (!token.equals("")) {
                            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                            new SessionManagement(getApplicationContext()).LoginDetails(token, Integer.parseInt(id), true, email, phone, status);

                        }


                    } catch (JSONException e) {

                        e.printStackTrace();

                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("errorLogin", error.getMessage());
                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
       /* JSONObject jsonArray = jsonData.getJSONObject("Subscription");
                         pack = jsonArray.getString("pack");

                        if (pack!=""){
                            Log.d("Pack", jsonArray.getString("pack"));

                            new SessionManagement(getApplicationContext()).LoginData(pack);
                        }*/
    }

    public void LoginUser(String phoneNumber, String password, Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LoginModel model = new LoginModel();
        model.setMobile(phoneNumber);
        byte[] bytesEncoded = Base64.encode(password.getBytes(), 0);
        model.setPass(new String(bytesEncoded).trim());
        model.setSource("app");
        model.setDeviceId(Utils.getDeviceId(context));
        Log.d("DeviceId", Utils.getDeviceId(context) + " PASS" + password);
        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.UserLoginRestrict("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, retrofit2.Response<RegisterResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getMessage().equals("You only allow to login on two devices at a same time.Kindly logout from other devices.")) {
                        Log.d("resMsg12", "" + response.body().getMessage());
                        //promptDialog(getApplicationContext(),response.body().getMessage());
                        ForceAlert(LoginActivity.this, response.body().getMessage());
                    } else {
                        Log.d("resMsg13", "" + response.body().getMessage());
                        Utils.Alert(LoginActivity.this, response.body().getMessage());
                    }
                } else if (response.code() == 500) {
                    Log.d("ServerError", "" + response.code());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("Error3", t.getMessage());
                Login(phoneNumber, password);
                // isLoading.postValue(false);
                if (t.getMessage().equals("timeout")) {

                }
            }
        });
    }

    private void Login(String phone, String pass) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("shared_prefs", Context.MODE_PRIVATE);
        String token = prefs.getString("token_key", "");
        Log.d("CustomerID", token);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("mobile", phone);
        params.put("deviceId", Utils.getDeviceId(getApplicationContext()));
        params.put("source", "app");
        Log.d("deviceId", Utils.getDeviceId(getApplicationContext()));
        byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        params.put("password", new String(bytesEncoded).trim());
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, "https://flictrick.com/api/user/login.php", new JSONObject(params), new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (response != null) {
                    try {
                        progressDialog.dismiss();
                        String token = "";
                        JSONObject jsonData = response.getJSONObject("data");
                        String email = jsonData.getString("email");
                        String status = jsonData.getString("status");
                        token = jsonData.getString("token");
                        String id = jsonData.getString("id");
                        String phone = jsonData.getString("mobile");
                        JSONObject jsonArray = jsonData.getJSONObject("Subscription");
                         pack = jsonArray.getString("pack");
                        expireyDate = jsonArray.getString("subscriptionEndDate");
                        if (pack!=""){
                            Log.d("Pack", jsonArray.getString("pack")  +" date "+expireyDate);
                            new SessionManagement(getApplicationContext()).LoginData(pack,expireyDate);
                        }
                        Log.d("token ", token);
                        if (!token.equals("")){
                            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                            new SessionManagement(getApplicationContext()).LoginDetails(token, Integer.parseInt(id), true, email, phone, status);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LoginWhileSubscribeDataNotGot(phone, pass);
                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("errorLogin", error.getMessage());
                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    public void promptDialog(final Context context, String resMsg) {
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert).setCancelable(true);
        builder1.setTitle("Message");
        builder1.setCancelable(true);
        builder1.setMessage(resMsg);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ForceLogout(CountryCode.replace("+", "").trim() + edPhone.getText().toString(), edPassword.getText().toString(), getApplicationContext());

                    }
                });


    }


    public void ForceLogout(String phoneNumber, String password, Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Log.d("DeviceId", phoneNumber + " PASS" + password);
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LoginModel model = new LoginModel();
        model.setMobile(phoneNumber);
        byte[] bytesEncoded = Base64.encode(password.getBytes(), 0);
        model.setPass(new String(bytesEncoded).trim());
        model.setSource("app");
        model.setDeviceId(Utils.getDeviceId(context));
        APIClient api = retrofit.create(APIClient.class);
        Call<LoginResponse> responseCall = api.ForceLogoutUser("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        LoginData data = response.body().getData();
                        Log.d("ForceError", data.getToken());
                        new SessionManagement(getApplicationContext()).LoginDetails(data.getToken(), Integer.parseInt(data.getId()), true, data.getEmail(), data.getEmail(), data.getStatus());
                        Log.d("ForceLogout", response.body().getMessage());
                    } else {
                        Log.d("InternalServer", "" + response.code());
                    }


                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ForceLogoutError", t.getMessage());
                Login(phoneNumber, password);
            }
        });
    }

    public void ShowHidePass(View view) {

        if (view.getId() == R.id.show_pass_btn) {

            if (edPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                ((ImageView) (view)).setImageResource(R.drawable.hide_password);

                //Show Password
                edPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                ((ImageView) (view)).setImageResource(R.drawable.show_password);

                //Hide Password
                edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
    }

    private void GetIpAddress() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://httpbin.org/ip", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    IpAddress = object.getString("origin");
                    GetUserLocationCountry(IpAddress);
                    Log.d("IPAddress", IpAddress);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void GetUserLocationCountry(String ipAddress) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://api.ip2location.io/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    String countryName = object.getString("country_name");
                    Log.d("countryName", countryName);
                    new SessionManagement(getApplicationContext()).UpdateUserCountry(countryName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("key", "D181D30A73D19967EB2F9DFD80C567DD");
                headers.put("ip", ipAddress);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


/*
    public void VerifyPhone(String phoneNumber, String deviceId) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL_LOGIN)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        LoginModel model = new LoginModel();
        model.setMobile(phoneNumber);
        model.setChannel("whatsapp");
        model.setDeviceId(deviceId);
        // byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        model.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<PhoneNumberVerifyResponseModel> responseCall = api.SendOtp("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<PhoneNumberVerifyResponseModel>() {
            @Override
            public void onResponse(Call<PhoneNumberVerifyResponseModel> call, Response<PhoneNumberVerifyResponseModel> response) {
                Log.d("data", response.body().getData());
                if (response.body().getMessage().contains("OTP Sent")) {
                    // layoutEmail.setVisibility(View.VISIBLE);
                    // layoutOTP.setVisibility(View.GONE);
                    Log.d("Qwerty12", response.body().getMessage());
                    Utils.promptDialog(LoginActivity.this, response.body().getMessage());

                }else {
                    Utils.promptDialog(LoginActivity.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PhoneNumberVerifyResponseModel> call, Throwable t) {
                Log.e("ErrorEmail", t.getMessage());

            }
        });
    }

    CountryCode.substring(0, CountryCode.length()) + "" +
*/

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:

                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    //Log.d("checkvalid", String.valueOf(isValidPhoneNumber(ccp.getSelectedCountryCode() + edPhone.getText().toString())) +"countrycode"+ccp.getSelectedCountryCode());
                    boolean status = Utils.isValidPhoneNumber("+" + ccp.getSelectedCountryCode() + edPhone.getText().toString(), edPhone);
                    if (!status) {
                        edPhone.setError("Please check the phone number or country code");
                    } else if (edPassword.getText().toString().isEmpty()) {
                        edPassword.setError("Password field can't be empty");
                    } else {
                        LoginUser(ccp.getSelectedCountryCode() + edPhone.getText().toString(), edPassword.getText().toString().trim(), getApplicationContext());

                    }

                } else {
                    Utils.internetAlert(LoginActivity.this);
                }
                break;

            case R.id.linkRegister:
                startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));

                break;
            case R.id.btnForgot:
                startActivity(new Intent(getApplicationContext(), ForgotPassword.class));

                break;


        }
    }

    public void ForceAlert(final Context context, String msg) {
        final AlertDialog.Builder errorDialog = new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert)
                .setCancelable(false);
        errorDialog.setTitle("Message");
        errorDialog.setMessage(msg);
        errorDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("EnterDialog", "ok");
                        ForceLogout(ccp.getSelectedCountryCode() + edPhone.getText().toString(), edPassword.getText().toString(), getApplicationContext());

                    }
                });
        AlertDialog alert11 = errorDialog.create();
        alert11.show();

    }
}

