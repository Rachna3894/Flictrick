package com.instaplay.flictrick.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionLiveData;
import com.instaplay.flictrick.Activity.InternetConnection.ConnectionModel;
import com.instaplay.flictrick.Model.LogoutModel;
import com.instaplay.flictrick.Model.Payment.PaymentData;
import com.instaplay.flictrick.Model.Payment.PaymentDoneResponse;
import com.instaplay.flictrick.Model.Payment.PaymentModel;
import com.instaplay.flictrick.Model.Payment.PaymentRequest;
import com.instaplay.flictrick.Model.Payment.PaymentResponse;
import com.instaplay.flictrick.Model.Register.RegisterResponse;
import com.instaplay.flictrick.Model.Subscription.SubscriptionPackage;
import com.instaplay.flictrick.NetworkService.APIClient;
import com.instaplay.flictrick.NetworkService.BaseClient;
import com.instaplay.flictrick.R;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.googlepaylauncher.GooglePayEnvironment;
import com.stripe.android.googlepaylauncher.GooglePayLauncher;
import com.stripe.android.paymentsheet.PaymentSheet;
import com.stripe.android.paymentsheet.PaymentSheetResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubscriptionActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtmonthly, txtyearly, btnmonthly, btnyearly, txmonthly, txyearly, currency, year_currency;
    PaymentSheet paymentSheet;
    GooglePayLauncher googlePayLauncher;
    CardView cardMonthly, cardYearly;
    String priceType, Package="", token, email, Phnumber, currentDate, expiryDate, Currency,PaymentStatus="";
    int Uid;
    Toolbar toolbar;
    ProgressDialog dialog;
    boolean isConnect= false;
    public static final int MobileData = 2;
    public static final int WifiData = 1;
    PaymentSheet.GooglePayConfiguration googlePayConfiguration;
    private static final String TAG = "SubscriptionActivity";
    private String ClientSecretKey, CustomerID, EphericalKey,P_Id;
    SharedPreferences prefs;
    SubscriptionPackage subscriptionPackage=null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    //String Secret_key = "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";
    String Secret_key ;
    String Publisher_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        toolbar = findViewById(R.id.toolbar);
        txmonthly = findViewById(R.id.monthly);
        txyearly = findViewById(R.id.yearly);
        txtmonthly = findViewById(R.id.monthly_price);
        txtyearly = findViewById(R.id.yearly_price);
        btnmonthly = findViewById(R.id.btnMonthly);
        btnyearly = findViewById(R.id.btnYearly);
        cardMonthly = findViewById(R.id.cardMonthly);
        cardYearly = findViewById(R.id.cardYearly);
        currency = findViewById(R.id.txtcurrency);
        year_currency = findViewById(R.id.year_currency);
        cardMonthly.setOnClickListener(this);
        cardYearly.setOnClickListener(this);
        subscriptionPackage = getIntent().getParcelableExtra("package");
       // if (!subscriptionPackage.getCountry().isEmpty()){



        try {
            if (subscriptionPackage.getCountry()!=null){
                txtmonthly.setText(subscriptionPackage.getMonthly());
                txtyearly.setText(subscriptionPackage.getYearly());
                currency.setText(subscriptionPackage.getCurrency());
                year_currency.setText(subscriptionPackage.getCurrency());
            }
        }catch (NullPointerException e)
        {
            e.printStackTrace();
            SubscriptionActiveAndNot();
        }


        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });



        Publisher_key= getString(R.string.Publisher_key);
        PaymentConfiguration.init(this, Publisher_key);
        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()){
                        case WifiData:
                            isConnect=connection.getIsConnected();
                            /*dialog= new ProgressDialog(SubscriptionActivity.this);
                            dialog.setTitle("Please wait");
                            dialog.setMessage("Loading...");
                            dialog.setCancelable(false);
                            dialog.show();*/

                            break;
                        case MobileData:
                            isConnect=connection.getIsConnected();
                           /* dialog= new ProgressDialog(SubscriptionActivity.this);
                            dialog.setTitle("Please wait");
                            dialog.setMessage("Loading...");
                            dialog.setCancelable(false);
                            dialog.show();*/

                            break;
                    }
                } else {
                    isConnect=connection.getIsConnected();
                    Utils.internetAlert(SubscriptionActivity.this);
                }
            }
        });


       /* if (isConnect){
            GetCustomerId(dialog);
        }else{
            Utils.internetAlert(SubscriptionActivity.this);
        }*/



        paymentSheet = new PaymentSheet(this, paymentSheetResult -> {
            onPaymentResult(paymentSheetResult);
        });

        googlePayConfiguration =
                new PaymentSheet.GooglePayConfiguration(
                        PaymentSheet.GooglePayConfiguration.Environment.Production,
                        "US"
                );
        googlePayLauncher = new GooglePayLauncher(
                this,
                new GooglePayLauncher.Config(
                        GooglePayEnvironment.Production,
                        "US",
                        "Flictrick"
                ),
                this::onGooglePayReady,
                this::onGooglePayResult
        );

    }


    private void onGooglePayReady(boolean isReady) {
        btnmonthly.setEnabled(isReady);
        btnyearly.setEnabled(isReady);
        // implemented below
    }

    private void onGooglePayResult(@NotNull GooglePayLauncher.Result result) {
        if (result instanceof GooglePayLauncher.Result.Completed) {
            // Payment succeeded, show a receipt view
        } else if (result instanceof GooglePayLauncher.Result.Canceled) {
            // User canceled the operation
        } else if (result instanceof GooglePayLauncher.Result.Failed) {
            // Operation failed; inspect `result.getError()` for more details
        }
    }

    public void SubscriptionActiveAndNot() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        // volumesResponseLiveData = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        SharedPreferences prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
       int UserId = prefs.getInt("userId_key", 0);
        String token = prefs.getString("token_key", "");
        LogoutModel model = new LogoutModel();
        model.setToken(token);
        model.setUid(String.valueOf(UserId));
        model.setSource("app");
        // byte[] bytesEncoded = Base64.encode(pass.getBytes(), 0);
        APIClient api = retrofit.create(APIClient.class);
        Call<RegisterResponse> responseCall = api.SubscriptionStatus("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", model);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, retrofit2.Response<RegisterResponse> response) {
                if (response.body().getCode() == 200) {
                    Log.d("SubscriptionStatus", response.body().getMessage());
                    new SessionManagement(getApplicationContext()).updateSubscriptionstatus(response.body().getMessage());
                } else if (response.body().getMessage().contains("No Data Found")) {
                    new SessionManagement(getApplicationContext()).updateSubscriptionstatus("Subscription Is not Active");
                    Log.d("SubscriptionStatus12", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                // Log.e("Subscription", t.getMessage());
                if (t.getMessage().contains("Failed to connect to flictrick.com/45.32.66.180:443")) {
                    SubscriptionActiveAndNot();
                }

            }
        });
    }


    private void paymentFlow() {
        paymentSheet.presentWithPaymentIntent(
                ClientSecretKey,  // Client SecretKey
                new PaymentSheet.Configuration(
                        "Flictrick",
                        new PaymentSheet.CustomerConfiguration(
                                CustomerID, EphericalKey), googlePayConfiguration));

        SavePaymentDetailsBeforeDone();

    }

    public void GetCustomerId(ProgressDialog progressDialog) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/customers", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    CustomerID = object.getString("id");
                    Log.d("CustomerID",CustomerID);
                    //Toast.makeText(SubscriptionActivity.this, CustomerID, Toast.LENGTH_SHORT).show();
                    new SessionManagement(getApplicationContext()).SaveCustomerId(CustomerID);
                    getEphemeralKey(CustomerID,progressDialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " +Secret_key);
                return headers;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void getEphemeralKey(String customerID, ProgressDialog progressDialog) {
       // Log.d("CustomerID", customerID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/ephemeral_keys", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    EphericalKey = object.getString("id");
                    Log.d("qwerty", EphericalKey);
                    progressDialog.dismiss();
                    getClientSecret(customerID, priceType);
                    //Toast.makeText(SubscriptionActivity.this, EphericalKey, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SubscriptionActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " +Secret_key);
                headers.put("Stripe-Version", "2023-08-16");//header
                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", CustomerID);//passing customer in the form endcoded url body
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void getClientSecret(String customerID, String subscription_type) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/payment_intents", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);

                    ClientSecretKey = object.getString("client_secret");
                    P_Id= object.getString("id");
                    if (ClientSecretKey != null) {
                        paymentFlow();
                    }

                   // Toast.makeText(SubscriptionActivity.this, ClientSecretKey, Toast.LENGTH_SHORT).show();
                    Log.d("qwerty123", ClientSecretKey);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Secret_key= getString(R.string.Secret_key);
                headers.put("Authorization", "Bearer " + Secret_key);
                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                int price = Integer.parseInt(priceType);
                int val = price * 100;
                Log.d("CustomerID1",customerID);
                params.put("customer", customerID);
                params.put("amount", String.valueOf(val));
                params.put("currency", Currency);
               // params.put("automatic_payment_methods[enabled]", "true");
                Log.d("Price", String.valueOf(val));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }


    public void onPaymentResult(PaymentSheetResult paymentSheet) {
        if (paymentSheet instanceof PaymentSheetResult.Canceled) {
            Log.d(TAG, "Canceled");
           // UpdatePaymentDetailsAfterDone("Canceled");
            //showAlertDialogButtonClicked("Canceled", priceType, Package);
        } else if (paymentSheet instanceof PaymentSheetResult.Failed) {
            showAlertDialogButtonClicked("Failed", priceType, Package);
            UpdatePaymentDetailsAfterDone("Failed");
            Log.e(TAG, "Got_error: ", ((PaymentSheetResult.Failed) paymentSheet).getError());
        } else if (paymentSheet instanceof PaymentSheetResult.Completed) {
            showAlertDialogButtonClicked("Success", priceType, Package);
            UpdatePaymentDetailsAfterDone("Success");
            Log.d(TAG, "Completed");
            //Log.d(TAG,PaymentSheetResult.);
        }
    }


    public void showAlertDialogButtonClicked(String status, String price, String Package) {
        // Create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        //builder.setTitle("Name");
        final View customLayout = getLayoutInflater().inflate(R.layout.payment_popup, null);
        builder.setView(customLayout);
        ImageView imageView = customLayout.findViewById(R.id.img);
        TextView paymentMsg = customLayout.findViewById(R.id.paymentMsg);
        TextView paymentprice = customLayout.findViewById(R.id.paymentprice);
        TextView paymentDone = customLayout.findViewById(R.id.paymentDone);

        if (status.equals("Success")) {
            imageView.setImageResource(R.drawable.tick);
            paymentMsg.setText("Payment Successful\n");
            paymentprice.setText("Your payment of " +  Currency + " " + priceType + " for " + Package + " plan has completed");
        } else if (status.equals("Failed")){
            imageView.setImageResource(R.drawable.ic_cross);
            paymentMsg.setText("Payment Failed");
            paymentprice.setText("Your payment of " +  Currency + " " + priceType + " for " + Package + " plan has failed ");
        } else if (status.equals("Canceled")) {
            imageView.setImageResource(R.drawable.ic_cross);
            paymentMsg.setText("Payment Canceled");
            paymentprice.setText("Your payment of " +  Currency + " " + priceType + " for " + Package + " plan has canceled ");
        }


        paymentDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardMonthly:
                priceType = txtmonthly.getText().toString();
                Currency = currency.getText().toString();
                Package = txmonthly.getText().toString();
                Calendar calmonth = Calendar.getInstance();
                calmonth.add(Calendar.MONTH, +1);
                calmonth.add(Calendar.DAY_OF_MONTH, -1);
                expiryDate = dateFormat.format(calmonth.getTime());
                prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
                PaymentStatus = prefs.getString("package","");
                currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                Log.d("datetime " + currentDate, "expiry" + expiryDate + "currency" + Currency +"package"+Package);
                String status = "";
                status = new SessionManagement(getApplicationContext()).SubscriptionStatus();
                if (status.equals("Subscription Is Active")) {
                    if (PaymentStatus.equals("Yearly") || PaymentStatus.equals("Monthly") || PaymentStatus.equals("per Month") || PaymentStatus.equals("per Year")) {
                        Utils.Alert(SubscriptionActivity.this, "This package is already active");
                    }
                }else{
                    Intent intent= new Intent(getApplicationContext(),ChoosePaymentType.class);
                    intent.putExtra("Package",Package);
                    intent.putExtra("Currency",Currency);
                    intent.putExtra("Price",priceType);
                    intent.putExtra("CurrentDate",currentDate);
                    intent.putExtra("ExpiryDate",expiryDate);
                    startActivity(intent);
                   /* if (isConnect) {
                        dialog= new ProgressDialog(SubscriptionActivity.this);
                        dialog.setTitle("Please wait");
                        dialog.setMessage("Loading...");
                        dialog.setCancelable(false);
                        dialog.show();
                        GetCustomerId(dialog);

                    } else {
                        Utils.isNetworkAvailable(getApplicationContext());
                    }*/
                }

                break;

            case R.id.cardYearly:
                Calendar calYearly = Calendar.getInstance();
                calYearly.add(Calendar.YEAR, +1);
                calYearly.add(Calendar.DAY_OF_YEAR, -1);
                expiryDate = dateFormat.format(calYearly.getTime());
                currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                priceType = txtyearly.getText().toString();
                Package = txyearly.getText().toString();
                Currency = currency.getText().toString();
                Log.d("datetime " + currentDate, "expiryearly" + expiryDate + "currency" + Currency);

                prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
                PaymentStatus = prefs.getString("package","");
                status = new SessionManagement(getApplicationContext()).SubscriptionStatus();
                if (status.equals("Subscription Is Active")) {
                    if (PaymentStatus.equals("Yearly") || PaymentStatus.equals("Monthly") || PaymentStatus.equals("per Month") || PaymentStatus.equals("per Year")) {
                        Utils.Alert(SubscriptionActivity.this, "This package is already active");
                    }
                }else{
                   Intent intent= new Intent(getApplicationContext(),ChoosePaymentType.class);
                    intent.putExtra("Package",Package);
                    intent.putExtra("Currency",Currency);
                    intent.putExtra("Price",priceType);
                    intent.putExtra("CurrentDate",currentDate);
                    intent.putExtra("ExpiryDate",expiryDate);
                    startActivity(intent);
                   /* if (isConnect) {
                        dialog= new ProgressDialog(SubscriptionActivity.this);
                        dialog.setTitle("Please wait");
                        dialog.setMessage("Loading...");
                        dialog.setCancelable(false);
                        dialog.show();
                        GetCustomerId(dialog);
                        //getClientSecret(CustomerID, priceType);
                    } else {
                        Utils.isNetworkAvailable(getApplicationContext());
                    }*/
                }
                break;

        }
    }

    public void SavePaymentDetailsBeforeDone() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        token = prefs.getString("token_key", "");
        email = prefs.getString("email_key", "");
        Phnumber = prefs.getString("email_key", "");
        Log.d("currency", priceType);
        PaymentModel paymentModel = new PaymentModel();
        paymentModel.setPaymentSource("card");
        paymentModel.setAmount(priceType);
        paymentModel.setMobile(Phnumber);
        paymentModel.setToken(token);
        paymentModel.setEmail(email);
        paymentModel.setCustomerId(CustomerID);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setCurrency(Currency);
        paymentModel.setPack(Package);
        paymentModel.setSource("app");

        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentResponse> responseCall = api.PaymentDetailsSave("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, retrofit2.Response<PaymentResponse> response) {
                if (response.isSuccessful()) {
                    PaymentData model = response.body().getData();
                    new SessionManagement(getApplicationContext()).SaveOrderId(model.getOrderId(),expiryDate);
                    Log.d("PaymentStatus", response.body().getMessage() +"OrderId "+model.getOrderId());
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                Log.d("PaymentError", t.getMessage());
            }
        });
    }


    public void UpdatePaymentDetailsAfterDone(String status) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        prefs = getSharedPreferences("shared_prefs", MODE_PRIVATE);
        Uid = prefs.getInt("userId_key", 0);
        String orderId = prefs.getString("orderId","");

        Log.d("datetime " + currentDate, "expiry" + expiryDate + "currency" + Currency +"package"+Package +"OrderId" +orderId);
        Log.d("CustomerID2",CustomerID);
        //Log.d("currency", priceType.substring(0, 1));
        PaymentRequest paymentModel = new PaymentRequest();
        //paymentModel.setPaymentSource("card");
        paymentModel.setCustomerId(CustomerID);
        paymentModel.setSubEndDate(expiryDate);
        paymentModel.setSubStartDate(currentDate);
        paymentModel.setUid(String.valueOf(Uid));
        paymentModel.setPaymentStatus(status);
        paymentModel.setOrderId(orderId);
        paymentModel.setSource("app");
        APIClient api = retrofit.create(APIClient.class);
        Call<PaymentDoneResponse> responseCall = api.PaymentDetailsUpdate("Basic dGVmZjQxOG1lZGlhOnRlZmZANDE4QGZnaA==", paymentModel);
        responseCall.enqueue(new Callback<PaymentDoneResponse>() {
            @Override
            public void onResponse(Call<PaymentDoneResponse> call, retrofit2.Response<PaymentDoneResponse> response) {
                //String message = response.body().getMessage();
                if (response.isSuccessful()) {
                  // new SessionManagement(getApplicationContext()).SavePaymentDetails(model.getOrderId());
                    Log.d("PaymenentUpdateStatus", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                Log.d("PaymenentError", t.getMessage());
            }
        });
    }


}
